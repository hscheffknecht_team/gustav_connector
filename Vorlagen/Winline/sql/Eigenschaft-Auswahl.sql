-- Dropdown ID auslesen (winline_id ist die ID die im Eigenschaftenstamm ersichtlich ist)
select c000 as dropdown_id from CWLSYSTEM..T069CMP where C001=@winline_id and C006=1 

set @id = 1234 -- ID aus CWLSYSTEM..T069CMP (nicht aus Eigenschaftenstamm!)
set @artikelnr = '' -- Artikelnr
set @mesocomp = 'COMP' -- Mandant

SELECT TOP 1 b.c010
FROM T070 e
INNER JOIN CWLSYSTEM..T069CMP b ON e.c002 = b.C001 
WHERE b.C000 = @id 
and b.c003 >= 0 
and e.c000 = @artikelnr
and mesoyear=(SELECT max(mesoyear) FROM T001 WHERE mesocomp=@mesocomp)
and mesocomp=@mesocomp

-- Eigenschaft als WHERE bei Produkte (FROM t024 p)
INNER JOIN T070 e ON e.c000 = REPLACE(p.c106, '021-', '') AND e.mesocomp=p.mesocomp AND e.mesoyear=p.mesoyear
INNER JOIN CWLSYSTEM..T069CMP b ON b.c000=@id AND b.c001=e.c002 AND b.c003 >= 0

-- Eigenschaft als Subquery bei Produkte (FROM t024 p)
(SELECT TOP 1 b.c010 FROM T070 e INNER JOIN CWLSYSTEM..T069CMP b ON e.c002 = b.C001 WHERE b.c000=@id AND b.c003 >= 0 AND e.c000=REPLACE(p.c106, '021-', '') AND mesoyear=p.mesoyear AND mesocomp=p.mesocomp) as prop_n

-- Alle Produkte mit einer bestimmten Eigenschaft auslesen
SELECT e.c000
FROM t070 e
INNER JOIN CWLSYSTEM..T069CMP b ON e.c002 = b.c001
WHERE b.c000 = @id AND b.c003 >= 0 AND mesoyear=p.mesoyear and mesocomp=p.mesocomp

