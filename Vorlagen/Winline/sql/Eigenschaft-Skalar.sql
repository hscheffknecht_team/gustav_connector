set @id = 1234 -- ID aus Eigenschaftenstamm
set @artikelnr = '' -- Artikelnr
set @mesocomp = 'COMP' -- Mandant

select TOP 1 c001 
from t070 WITH (NOLOCK) 
where c002 = @id and mesocomp=@mesocomp and mesoyear=(SELECT max(mesoyear) FROM T001 WHERE mesocomp=T070.mesocomp)
and c000=@artikelnr

-- Eigenschaft als WHERE bei Produkte (FROM t024 p)
INNER JOIN t070 e ON e.c002 = @id AND e.mesocomp=p.mesocomp and e.mesoyear=p.mesoyear and e.c000=REPLACE(p.c106, '021-', '')

-- Eigenschaft als Subquery bei Produkte (FROM t024 p)
(SELECT TOP 1 c001 FROM T070 WHERE c002=@id AND mesocomp=p.mesocomp AND mesoyear=p.mesoyear AND c000=REPLACE(p.c106, '021-', '')) as prop_n