Gustav Connector Anleitung
==========================

Inhalt
------

* [Gustav Connector installieren](install.md)
* [Produktdaten in Gustav übertragen](import.md)
* [Bestellungen aus Gustav abrufen](export.md)
* [Beschreibung der Programmeinstellungen](settings.md)
* [Automatischer Modus](auto.md)