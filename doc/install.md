Installation
============

Voraussetzungen
---------------

* [Microsoft .NET Framework 4.0 oder h�her](http://www.microsoft.com/de-at/download/details.aspx?id=17718)
* [GTK# f�r .NET Framework 2.12.21](http://download.xamarin.com/GTKforWindows/Windows/gtk-sharp-2.12.21.msi)

**Hinweis:** Gustav Connector funktioniert mit Gtk# Version 2.12.30 nicht richtig, es sollte daher die oben genannte
Version verwendet werden.

Installation
------------
* Installation der Voraussetzungen
* Download [Gustav Connector](https://www.gustav.cc/downloads/Gustav_Connector.zip)
* Entpacken in ein Verzeichnis und Gustav_Connector.exe starten.
* Konfiguration der [Importe](import.md)/[Exporte](export.md)
* Einrichtung des Gustav Connectors als _Geplanter Task_ im Windows mit dem Parameter _auto_. (Siehe
  [Automatischer Modus](auto.md)).
