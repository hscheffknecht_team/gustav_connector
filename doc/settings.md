Programmeinstellungen
=====================

![Programmeinstellungen](img/settings.jpg)

Folgende Programmeinstellungen sind unter _Programm > Einstellungen_ verf�gbar:

* **Gustav Adresse:** Gibt die Adresse vom Gustav an (kann auch auf die Adresse einer Entwicklungsumgebung ge�ndert
  werden).
* **API Schl�ssel:** Gibt den API-Schl�ssel aus der Gustav-Datenbank f�r diesen Kunden an.
* **API Secure Token:** Gibt den Secure Token aus der Gustav-Datenbank f�r diesen Kunden an.
* **Fehlermails versenden:** Wenn aktiviert, wird im automatischen Modus bei einem Fehler eine Email versendet. Die
  Email geht immer an das Gustav-Supportpostfach.
* **Bildupload �berspringen:** Zu Debug-Zwecken kann der Bildupload �bersprungen werden. Dies sollte im Live-Betrieb
  immer deaktiviert bleiben!
* **T�glich pr�fen:** Ist diese Option aktiviert, werden 1x t�glich die Keys zwischen dem lokalen Datenstand und dem
  Gustav Datenstand verglichen. Differenzen werden dann per Email �ber die Gustav-API an das Gustav-Supportpostfach
  verschickt.
* **Letzte Auftrags-Freigabenr.:** Enth�lt die Freigabenummer des letzten importierten Auftrags.
* **Letztes Pr�fdatum:** Enth�lt das Datum, andem der Datenstand letztmalig automatisch gepr�ft wurde. Diees Datum
  kann gel�scht werden, dann wird der Datenstand beim n�chsten automatischen Durchlauf nochmal gepr�ft.
