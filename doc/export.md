Export der Auftr�ge vom Gustav ins ERP System
=============================================

![Gustav Auftragsexport](img/orders-empty.jpg)

Dieser Tab _Bestellungen & Kunden Export_ erm�glicht es, Bestellungen aus dem Gustav zu �bernehmen und diese als
Bestellungen sowie Kunden in mehrere ERP-Importtabelle zu schreiben. ERP-Importtabellen sind MS-SQL Tabellen, aus
denen das ERP-System die Bestellungen & Kunden liest und importiert. Es gibt folgende 3 Tabellen:

* Kunden
* Auftr�ge
* Auftragspositionen

Der Ablauf ist wie folgt:

* Zuerst m�ssen die ERP-Importtabellen in der SQL-Datenbank (und evtl. auch im ERP-System) vorbereitet werden
* Anschlie�end m�ssen mit den Schaltfl�chen _Auftr�ge_, _Artikelzeilen_ und _Kunden_ im Tab _Einstellungen_
  die Spalten definiert werden.
* Auf der rechten Seite m�ssen die DB-Writer definiert werden, f�r jeden ERP-Importtabelle ist ein DB-Writer anzulegen.
* Auf der linken Seite (bei Plugins) muss dann mindestens ein Mapping-Plugin pro ERP-Importtabelle erstellt werden.
  Damit werden dann die Daten vom Gustav-XML auf die ERP-Importtabellen gemappt. Zus�tzlich kann es notwendig sein,
  weitere Plugins zu verwenden.
  
Plugins
-------

F�r das Mappen der Daten vom Gustav-XML zu den ERP-Importtabellen stehen aktuell folgende Plugins zur Verf�gung:

![Plugins f�r Auftragsimport](img/orders-plugins.jpg)
 
### Mapping Plugin ###

![Mapping Plugin](img/orders-plugin-mapping.jpg)

Erm�glicht die einfache �bernahme der Daten vom Gustav-XML in die ERP-Importtabellen:

* Pro ERP-Importtabelle wird mindestens ein Mapping-Plugin ben�tigt
* **Zieltabelle:** Gibt an, in welche Tabelle die Daten kopiert werden
* **Zuordnungen:** Pro Feld kann hier eine Zuordnungs-Zeile hinzugef�gt werden
* **Pr�fix:** Erm�glicht, einen fixen Text vor den eigentlichen Wert anzuh�ngen
* **Quellspalte:** Wenn hier ein Wert ausgew�hlt wurde, dann wird der entsprechende Wert aus der XML �bernommen.
  **Hinweis:** Wenn hier keine Spalten ausw�hlbar ist, dann stehen vom Gustav her keine Auftr�ge zur Verf�gung. Dann
  muss im Gustav zuerst ein Auftrag freigegeben werden und im Gustav Connector der Cache (Men� _Cache_) geleert werden.
* **Quellwert:** Wenn hier ein Wert eingegeben wurde, wird immer fix dieser Wert �bernommen
* **Zielspalte:** Gibt an, in welcher Spalte innerhalb der ERP-Importtabelle der Wert gespeichert wird.
* **L�schen:** Erm�glicht es, die Zuordnung zu l�schen.

### Value Finder Plugin ###

![Value Finder Plugin](img/orders-plugin-valuefinder.jpg)

Dieses Plugin erm�glicht es, einen einzelnen Wert (analog zu einer select/case Anweisung) zu mappen:
 
* **Zieltabelle:** Gibt die ERP-Importtabelle an. Nach �nderung dieses Werts muss das Fenster neu ge�ffnet werden, damit
  _Quellspalte_ und _Zielspalte_ neu geladen werden!
* **Quellspalte:** Gibt an, aus welcher Spalte der Wert gelesen wird.
* **Zielspalte:** Gibt an, in welche Spalte der neue Wert gespeichert wird (kann auch gleich sein wie _Quellspalte_).
* **Werte:** Hier kann ein zus�tzliches Mapping f�r den Wert hinzugef�gt werden.
* **Ausgangswert:** Wenn der Wert der Quellspalte genau diesen Wert enth�lt...
* **Zielwert:** ...wird dieser Wert in die Zielspalte geschrieben.
* **L�schen:** Damit kann diese Zeile entfernt werden.

### Winline Auftragsimport Plugin ###

![Value Finder Plugin](img/orders-plugin-winline.jpg)

Dieses Plugin enth�lt eine Reihe von Funktionen, damit Auftr�ge und Kunden in die Mesonic Winline importiert werden
k�nnen:

* Ermittlung von Kundennummern aus der Winline Datenbank
* Optionales Einf�gen des Original-Preislistenpreies in die Auftragszeilen
* Einf�gen der Preisliste, Belegart und Zahlungskondition in die Kundentabelle falls der Kunde bereits existiert
* Einf�gen Belegart und Zahlungsmethode in die Auftragstablle falls der Kunde bereits existiert
* Ermittlung der Auftrags-Referenznummer aus Kunden-Zusatzfeld

Damit das Plugin funktioniert, m�ssen folgende Daten angegeben werden:

* **Datenbank (Daten):** Gibt den Datenbanknamen (zb. CWLDATEN) an.
* **Mandant:** Gibt den Winline-Mandant (zb. ABCD) an.
* **Pr�fix:** Gibt optional einen Pr�fix f�r die Kundennummern an.
* **Erste Kundennummer:** Definiert den Start des Kundennummernkreises.
* **Nummern absteigend:** Gibt an, dass die Nummern absteigend vergeben werden.
* **Nur Kunden im Nummernkreis verwenden:** Wird bei der Kundenfindung ein Kunde ermittelt, der au�erhalb des
  definierten Nummernkreises liegt, dann wird trotzdem ein neuer Kunde angelegt.
* **Textbaustein-Platzhalter:** Wird dieser Platzhalter verwendet, wird der Inhalt eines Winline-Textbausteins
  eingetragen.
* **Textbaustein-Nummer:** Gibt die Textbaustein-Nummer aus der Winline an, welche geladen werden soll.

### Gutscheine Plugin ###

![Value Finder Plugin](img/orders-plugin-coupon.jpg)

Erstellt f�r Auftr�ge mit einem Gutscheinwert eine zus�tzliche Artikelzeile, die den eingegebenen Gutscheincode sowie
den negativen Gutscheinwert enth�lt.

**Hinweis:** Die Funktion ist auch direkt mit dem Mapping Plugin m�glich und auch die empfohlene Vorgehensweise,
da diese flexibler ist.

### Gutschein-Prozent Plugin ###

Dieses Plugin rechnet den Gutscheinwert pro Produkt auf einen Rabatt-Prozentsatz um und tr�gt bei den Artikelzeilen
diesen Rabatt ein. Dieses Plugin bietet keine Konfigurationsm�glichkeiten.

Datenstand-Funktionen
=====================

Schaltfl�che "Gustav"
---------------------

Die Schaltfl�che erm�glicht, das Gustav-XML mit den Auftragsdaten anzusehen. Der Gustav gibt alle Auftr�ge zur�ck,
dessen Freigabe-Nummer ("approve_number") gr��er ist, wie die unter _Programm -> Einstellungen ->
Letzte Auftrags-Freigabenummer_.

**Achtung:** Die Antwort vom Gustav-Server wird gecached. Daher muss nach �nderung der _Letzte Auftrags-Freigabenummer_
oder nach Freigabe eines Auftrags entweder der Cache geleert werden ("Men� Cache -> SOAP Cache leeren") oder der
_Gustav Connector_ neu gestartet werden.

Schaltfl�chen "Auftr�ge" / "Artikelzeilen" / "Kunden"
-----------------------------------------------------

Diese Schaltfl�chen bieten 2 Funktionen:
* Im Tab _Vorschau_ kann nach einem Durchlauf der zu importierende/importierte Datenstand angesehen werden.
* Im Tab _Einstellungen_ werden die Spalten dieser Tabelle definiert.

Schaltfl�chen "Testlauf starten (Vorschau)" / "Kompletter Lauf starten"
-----------------------------------------------------------------------

Mit diesen Schaltfl�chen wird ein Durchlauf gestartet. Der Testlauf schreibt keine Daten. Im Kompletten Lauf wird die
_Letzte Auftrags-Freigabenummer_ auf den h�chsten Wert aller vorhandenen Auftr�ge gesetzt und die ERP-Importtabellen
werden gef�llt.

DB-Writer
=========

Mittels DB-Writer werden die Daten in die ERP-Importtabellen geschrieben. Aktuell ist nur ein DB-Writer verf�gbar.

MS-SQL Writer
-------------

![MS-SQL Writer](img/orders-writer-mssql.jpg)

Mit diesem Writer k�nnen die Daten in eine MS-SQL Tabelle geschrieben werden. Dazu sind folgende Angaben notwendig:

* **Zieltabelle:** Gibt an, f�r welche ERP-Importtabelle der Writer zust�ndig ist.
* **DB-Server:** Gibt den MS-SQL Server an.
* **DB-Benutzer:** Gibt den Datenbank-Benutzer an.
* **DB-Passwort:** Gibt das Passwort f�r den Datenbank-Benutzer an.
* **Datenbank:** Gibt an, in welcher Datenbank geschrieben wird.
* **DB-Tabelle:** Gibt den MS-SQL Tabellennamen an.

&nbsp;