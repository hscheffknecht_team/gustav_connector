Import vom ERP-System in Gustav
===============================

![Gustav Hauptfenster](img/mainwindow-empty.jpg)

Folgende Punkte beschreiben die Oberfl�che f�r den Import (alle Tabs au�er _Bestellungen und Kunden Export_):

* F�r jede zu �bertragende Entit�t steht ein Tab zur Verf�gung
* Um eine Entit�t zu importieren, muss mindestens ein Datenbank-Adapter (links) hinzugef�gt werden.
* Wenn eine Entit�t (zb. Hersteller) nicht ben�tigt wird, kann der Tab auch leer gelassen werden.
* Je nach Datenquelle kann ein entsprechender Datenbank-Adapter verwendet werden. Diese werden im n�chsten Abschnitt
  beschrieben.
* Um die Daten nach dem Lesen weiter zu verarbeiten, k�nnen Plugins verwendet werden. Es werden daher die Daten aus
  allen Datenbank-Adaptern in eine tempor�re Tabelle geladen, anschlie�end werden die Plugins ausgef�hrt.
* Wenn auf _Verarbeitung durchf�hren (ohne Gustav Import)_ geklickt wird, werden Datenbank-Adapter und Plugins
  ausgef�hrt, allerdings keine Daten zum Gustav Server �bertragen.
* Wenn auf _Verarbeitung durchf�hren (mit Gustav Import)_ geklickt wird, werden Datenbank-Adapter und Plugins ausgef�hrt
  und anschlie�end die Daten zum Gustav Server �bertragen.
* Mit den Schaltfl�chen _Datenstand nach Import..._ und _Datenstand nach Plugins..._ kann der Datenstand vor und nach
  der Plugin-Ausf�hrung angesehen werden.
* Mit der Funktion _Daten vergleichen_ kann der Datenstand zwischen dem lokalen Stand (Datenbank-Adapter) und dem Gustav
  Server verglichen werden. **Achtung:** Dies vergleicht nur, ob die entsprechende Entit�t vorhanden ist, nicht ob deren
  Eigenschaften �bereinstimmen.
* Mit der Funktion _Auf Server l�schen..._ k�nnen bestimmte Entit�ten auf dem Gustav Server gel�scht werden. Das ist
  vor allem dann sinnvoll, wenn falsche Entit�ten importiert wurden oder diese nicht gel�scht wurden.

Datenbankadapter
----------------

Die Datenbank-Adpater kennen die notwendigen Felder der ausgew�hlten Entit�t (=Tab). Grunds�tzlich sind alle Werte
optional, zumindest die _Key_-Felder sollten immer bef�llt werden, da diese f�r die Identifizierung bzw. Zuordnung der
Entit�ten notwendig sind.

Folgende Datenbank-Adapter stehen zur Auswahl:

![Screenshot Datenbankadapter](img/database-adapters.jpg)

### MS-SQL Reader ###

![MSSQL Datenbankadapter](img/database-adapter-mssql.jpg)

Dieser Adapter sollte f�r den Zugriff auf MS-SQL Datenbanken verwendet werden.

* _DB-Server_, _DB-Benutzer_, _DB-Passwort_ und _Datenbank_ sind f�r den Login zur Datenbank erforderlich.
* Die Felder _SQL-Abfrage_ sind abh�ngig von der gew�hlten Entit�t, in dem Beispiel werden die Felder f�r den Hersteller
  ben�tigt: _Key_, _Name_ und _Logo-Key_. Hier werden Spalten, Tabellen und Filter als SQL-Statements eingegeben.
* _Key_ dient zur Identifizierung und ist erforderlich. Name sollte auch �bermittelt werden und _Logo-Key_ ist optional.
* _Timestamp_ dient zur Identifizerung von neuen bzw. ge�nderten Datens�tzen und ist daher erforderlich. Diese Spalte
  sollte den MS-SQL Timestamp-Datentyp als Integer Wert zur�ckgeben (zb. _CAST(ts as int)_).
* _Zus. Spalten_ ist f�r Kundenspezifische Felder oder �bersetzungen reserviert. (zb. _c000 as prop_1_ oder _c001 as
  name_en_).
* _FROM_ gibt die Tabelle an, hier k�nnen auch mehrere Tabellen per join verkn�pft werden
* _Timestamp (@timestamp)_ gibt den Filter an, damit nur die neuen Datens�tze abgerufen werden. @timestamp ist der
  Wert, der ganz unten unter _Letzter Timestamp_ eingetragen ist. Nach einer kompletten Verarbeitung (mit Gustav Import)
  wird dieser Wert mit dem h�chsten Wert aus der _Timestamp_ Spalte eingetragen.
* _Zus. Where_ gibt zus�tzliche Filter f�r die Datenabfrage an.
* In dem Beispiel wird folgende Datenbank-Abfrage erstellt und ausgef�hrt:<br>
  _SELECT id as [key], name as name, CAST(ts as int) as timestamp FROM manufacturers WHERE ts > 0 AND is_active=1_

### ODBC Reader ###

![ODBC Datenbankadapter](img/database-adapter-odbc.jpg)

Einfacher Reader f�r ODBC-Verbindungen. Wird aktuell nicht verwendet und nicht getestet, k�nnte daher Fehler
verursachen. Hier muss die Datenbankverbindung manuell eingegeben werden, funktioniert ansonsten gleich wie der MS-SQL
Adapter.

### OLEDB Reader ###

![OLEDB Datenbankadapter](img/database-adapter-oledb.jpg)

Reader f�r OLEDB-Verbindungen. Wird aktuell nicht verwendet und nicht getestet, k�nnte daher Fehler verursachen.
Funktion �hnlich wie vom MS-SQL Adapter.

### Statische Daten ###

![Adapter f�r Statische Daten](img/database-adapter-static.jpg)

Mit diesem Adapter k�nnen Statische Daten eingegeben werden. Dies kann f�r Testzwecke verwendet werden, oder wenn Daten
�bertragen werden sollen, die in der Datenbank nicht vorhanden sind.

Das Feld _Spalten_ wird schon vorausgef�llt, nicht ben�tigte Spalten k�nnen aber entfernt werden. Auch hier sollten
_key_-Spalten und die _timestamp_ Spalte nicht gel�scht werden.

Im _Daten_ Feld k�nnen dann per Komma (,) getrennt die Werte eingegeben werden. Pro Zeile kann ein Datensatz definiert
werden. Erweiterte CSV-Funktionen (wie zb. Anf�hrungszeichen um Zeilumbr�che oder Kommas als Daten einzutragen)
sind hier nicht m�glich.

### Files Reader ###

![Adapter f�r Bilder](img/database-adapter-files.jpg)

Dieser Adapter dient dazu, Bilder zu �bertragen. Hier kann das Verzeichnis (Unterverzeichnisse werden ebenfalls
durchsucht) sowie die zul�ssigen Dateierweiterungen eingetragen bzw. angepasst werden.

Die Dateien m�ssen dabei folgende Syntax aufweisen:

* _ARTIKELNUMMER.ext_ oder
* _ARTIKELNUMMER_SORTIERUNG.ext_ (_SORTIERUNG_ muss dabei eine Ganzzahl sein) oder
* _ARTIKELNUMMER_PLUGIN-DATEN.ext_ (_PLUGIN-DATEN_ darf dabei keine Ganzzahl sein) oder
* _ARTIKELNUMMER_SORTIERUNG_PLUIN-DATEN.ext_

Darunter k�nnen die Felder den folgenden Datei-Eigenschaften zugewiesen werden:

* _Bild-Key_: Entspricht dem Dateinamen
* _Product-Key_: Entspricht _ARTIKELNUMMER_ aus dem Dateinamen
* _Dateiname_: Entspricht dem Dateinamen
* _Dateigr��e_: Entspricht der Dateigr��e
* _Dateiinhalt_: Dateiinhalt als Bin�rdaten
* _Sortierung_: Entspricht _SORTIERUNG_ aus dem Dateinamen
* _EXIF Beschreibung_: Beschreibung aus den EXIF-Daten der Bilddatei
* _Plugin-Daten_: Entspricht _PLUGIN-DATEN_ aus dem Dateinamen
* _Letzte �nderung_: �nderungsdatum der Datei

Die Spalte _Timestamp_ kann hier ignoriert werden.

Plugins
-------

![Plugins �bersicht](img/plugins.jpg)

Plugins dienen dazu, den Datenstand nach dem einlesen und vor dem Weitersenden an den Gustav zu ver�ndern. Aktuell
stehen folgende Plugins zur Verf�gung:

* _RTF Umwandler_: Wandelt RTF in einer Spalte in HTML um. 
* _Temp-2-Json_: L�dt Datent aus einer tmp. Tabelle und schreibt diese als JSON in die Export-Tabelle.
* _Split field_: Erm�glicht es, aus einem Datensatz mehrere zu machen.

### RTF Umwandler ###

![RTF Plugin](img/plugin-rtf.jpg)

Dieses Plugin wandelt alle Texte innerhalb einer Spalte von RTF zu HTML/Text um.

Dabei werden folgende Formatierungen unterst�tzt:

* Absatz
* Zeilenumbruch
* Fett
* Kursiv
* Unterstrichen
* Aufz�hlungen
* Nummerierungen
* Schriftgr��e

**Achtung:** Bilder in RTF f�hren aktuell zu Fehler und sollten vermieden werden.

Bei _RTF Spalte_ muss die entsprechende Spalte eingegeben werden, welche umgewandelt werden m�ssen. HTML Schriftgr��en
werden relativ exportiert, daher kann hier die Basis-Schriftgr��e eingegeben werden. Diese kann auf _0_ belassen werden,
wenn die Schriftgr��e nicht relevant ist.

Zuletzt kann noch angegeben werden, ob das RTF in Html konviertiert werden soll oder in Klartext.

Wenn mehrere Spalten konvertiert werden m�ssen, dann kann das Plugin mehrmals hinzugef�gt werden.

### Temp-2-Json ###

![Temp 2 JSON Plugin](img/plugin-json.jpg)

Dieses Plugin erm�glicht es, zus�tzliche Daten per JSON zu �bertragen. Hauptzweck dieses Plugins ist, einem Plugin
innerhalb des Gustav Portls zus�tzliche Daten innerhalb der Spalte  _PluginData_ mitzugeben.

Dazu muss ein zus�tzliche Datenbank-Adapter angelegt werden und bei den Filter-Einstellungen bei _Temp. Tabelle_ ein
Wert eingetragen werden. Dann werden die Daten nicht mehr in die Haupttabelle �bertragen, sondern in eine nicht
sichtbare, tempor�re Tabelle.

Anschlie�end kann das Plugin konfiguriert werden:

* _Name der tmp. Tabelle_: Tabellenname aus den Filtereinstellungen
* _Zielspalte_: Spalte, in der das JSON gespeichert werden soll
* _Key-Spalte (tmp.)_ / _Key-Spalte (Gustav)_: Hier m�ssen die Schl�sselspalten eingetragen werden, um die Datens�tze
  aus der tmp. Tabelle zu den Datens�tze in der Haupttabelle zuzordnen.



### Split field Plugin ###

![Split field Plugin](img/plugin-split.jpg)

Mit diesem Plugin k�nnen mehrere Werte innerhalb eines Datensatzes in mehrere aufgeteilt werden. Beispielsweise k�nnten
in einem Produktattribut durch Komma getrennt alle Produktgruppen-Zuordnungen stehen. Mit diesem Plugin k�nnen nach
Eingabe des zu trennenden Feldes und des Trennzeichens die Zeilen in mehrere Felder getrennt. Andere Felder werden dabei
einfach kopiert.
