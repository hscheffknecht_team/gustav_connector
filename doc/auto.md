Automatischer Modus
===================

![Automatischer Modus](img/auto.jpg)

Wenn der Gustav-Connector �ber die Konsole mit dem Parameter ''auto'' aufgerufen wird, wird kein Fenster ge�ffnet,
stattdessen werden alle Importe durchgef�hrt und das Programm wird beendet.