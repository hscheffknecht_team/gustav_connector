using System;
using Gtk;

namespace Gustav_Connector {

	public partial class MainWindow: Gtk.Window
	{	
		public MainWindow (): base (Gtk.WindowType.Toplevel)
		{
			Build ();
			Init();
			ShowPlugins();

			// Show version
			this.Title += String.Format (" ({0})", Common.GetVersion());

			// Assign processors
			manufacturersContent.Processor = new ManufacturersProcessor ();
			customersContent.Processor = new CustomersProcessor ();
			customergroupContent.Processor = new CustomergroupsProcessor ();
			contactsContent.Processor = new ContactsProcessor ();
			imagesContent.Processor = new ImagesProcessor ();
			productgroupsContent.Processor = new ProductgroupsProcessor ();
			pricelistsContent.Processor = new PricelistsProcessor ();
			productsContent.Processor = new ProductsProcessor ();
			prodgrprelContent.Processor = new ProdgrprelProcessor ();
			pricesContent.Processor = new PricesProcessor ();
			productimagesContent.Processor = new ProductimagesProcessor ();
			discountsContent.Processor = new DiscountsProcessor ();
			stockContent.Processor = new StockProcessor ();
			orderStatusContent.Processor = new OrderStatusProcessor ();
			trackingCodesContent.Processor = new TrackingCodesProcessor ();
		}
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}

		protected void OnBeendenActionActivated (object sender, EventArgs e)
		{
			OnDeleteEvent(null, new DeleteEventArgs());
		}

		private void Init ()
		{
			mainTabs.CurrentPage = 0;
		}

		protected void OnExecuteActionActivated (object sender, EventArgs e)
		{
			var settings = new SettingsDialog();
			settings.Show();
		}

		protected void OnBtnDataGustavClicked (object sender, EventArgs e)
		{
			var viewer = new GustavOrdersViewer();
			viewer.Show();
		}

		protected void OnClearSoapCacheActionActivated (object sender, EventArgs e)
		{
			SoapDataCache.Clear();
		}

		protected void OnBtnDataOrdersClicked (object sender, EventArgs e)
		{
			var window = new TargetTablesEditor();
			window.Title = "Aufträge";
			window.ContentTable = OrdersDataCache.Orders;
			window.Columns = OrderSettings.Current.OrdersColumns;
			window.Run();

			if(window.Columns != null)
			{
				OrderSettings.Current.OrdersColumns = window.Columns;
				OrderSettings.Current.Save();
			}
		}

		protected void OnBtnDataOrderItemsClicked (object sender, EventArgs e)
		{
			var window = new TargetTablesEditor();
			window.Title = "Artikelzeilen";
			window.ContentTable = OrdersDataCache.OrderItems;
			window.Columns = OrderSettings.Current.OrderItemsColumns;
			window.Run();
			
			if(window.Columns != null)
			{
				OrderSettings.Current.OrderItemsColumns = window.Columns;
				OrderSettings.Current.Save();
			}
		}

		protected void OnBtnDataCustomersClicked (object sender, EventArgs e)
		{
			var window = new TargetTablesEditor();
			window.Title = "Kunden";
			window.ContentTable = OrdersDataCache.Customers;
			window.Columns = OrderSettings.Current.CustomersColumns;
			window.Run();
			
			if(window.Columns != null)
			{
				OrderSettings.Current.CustomersColumns = window.Columns;
				OrderSettings.Current.Save();
			}
		}

		protected void OnAddPluginActivated (object sender, EventArgs e)
		{
			var dialog = new PluginChooser();
			dialog.PluginsList = PluginsBrowser.Current.OrderPlugins.ToArray();
			dialog.FillPlugins();
			dialog.Run();
			dialog.Destroy();

			if(dialog.SelectedPlugin != null) {
				var plugin = (System.Xml.Serialization.IXmlSerializable)Activator.CreateInstance(dialog.SelectedPlugin.GetType());
				OrderSettings.Current.OrdersPlugins.Add(plugin);
			}

			OrderSettings.Current.Save();
			ShowPlugins();
		}

		private void ShowOrderPlugins()
		{
			ShowPlugins(OrderSettings.Current.OrdersPlugins, pluginsContainer, "plugin");
			ShowPlugins(OrderSettings.Current.WriterPlugins, writersContainer, "writer");
		}

		private void ShowPlugins()
		{
			ShowOrderPlugins ();
		}

		private void ShowPlugins(SettingsCollection plugins, Gtk.VBox container, string type) {
			if(plugins == null)
				return;

			// Clear container
			while(container.Children.Length > 0) {
				container.Remove(container.Children[0]);
			}

			foreach(var sett in plugins)
			{
				var plugin = (PluginBase)sett;

				// Add name
				var lblName = new Gtk.Label(plugin.Name);
				container.Add(lblName);
				
				// Add status
				var lblStatus = new Gtk.Label(plugin.GetStatusInfo());
				container.Add(lblStatus);
				
				// Add filter
				var lblFilter = new Gtk.Label("Filter: " + plugin.GetFilterInfo());
				container.Add(lblFilter);

				// Create HBOX
				var btnContainer = new Gtk.HBox();
				container.Add(btnContainer);

				// Add settings button
				var btnSett = new Gtk.Button();
				btnSett.Name = "settings_" + plugin.Id;
				btnSett.Clicked += ButtonPluginSettingsClick;
				btnContainer.Add(btnSett);
				
				var imgSett = new Gtk.Image();
				imgSett.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-edit", global::Gtk.IconSize.Menu);
				btnSett.Add(imgSett);

				// Add filter button
				var btnFilter = new Gtk.Button();
				btnFilter.Name = "filter_" + plugin.Id;
				btnFilter.Clicked += ButtonPluginFilterClick;
				btnContainer.Add(btnFilter);
				
				var imgFilter = new Gtk.Image();
				imgFilter.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-color-picker", global::Gtk.IconSize.Menu);
                btnFilter.Add(imgFilter);
                
                // Add remove button
                var btnDelete = new Gtk.Button();
				btnDelete.Name = "delete_" + plugin.Id;
                btnDelete.Clicked += ButtonDeletePluginClick;
                btnContainer.Add(btnDelete);
                
                var imgDel = new Gtk.Image();
                imgDel.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-delete", global::Gtk.IconSize.Menu);
                btnDelete.Add(imgDel);
                
                // Add move up button
                var btnMoveUp = new Gtk.Button();
				btnMoveUp.Name = "moveup_" + plugin.Id;
                btnMoveUp.Clicked += ButtonMoveUpPluginClick;
                btnContainer.Add(btnMoveUp);
                
                var imgMoveUp = new Gtk.Image();
                imgMoveUp.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-go-up", global::Gtk.IconSize.Menu);
                btnMoveUp.Add(imgMoveUp);
                
                // Add move down button
                var btnMoveDown = new Gtk.Button();
				btnMoveDown.Name = "movedown_" + plugin.Id;
                btnMoveDown.Clicked += ButtonMoveDownPluginClick;
                btnContainer.Add(btnMoveDown);
                
                var imgMoveDown = new Gtk.Image();
                imgMoveDown.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-go-down", global::Gtk.IconSize.Menu);
                btnMoveDown.Add(imgMoveDown);

				// Add to global list
				PluginsBrowser.Current.AllPluginInstances [plugin.Id] = plugin;
			}

			container.ShowAll();
		}
		
		private void ButtonPluginSettingsClick(object sender, EventArgs e)
		{
			var id = ((Button)sender).Name.Split('_')[1];
			PluginBase plugin = PluginsBrowser.Current.AllPluginInstances [id];

			Type dialogType = null;

			if (plugin is OrderPluginBase) {
				dialogType = ((OrderPluginBase)plugin).SettingsDialogType;
			} else if (plugin is WriterPluginBase) {
				dialogType = ((WriterPluginBase)plugin).SettingsDialogType;
			}

			var dialog = (PluginSettingsDialog)Activator.CreateInstance (dialogType);
			dialog.CurrentPlugin = plugin;
			dialog.Run ();
			dialog.Destroy ();
			plugin.Container.Container.Save ();
			ShowPlugins ();
		}
		
		private void ButtonPluginFilterClick(object sender, EventArgs e)
		{
			var id = ((Button)sender).Name.Split('_')[1];
			PluginBase plugin = PluginsBrowser.Current.AllPluginInstances [id];
			
			var dialog = new FilterEditor();
			dialog.CurrentPlugin = plugin;
			dialog.Run();
			dialog.Destroy();
			OrderSettings.Current.Save();
			ShowPlugins();
		}
		
		private void ButtonDeletePluginClick(object sender, EventArgs e)
		{
			var id = ((Button)sender).Name.Split('_')[1];
			PluginBase plugin = PluginsBrowser.Current.AllPluginInstances [id];

			plugin.Container.Remove (plugin);
			
			OrderSettings.Current.Save();
			ShowPlugins();
		}
        
        private void ButtonMoveUpPluginClick(object sender, EventArgs e)
        {
			var id = ((Button)sender).Name.Split('_')[1];
			PluginBase plugin = PluginsBrowser.Current.AllPluginInstances [id];

			plugin.Container.MoveUp (plugin.Container.IndexOf(plugin));
            
            OrderSettings.Current.Save();
            ShowPlugins();
        }
        
        private void ButtonMoveDownPluginClick(object sender, EventArgs e)
        {
			var id = ((Button)sender).Name.Split('_')[1];
			PluginBase plugin = PluginsBrowser.Current.AllPluginInstances [id];

			plugin.Container.MoveDown (plugin.Container.IndexOf(plugin));
            
            OrderSettings.Current.Save();
            ShowPlugins();
        }

		protected void OnBtnPreviewClicked (object sender, EventArgs e)
		{
			var processor = new OrderProcessor();
			processor.Start(true);
		}

		protected void OnBtnStartClicked (object sender, EventArgs e)
		{
			var processor = new OrderProcessor();
			processor.Start(false);
		}

		protected void OnBtnAddWriterClicked (object sender, EventArgs e)
		{
			var dialog = new PluginChooser();
			dialog.PluginsList = PluginsBrowser.Current.WriterPlugins.ToArray();
			dialog.FillPlugins();
			dialog.Run();
			dialog.Destroy();

			if(dialog.SelectedPlugin != null) {
				var plugin = (System.Xml.Serialization.IXmlSerializable)Activator.CreateInstance(dialog.SelectedPlugin.GetType());
				OrderSettings.Current.WriterPlugins.Add(plugin);
			}

			OrderSettings.Current.Save();
			ShowPlugins();
		}

		protected void OnTestAdded (object o, AddedArgs args)
		{
			throw new System.NotImplementedException ();
		}

		private PluginBase _AddReaderPlugin()
		{
			var dialog = new PluginChooser();
			dialog.PluginsList = PluginsBrowser.Current.ReaderPlugins.ToArray();
			dialog.FillPlugins();
			dialog.Run();
			dialog.Destroy();

			if(dialog.SelectedPlugin != null) {
				var plugin = (PluginBase)Activator.CreateInstance(dialog.SelectedPlugin.GetType());
				return plugin;
			}

			return null;
		}

		protected void OnBtnMfAddReaderClicked (object sender, EventArgs e)
		{
			var plugin = _AddReaderPlugin ();

			if(plugin != null) {
				MfSettings.Current.ReaderPlugins.Add(plugin);
			}

			MfSettings.Current.Save();
			ShowPlugins();
		}

		protected void OnMainTabsSwitchPage (object o, SwitchPageArgs args)
		{
			ShowPlugins ();
		}

		protected void OnWinlineBilderAbspeichernActionActivated (object sender, EventArgs e)
		{
			var dialog = new WinlineImagesSaveDialog ();
			dialog.Run ();
			dialog.Destroy ();
		}
	}
}