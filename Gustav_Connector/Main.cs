using System;
using Gtk;
using System.Runtime.InteropServices;

namespace Gustav_Connector
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();

			if(args.Length >= 1 && args[0] == "auto") 
			{
				/*
				 * Auto mode
				 */

				Context.Current.AutoMode = true;
				Console.WriteLine("Automatischer Modus aktiviert, starte Durchlauf...");

				var helper = new AutoModeHelper ();
				helper.DoWork ();
			}
			else
			{
				// Normal mode
				try {
					MainWindow win = new MainWindow ();
					win.Show ();
					Application.Run ();
				} catch(Exception ex) {
					Logger.Current.HandleException(ex, true);
				}
			}
		}
	}
}
