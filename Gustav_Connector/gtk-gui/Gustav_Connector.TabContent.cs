
// This file has been generated by the GUI designer. Do not modify.
namespace Gustav_Connector
{
	public partial class TabContent
	{
		private global::Gtk.HBox hbox1;
		
		private global::Gtk.VBox vbox2;
		
		private global::Gtk.Label label2;
		
		private global::Gtk.ScrolledWindow GtkScrolledWindow;
		
		private global::Gtk.VBox vAdaptersContainer;
		
		private global::Gustav_Connector.PluginItem pluginitem1;
		
		private global::Gustav_Connector.PluginItem pluginitem2;
		
		private global::Gustav_Connector.PluginItem pluginitem3;
		
		private global::Gtk.Button btnAddReader;
		
		private global::Gtk.VBox vbox5;
		
		private global::Gtk.Label label3;
		
		private global::Gtk.ScrolledWindow GtkScrolledWindow1;
		
		private global::Gtk.VBox vPluginsContainer;
		
		private global::Gtk.Button btnAddPlugin;
		
		private global::Gtk.VBox vbox6;
		
		private global::Gtk.Label label4;
		
		private global::Gtk.Button btnStartPreview;
		
		private global::Gtk.Button btnReadersPreview;
		
		private global::Gtk.Button btnPluginsPreview;
		
		private global::Gtk.Button btnStartAll;
		
		private global::Gtk.HSeparator hseparator1;
		
		private global::Gtk.Button btnCheck;
		
		private global::Gtk.Button btnDeleteOnServer;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget Gustav_Connector.TabContent
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Gustav_Connector.TabContent";
			// Container child Gustav_Connector.TabContent.Gtk.Container+ContainerChild
			this.hbox1 = new global::Gtk.HBox ();
			this.hbox1.Name = "hbox1";
			this.hbox1.Homogeneous = true;
			this.hbox1.Spacing = 6;
			this.hbox1.BorderWidth = ((uint)(6));
			// Container child hbox1.Gtk.Box+BoxChild
			this.vbox2 = new global::Gtk.VBox ();
			this.vbox2.Name = "vbox2";
			this.vbox2.Spacing = 6;
			// Container child vbox2.Gtk.Box+BoxChild
			this.label2 = new global::Gtk.Label ();
			this.label2.Name = "label2";
			this.label2.LabelProp = global::Mono.Unix.Catalog.GetString ("Datenbank");
			this.vbox2.Add (this.label2);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.label2]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;
			// Container child vbox2.Gtk.Box+BoxChild
			this.GtkScrolledWindow = new global::Gtk.ScrolledWindow ();
			this.GtkScrolledWindow.Name = "GtkScrolledWindow";
			this.GtkScrolledWindow.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow.Gtk.Container+ContainerChild
			global::Gtk.Viewport w2 = new global::Gtk.Viewport ();
			w2.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child GtkViewport.Gtk.Container+ContainerChild
			this.vAdaptersContainer = new global::Gtk.VBox ();
			this.vAdaptersContainer.Name = "vAdaptersContainer";
			this.vAdaptersContainer.Spacing = 6;
			// Container child vAdaptersContainer.Gtk.Box+BoxChild
			this.pluginitem1 = new global::Gustav_Connector.PluginItem ();
			this.pluginitem1.Events = ((global::Gdk.EventMask)(256));
			this.pluginitem1.Name = "pluginitem1";
			this.vAdaptersContainer.Add (this.pluginitem1);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.vAdaptersContainer [this.pluginitem1]));
			w3.Position = 0;
			w3.Expand = false;
			w3.Fill = false;
			// Container child vAdaptersContainer.Gtk.Box+BoxChild
			this.pluginitem2 = new global::Gustav_Connector.PluginItem ();
			this.pluginitem2.Events = ((global::Gdk.EventMask)(256));
			this.pluginitem2.Name = "pluginitem2";
			this.vAdaptersContainer.Add (this.pluginitem2);
			global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.vAdaptersContainer [this.pluginitem2]));
			w4.Position = 1;
			w4.Expand = false;
			w4.Fill = false;
			// Container child vAdaptersContainer.Gtk.Box+BoxChild
			this.pluginitem3 = new global::Gustav_Connector.PluginItem ();
			this.pluginitem3.Events = ((global::Gdk.EventMask)(256));
			this.pluginitem3.Name = "pluginitem3";
			this.vAdaptersContainer.Add (this.pluginitem3);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.vAdaptersContainer [this.pluginitem3]));
			w5.Position = 2;
			w5.Expand = false;
			w5.Fill = false;
			w2.Add (this.vAdaptersContainer);
			this.GtkScrolledWindow.Add (w2);
			this.vbox2.Add (this.GtkScrolledWindow);
			global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.GtkScrolledWindow]));
			w8.Position = 1;
			// Container child vbox2.Gtk.Box+BoxChild
			this.btnAddReader = new global::Gtk.Button ();
			this.btnAddReader.CanFocus = true;
			this.btnAddReader.Name = "btnAddReader";
			this.btnAddReader.UseUnderline = true;
			this.btnAddReader.Label = global::Mono.Unix.Catalog.GetString ("Datenbank-Adapter hinzufügen...");
			this.vbox2.Add (this.btnAddReader);
			global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.btnAddReader]));
			w9.Position = 2;
			w9.Expand = false;
			w9.Fill = false;
			this.hbox1.Add (this.vbox2);
			global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.vbox2]));
			w10.Position = 0;
			// Container child hbox1.Gtk.Box+BoxChild
			this.vbox5 = new global::Gtk.VBox ();
			this.vbox5.Name = "vbox5";
			this.vbox5.Spacing = 6;
			// Container child vbox5.Gtk.Box+BoxChild
			this.label3 = new global::Gtk.Label ();
			this.label3.Name = "label3";
			this.label3.LabelProp = global::Mono.Unix.Catalog.GetString ("Plugins");
			this.vbox5.Add (this.label3);
			global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.vbox5 [this.label3]));
			w11.Position = 0;
			w11.Expand = false;
			w11.Fill = false;
			// Container child vbox5.Gtk.Box+BoxChild
			this.GtkScrolledWindow1 = new global::Gtk.ScrolledWindow ();
			this.GtkScrolledWindow1.Name = "GtkScrolledWindow1";
			this.GtkScrolledWindow1.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow1.Gtk.Container+ContainerChild
			global::Gtk.Viewport w12 = new global::Gtk.Viewport ();
			w12.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child GtkViewport1.Gtk.Container+ContainerChild
			this.vPluginsContainer = new global::Gtk.VBox ();
			this.vPluginsContainer.Name = "vPluginsContainer";
			this.vPluginsContainer.Spacing = 6;
			w12.Add (this.vPluginsContainer);
			this.GtkScrolledWindow1.Add (w12);
			this.vbox5.Add (this.GtkScrolledWindow1);
			global::Gtk.Box.BoxChild w15 = ((global::Gtk.Box.BoxChild)(this.vbox5 [this.GtkScrolledWindow1]));
			w15.Position = 1;
			// Container child vbox5.Gtk.Box+BoxChild
			this.btnAddPlugin = new global::Gtk.Button ();
			this.btnAddPlugin.CanFocus = true;
			this.btnAddPlugin.Name = "btnAddPlugin";
			this.btnAddPlugin.UseUnderline = true;
			this.btnAddPlugin.Label = global::Mono.Unix.Catalog.GetString ("Plugin hinzufügen...");
			this.vbox5.Add (this.btnAddPlugin);
			global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.vbox5 [this.btnAddPlugin]));
			w16.Position = 2;
			w16.Expand = false;
			w16.Fill = false;
			this.hbox1.Add (this.vbox5);
			global::Gtk.Box.BoxChild w17 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.vbox5]));
			w17.Position = 1;
			// Container child hbox1.Gtk.Box+BoxChild
			this.vbox6 = new global::Gtk.VBox ();
			this.vbox6.Name = "vbox6";
			this.vbox6.Spacing = 6;
			// Container child vbox6.Gtk.Box+BoxChild
			this.label4 = new global::Gtk.Label ();
			this.label4.Name = "label4";
			this.label4.LabelProp = global::Mono.Unix.Catalog.GetString ("Aktionen");
			this.vbox6.Add (this.label4);
			global::Gtk.Box.BoxChild w18 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.label4]));
			w18.Position = 0;
			w18.Expand = false;
			w18.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnStartPreview = new global::Gtk.Button ();
			this.btnStartPreview.CanFocus = true;
			this.btnStartPreview.Name = "btnStartPreview";
			this.btnStartPreview.UseUnderline = true;
			this.btnStartPreview.Label = global::Mono.Unix.Catalog.GetString ("Verarbeitung durchführen (ohne Gustav Import)");
			this.vbox6.Add (this.btnStartPreview);
			global::Gtk.Box.BoxChild w19 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnStartPreview]));
			w19.Position = 1;
			w19.Expand = false;
			w19.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnReadersPreview = new global::Gtk.Button ();
			this.btnReadersPreview.CanFocus = true;
			this.btnReadersPreview.Name = "btnReadersPreview";
			this.btnReadersPreview.UseUnderline = true;
			this.btnReadersPreview.Label = global::Mono.Unix.Catalog.GetString ("Datenstand nach Import...");
			this.vbox6.Add (this.btnReadersPreview);
			global::Gtk.Box.BoxChild w20 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnReadersPreview]));
			w20.Position = 2;
			w20.Expand = false;
			w20.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnPluginsPreview = new global::Gtk.Button ();
			this.btnPluginsPreview.CanFocus = true;
			this.btnPluginsPreview.Name = "btnPluginsPreview";
			this.btnPluginsPreview.UseUnderline = true;
			this.btnPluginsPreview.Label = global::Mono.Unix.Catalog.GetString ("Datenstand nach Plugins...");
			this.vbox6.Add (this.btnPluginsPreview);
			global::Gtk.Box.BoxChild w21 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnPluginsPreview]));
			w21.Position = 3;
			w21.Expand = false;
			w21.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnStartAll = new global::Gtk.Button ();
			this.btnStartAll.CanFocus = true;
			this.btnStartAll.Name = "btnStartAll";
			this.btnStartAll.UseUnderline = true;
			this.btnStartAll.Label = global::Mono.Unix.Catalog.GetString ("Verarbeitung durchführen (mit Gustav Import)");
			this.vbox6.Add (this.btnStartAll);
			global::Gtk.Box.BoxChild w22 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnStartAll]));
			w22.Position = 4;
			w22.Expand = false;
			w22.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.hseparator1 = new global::Gtk.HSeparator ();
			this.hseparator1.Name = "hseparator1";
			this.vbox6.Add (this.hseparator1);
			global::Gtk.Box.BoxChild w23 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.hseparator1]));
			w23.Position = 5;
			w23.Expand = false;
			w23.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnCheck = new global::Gtk.Button ();
			this.btnCheck.CanFocus = true;
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.UseUnderline = true;
			this.btnCheck.Label = global::Mono.Unix.Catalog.GetString ("Daten vergleichen");
			this.vbox6.Add (this.btnCheck);
			global::Gtk.Box.BoxChild w24 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnCheck]));
			w24.Position = 6;
			w24.Expand = false;
			w24.Fill = false;
			// Container child vbox6.Gtk.Box+BoxChild
			this.btnDeleteOnServer = new global::Gtk.Button ();
			this.btnDeleteOnServer.CanFocus = true;
			this.btnDeleteOnServer.Name = "btnDeleteOnServer";
			this.btnDeleteOnServer.UseUnderline = true;
			this.btnDeleteOnServer.Label = global::Mono.Unix.Catalog.GetString ("Auf Server löschen...");
			this.vbox6.Add (this.btnDeleteOnServer);
			global::Gtk.Box.BoxChild w25 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.btnDeleteOnServer]));
			w25.Position = 7;
			w25.Expand = false;
			w25.Fill = false;
			this.hbox1.Add (this.vbox6);
			global::Gtk.Box.BoxChild w26 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.vbox6]));
			w26.Position = 2;
			this.Add (this.hbox1);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
			this.btnAddReader.Clicked += new global::System.EventHandler (this.OnBtnAddReaderClicked);
			this.btnAddPlugin.Clicked += new global::System.EventHandler (this.OnBtnAddPluginClicked);
			this.btnStartPreview.Clicked += new global::System.EventHandler (this.OnBtnStartPreviewClicked);
			this.btnReadersPreview.Clicked += new global::System.EventHandler (this.OnBtnReadersPreviewClicked);
			this.btnPluginsPreview.Clicked += new global::System.EventHandler (this.OnBtnPluginsPreviewClicked);
			this.btnStartAll.Clicked += new global::System.EventHandler (this.OnBtnStartAllClicked);
			this.btnCheck.Clicked += new global::System.EventHandler (this.OnBtnCheckClicked);
			this.btnDeleteOnServer.Clicked += new global::System.EventHandler (this.OnBtnDeleteOnServerClicked);
		}
	}
}
