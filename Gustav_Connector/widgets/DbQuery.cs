using System;
using System.Collections;
using System.Collections.Generic;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class DbQuery : Gtk.Bin
	{
		public ReaderColumnInfo[] SelectColumns { get; set; }
		public ReaderColumnInfo[] WhereColumns { get; set; }

		public DbQuery ()
		{
			this.Build ();
		}

		public void ShowColumns(DbQueryData data)
		{
			uint index = 0;

			/*
			 * SELECT
			 */
			foreach (ReaderColumnInfo col in this.SelectColumns) {

				// Label
				var label = new Gtk.Label (col.Title);

				// Entry
				var entry = new Gtk.Entry ();
				entry.Name = "select_" + col.ColumnName;
				if(data.SelectColumns.ContainsKey(col.ColumnName))
					entry.Text = data.SelectColumns [col.ColumnName];

				// Add to table
				this._InsertRow (index);
				table1.Attach (label, 0, 1, index, index+1);
				table1.Attach (entry, 1, 2, index, index+1);
				index++;
			}

			index += 2;

			/*
			 * WHERE
			 */
			foreach (ReaderColumnInfo col in this.WhereColumns) {

				// Label
				var lblText = String.Format ("{0} (@{1})", col.Title, col.ColumnName);
				var label = new Gtk.Label (lblText);

				// Entry
				var entry = new Gtk.Entry ();
				entry.Name = "where_" + col.ColumnName;
				if(data.WhereColumns.ContainsKey(col.ColumnName))
					entry.Text = data.WhereColumns [col.ColumnName];

				// Add to table
				this._InsertRow (index);
				table1.Attach (label, 0, 1, index, index+1);
				table1.Attach (entry, 1, 2, index, index+1);
				index++;
			}

			/*
			 * Other fields
			 */
			if(data.SelectColumns.ContainsKey("others"))
				this.select_others.Buffer.Text = data.SelectColumns ["others"];
			this.from.Buffer.Text = data.From;
			if(data.WhereColumns.ContainsKey("others"))
				this.where_others.Buffer.Text = data.WhereColumns ["others"];

			/*
			 * Show all
			 */
			table1.ShowAll ();
		}

		public DbQueryData GetQuery()
		{
			var query = new DbQueryData ();
			query.SelectColumns = new Dictionary<string, string> ();
			query.WhereColumns = new Dictionary<string, string> ();

			ArrayList children = (ArrayList)table1.AllChildren;

			for(int i=children.Count-1; i>=0; i--) {
				Gtk.Widget w = (Gtk.Widget)children [i];
				var child = (Gtk.Table.TableChild)table1 [w];
				var keys = child.Child.Name.Split ("_".ToCharArray(), 2);
				var key_name = keys [0];

				var value = "";
				if (child.Child is Gtk.ScrolledWindow) {
					var scrolledwindow = (Gtk.ScrolledWindow)child.Child;
					value = ((Gtk.TextView)scrolledwindow.Child).Buffer.Text;
					keys = scrolledwindow.Child.Name.Split ("_".ToCharArray(), 2);
					key_name = keys [0];
				}
				else if (child.Child is Gtk.Entry)
					value = ((Gtk.Entry)child.Child).Text;

				if (key_name == "select") {
					query.SelectColumns.Add (keys [1], value);
				}

				if (key_name == "from") {
					query.From = value;
				}

				if (key_name == "where") {
					query.WhereColumns.Add (keys [1], value);
				}
			}



			return query;
		}

		private void _InsertRow(uint index)
		{
			table1.NRows++;

			foreach (Gtk.Widget widget in table1.AllChildren) {

				var child = (Gtk.Table.TableChild)table1 [widget];

				if (child.TopAttach >= index) {
					child.BottomAttach++;
					child.TopAttach++;
				}
			}
		}
	}
}

