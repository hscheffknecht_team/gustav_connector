using System;
using Gtk;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class PluginItem : Gtk.Bin
	{
		public PluginBase CurrentPlugin { get; set; }
		public SettingsBase CurrentSettings { get; set; }
		public TabContent CurrentContainer { get; set; }

		public PluginItem ()
		{
			this.Build ();
		}

		public void Refresh()
		{
			this.lblName.Text = this.CurrentPlugin.Name;
			this.lblStatus.Text = this.CurrentPlugin.GetStatusInfo ();
			this.lblFilter.Text = this.CurrentPlugin.GetFilterInfo ();
		}

		protected void OnBtnSettingsClicked (object sender, EventArgs e)
		{
			if (this.CurrentPlugin is CommonPluginBase) {

				var dialog = new CommonPluginDialog ();
				var settWidget = (PluginSettingsWidget)Activator.CreateInstance (((CommonPluginBase)this.CurrentPlugin).SettingsWidgetType);
				dialog.SettingsWidget = settWidget;
				settWidget.CurrentPlugin = this.CurrentPlugin;
				settWidget.CurrentProcessor = this.CurrentContainer.Processor;
				dialog.Load ();
				dialog.Run ();
				dialog.Destroy ();

			} else if (this.CurrentPlugin is ReaderPluginBase) {

				var dialog = (PluginSettingsDialog)Activator.CreateInstance (((ReaderPluginBase)this.CurrentPlugin).SettingsDialogType);
				dialog.CurrentPlugin = this.CurrentPlugin;
				dialog.SelectColumns = this.CurrentContainer.Processor.GetSelectColumns ();
				dialog.WhereColumns = this.CurrentContainer.Processor.GetWhereColumns ();
				dialog.Load ();
				dialog.Run ();
				dialog.Destroy ();
			}

			this.CurrentSettings.Save ();
			this.CurrentContainer.RefreshReadersAndPlugins ();
		}

		protected void OnBtnFilterClicked (object sender, EventArgs e)
		{
			var dialog = new FilterEditor();
			dialog.CurrentPlugin = this.CurrentPlugin;
			dialog.Run();
			dialog.Destroy();
			this.CurrentSettings.Save ();

			this.CurrentContainer.RefreshReadersAndPlugins ();
		}

		protected void OnBtnDeleteClicked (object sender, EventArgs e)
		{
			var msgbox = new Gtk.MessageDialog (null, DialogFlags.Modal, MessageType.Question, ButtonsType.OkCancel, "Soll die Plugin-Instanz wirklich gelöscht werden?");
			var result = msgbox.Run ();
			msgbox.Destroy ();

			if (result == (int)Gtk.ResponseType.Ok) {
				this.CurrentPlugin.Container.Remove (this.CurrentPlugin);
				this.CurrentSettings.Save ();

				this.CurrentContainer.RefreshReadersAndPlugins ();
			}
		}

		protected void OnBtnMoveUpClicked (object sender, EventArgs e)
		{
			this.CurrentPlugin.Container.MoveUp (this.CurrentPlugin.Container.IndexOf(this.CurrentPlugin));
			this.CurrentSettings.Save ();

			this.CurrentContainer.RefreshReadersAndPlugins ();
		}

		protected void OnBtnMoveDownClicked (object sender, EventArgs e)
		{
			this.CurrentPlugin.Container.MoveDown (this.CurrentPlugin.Container.IndexOf(this.CurrentPlugin));
			this.CurrentSettings.Save ();

			this.CurrentContainer.RefreshReadersAndPlugins ();
		}

		protected void OnBtnFlushCacheClicked (object sender, EventArgs e)
		{
			if (!(this.CurrentPlugin is ReaderPluginBase))
				return;

			var dialog = new FlushCache ();
			dialog.Processor = CurrentContainer.Processor;
			dialog.ReaderPlugin = (ReaderPluginBase)this.CurrentPlugin;
			dialog.Modal = true;
			dialog.ShowAll ();
		}
	}
}
