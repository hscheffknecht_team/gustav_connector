using System;
using System.Collections.Generic;
using System.ComponentModel;
using Gtk;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class TabContent : Gtk.Bin
	{
		private ImportSettings _Settings = null;
		private SettingsCollection _Readers = null;
		private SettingsCollection _Plugins = null;
		private ImportProcessorBase _Processor;
		private BackgroundWorker _Worker = null;
		private bool _PreviewMode;

		public TabContent ()
		{
			this.Build ();
		}

		public ImportProcessorBase Processor { 
			get { return this._Processor; }
			set {
				this._Processor = value;
				this._Settings = this._Processor.Settings;
				this._Readers = this._Settings.Readers;
				this._Plugins = this._Settings.Plugins;

				this._RefreshReadersList ();
				this._RefreshPluginsList ();

				// Workaround because Plugins are not shown after startup
				this.vPluginsContainer.HideAll ();
				this.vPluginsContainer.ShowAll ();
			}
		}

		protected void OnBtnAddReaderClicked (object sender, EventArgs e)
		{
			this._AddPlugin (this._Readers, PluginsBrowser.Current.ReaderPlugins.ToArray());
			_RefreshReadersList ();
		}

		protected void OnBtnAddPluginClicked (object sender, EventArgs e)
		{
			this._AddPlugin (this._Plugins, PluginsBrowser.Current.CommonPlugins.ToArray());
			_RefreshPluginsList ();
		}

		private void _AddPlugin(SettingsCollection pluginsCollection, PluginBase[] pluginsList) 
		{
			var dialog = new PluginChooser();
			dialog.PluginsList = pluginsList;
			dialog.FillPlugins();
			dialog.Run();
			dialog.Destroy();

			if(dialog.SelectedPlugin == null) {
				return;
			}

			var plugin = (PluginBase)Activator.CreateInstance(dialog.SelectedPlugin.GetType());

			if(plugin != null) {
				pluginsCollection.Add (plugin);
				this._Settings.Save ();
			}
		}

		public void RefreshReadersAndPlugins()
		{
			_RefreshReadersList ();
			_RefreshPluginsList ();
		}

		private void _RefreshReadersList()
		{
			_RefreshPlugins (this.vAdaptersContainer, this._Readers);
		}

		private void _RefreshPluginsList()
		{
			_RefreshPlugins (this.vPluginsContainer, this._Plugins);
		}

		private void _RefreshPlugins(Gtk.VBox container, SettingsCollection plugins)
		{
			if(plugins == null)
				return;

			// Clear container
			while(container.Children.Length > 0) {
				container.Remove(container.Children[0]);
			}

			int position = 0;

			foreach (var sett in plugins) {
				// Get plugin
				var plugin = (PluginBase)sett;

				// Add to global list
				PluginsBrowser.Current.AllPluginInstances [plugin.Id] = plugin;

				// Create widget
				var widget = new PluginItem ();
				widget.CurrentContainer = this;
				widget.CurrentPlugin = plugin;
				widget.CurrentSettings = this._Settings;
				container.Add (widget);

				// Set boxchild settings
				var boxchild = (Box.BoxChild)container[widget];
				boxchild.Position = 0;
				boxchild.Expand = false;
				boxchild.Fill = false;
				boxchild.Position = position++;

				// Update widget content
				widget.Refresh ();
			}

			container.ShowAll();
		}

		private void OnBtnReadersPreviewClicked (object sender, EventArgs e)
		{
			var viewer = new TableViewer ();
			viewer.Title = "Datenbank Vorschau";
			viewer.ContentTable = this.Processor.DatabaseTable;
			viewer.LoadContent();
			viewer.ShowAll ();
		}

		private void OnBtnPluginsPreviewClicked (object sender, EventArgs e)
		{
			var viewer = new TableViewer ();
			viewer.Title = "Gustav Vorschau";
			viewer.ContentTable = this.Processor.GustavTable;
			viewer.LoadContent();
			viewer.ShowAll ();
		}

		private void _StartProcessor(bool preview) {

			if (this._Worker != null) {

				var msg = new Gtk.MessageDialog (
					          null,
					          DialogFlags.Modal,
					          MessageType.Error,
					          ButtonsType.Ok,
					          "Es wird bereits ein Import durchgeführt"
				          );
				msg.Run ();
				msg.Destroy ();
				return;
			}

			this._PreviewMode = preview;
			this._Worker = new BackgroundWorker ();
			this._Worker.DoWork += _StartProcessorWork;
			this._Worker.RunWorkerAsync ();
		}

		private void _StartProcessorWork (object sender, DoWorkEventArgs e) {

			Gtk.Application.Invoke (delegate {
				this.btnStartPreview.Sensitive = false;
				this.btnStartAll.Sensitive = false;
			});

			var previewLabel = this.btnStartPreview.Label;
			var allLabel = this.btnStartAll.Label;

			this.Processor.ProgressChanged += _ProcessorProgressChanged;
			this.Processor.Start (this._PreviewMode);

			Gtk.Application.Invoke (delegate {
				this.btnStartPreview.Label = previewLabel;
				this.btnStartAll.Label = allLabel;

				this.btnStartPreview.Sensitive = true;
				this.btnStartAll.Sensitive = true;
			});

			this._Worker = null;
		}

		private void _ProcessorProgressChanged (object sender, EventArgs e) {

			Gtk.Application.Invoke (delegate {

				if (this._PreviewMode) {
					this.btnStartPreview.Label = this.Processor.CurrentPercentage + "%";
			
				} else {
					this.btnStartAll.Label = this.Processor.CurrentPercentage + "%";
				}

			});
		}

		protected void OnBtnStartPreviewClicked (object sender, EventArgs e)
		{
			this._StartProcessor (true);
		}

		protected void OnBtnStartAllClicked (object sender, EventArgs e)
		{
			this._StartProcessor (false);
		}

		protected void OnBtnCheckClicked (object sender, EventArgs e)
		{
			try {
				var result = this._Processor.Check ();

				var dialog = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, MessageType.Info, Gtk.ButtonsType.Ok, result.ToString());
				dialog.Run ();
				dialog.Destroy ();
			} catch (Exception ex) {
				Logger.Current.HandleException (ex, true);
			}
		}

		protected void OnBtnDeleteOnServerClicked (object sender, EventArgs e)
		{
			var dialog = new DeleteOnServer ();
			dialog.Processor = Processor;
			dialog.Modal = true;
			dialog.ShowAll ();
		}
	}
}

