using System;
using System.Xml;

namespace Gustav_Connector
{
	public partial class GustavOrdersViewer : Gtk.Dialog
	{
		public GustavOrdersViewer ()
		{
			if(SoapDataCache.Orders == null) {
				this.Hide();
				return;
			}

			this.Build ();
			this._InitTreeView();
			this._FillTreeView(GetOrders(), null, null);
		}

        private XmlNode GetOrders() {

            switch(ddType.Active)
            {
                case 0:
                    return SoapDataCache.Orders;
                case 1:
                    return SoapDataCache.FinalOrders;
                default:
                    return null;
            }
        }

		private void _InitTreeView() 
		{
			// Define name column
			var nameColumn = new Gtk.TreeViewColumn();
			nameColumn.Title = "Name";
			var nameRenderer = new Gtk.CellRendererText();
			nameColumn.PackStart(nameRenderer, true);
			this.treeview.AppendColumn(nameColumn);
			nameColumn.AddAttribute(nameRenderer, "text", 0);

			// Define value column
			var valueColumn = new Gtk.TreeViewColumn();
			valueColumn.Title = "Wert";
			var valueRenderer = new Gtk.CellRendererText();
			valueColumn.PackStart(valueRenderer, true);
			this.treeview.AppendColumn(valueColumn);
			valueColumn.AddAttribute(valueRenderer, "text", 1);
		}

		private void _FillTreeView(XmlNode node, Gtk.TreeStore treeStore, Gtk.TreeIter? parent) 
		{
			if(treeStore == null) {
				treeStore = new Gtk.TreeStore(typeof(String), typeof(String));
			}

			// Load data
			if(node.HasChildNodes && node.FirstChild is XmlElement) {

				// Node with childs (group)

				var groupIter = new Gtk.TreeIter();

				if(parent == null) {
					groupIter = treeStore.AppendValues(node.LocalName);
				} else {
					groupIter = treeStore.AppendValues((Gtk.TreeIter)parent, node.LocalName);
				}

				foreach(XmlNode child in node.ChildNodes)
				{
					_FillTreeView(child, treeStore, groupIter);
				}

			} else {

				// Last node (value)

				if(parent == null) {
					treeStore.AppendValues(node.LocalName, node.InnerText);
				} else {
					treeStore.AppendValues((Gtk.TreeIter)parent, node.LocalName, node.InnerText);
				}
			}

			this.treeview.Model = treeStore;
			this.treeview.ExpandAll();
		}

		protected void OnBtnCloseClicked (object sender, EventArgs e)
		{
			this.Hide();
		}

        protected void OnDdTypeChanged (object sender, EventArgs e)
        {
            this._FillTreeView(GetOrders(), null, null);
        }
	}
}

