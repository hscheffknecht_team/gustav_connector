using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public partial class TargetTablesEditor : Gtk.Dialog
	{
		public TargetTablesEditor ()
		{
			this.Build ();
		}

		public new string Title {

			get { return base.Title; }
			set { base.Title = value; }
		}

		public DataTable ContentTable { get; set; }

		private List<ColumnInfo> _Columns;

		public List<ColumnInfo> Columns { 
			get { return this._Columns; }
			set { this._Columns = value; this.Load();}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			try {
				this.Save();
				this.Hide();
			} 
			catch(Exception ex) {
				Logger.Current.HandleException(ex, true);
			}
		}

		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Columns = null;
			this.Hide();
		}

		protected void OnBtnAddColumnClicked (object sender, EventArgs e)
		{
			this.AddColumn("Spalte " + tblColumns.NRows, "string", false, false);
		}

		protected void OnBtnDeleteColumnClicked (object sender, EventArgs e)
		{
			// Get row number
			var buttonChild =  (Gtk.Table.TableChild)tblColumns[(Gtk.Widget)sender];
			int rowNr = (int)buttonChild.TopAttach;

			// Move or delete widgets
			foreach(Gtk.Widget w in tblColumns.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblColumns[w];

				if(child.TopAttach > rowNr)
				{
					child.TopAttach--;
					child.BottomAttach--;
				} 
				else if(child.TopAttach == rowNr)
				{
					tblColumns.Remove(child.Child);
				}
			}

			// Remove row
			tblColumns.NRows--;
		}

		private void AddColumn(string name, string type, bool isPrimaryKey, bool isInternal)
		{
			// Column name
			var columnName = new Gtk.Entry(name);
			columnName.Name = "colname_" + tblColumns.NRows;

			// Column type
			var values = new String[] { "string", "int", "decimal" };
			var columnType = new Gtk.ComboBox(values);
			columnType.Name = "coltype_" + tblColumns.NRows;

			var i=0;
			foreach(string value in values) 
			{
				if(value == type) {
					columnType.Active = i;
				}

				i++;
			}

			// Primary key
			var primary = new Gtk.CheckButton("Ja");
			primary.Name = "primary_" + tblColumns.NRows;
			primary.Active = isPrimaryKey;
			
			// Internal
			var intern = new Gtk.CheckButton("Ja");
			intern.Name = "internal_" + tblColumns.NRows;
			intern.Active = isInternal;
			
			// Delete Button
			var deleteButton = new Gtk.Button();
			deleteButton.Label = "Löschen";
			deleteButton.Clicked += OnBtnDeleteColumnClicked;
			deleteButton.Name = "btnDeleteColumn_" + tblColumns.NRows;
			
			tblColumns.NRows++;
			tblColumns.Attach (columnName, 0, 1, tblColumns.NRows-1, tblColumns.NRows);
			tblColumns.Attach (columnType, 1, 2, tblColumns.NRows-1, tblColumns.NRows);
			tblColumns.Attach (primary, 2, 3, tblColumns.NRows-1, tblColumns.NRows);
			tblColumns.Attach (intern, 3, 4, tblColumns.NRows-1, tblColumns.NRows);
			tblColumns.Attach (deleteButton, 4, 5, tblColumns.NRows-1, tblColumns.NRows);
			tblColumns.ShowAll();
		}

		private void Load()
		{
			if(this.Columns != null)
			{
				foreach(ColumnInfo col in this.Columns)
				{
					this.AddColumn(col.Name, col.Type.ToString().ToLower(), col.IsPrimaryKey, col.IsInternal);
				}

				ShowTable();
			}
			tblColumns.HideAll();
			tblColumns.ShowAll();
		}
		
		private void Save() 
		{
			// Create list with ColumnInfo instances
			var cols = new List<ColumnInfo>((int)tblColumns.NRows-1);
			while(cols.Count < cols.Capacity) {
				cols.Add(new ColumnInfo());
			}

			// Fill ColumnInfo instances from table widgets
			foreach(Gtk.Widget w in tblColumns.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblColumns[w];
				var keys = child.Child.Name.Split('_');
				var key_name = keys[0];
				var row = (int)child.TopAttach;

				switch(key_name)
				{
				case "colname":
					cols[row-1].Name = ((Gtk.Entry)child.Child).Text;
					break;
					
				case "coltype":
					cols[row-1].Type = (ColumnType)Enum.Parse(typeof(ColumnType), ((Gtk.ComboBox)child.Child).ActiveText.ToUpper());
					break;
					
				case "primary":
					cols[row-1].IsPrimaryKey = ((Gtk.CheckButton)child.Child).Active;
					break;
					
				case "internal":
					cols[row-1].IsInternal = ((Gtk.CheckButton)child.Child).Active;
					break;
				}


			}

			this.Columns = cols;
		}

		private void ShowTable()
		{
			if(this._Columns == null)
				return;

			var types = new List<System.Type>();
			var i=0;

			foreach(ColumnInfo col in this._Columns)
			{
				// Add type
				switch(col.Type)
				{
				case ColumnType.DECIMAL:
					types.Add(typeof(Decimal));
					break;
				case ColumnType.INT:
					types.Add(typeof(Int32));
					break;
				case ColumnType.STRING:
					types.Add(typeof(String));
					break;
				}

				treeview1.AppendColumn (col.Name, new Gtk.CellRendererText (), "text", i);
				i++;
			}
			Gtk.ListStore musicListStore = new Gtk.ListStore (types.ToArray());

			if(this.ContentTable != null)
			{
				foreach(DataRow row in this.ContentTable.Rows)
				{
					musicListStore.AppendValues(row.ItemArray);
				}	
			}
			
			treeview1.Model = musicListStore;
		}

		protected void OnNotebook1SwitchPage (object o, Gtk.SwitchPageArgs args)
		{
			tblColumns.HideAll();
			tblColumns.ShowAll();
		}
	}
}

