﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Gustav_Connector
{
	public partial class WinlineImagesSaveDialog : Gtk.Dialog
	{
		private List<string> _InvalidFilenames;
		public WinlineImagesSaveDialog ()
		{
			this.Build ();
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			if (!_Validate ()) {
				return;
			}

			var thread = new Thread (new ThreadStart (this._RunWorkerThread));
			thread.Start ();

		}

		private void _RunWorkerThread() {

			try {

				this.buttonOk.Sensitive = this.buttonCancel.Sensitive = false;
				_ExportAllImages ();

				Gtk.Application.Invoke (delegate {
					var msg = "Bilder wurden erfolgreich abgespeichert";
					if(this._InvalidFilenames.Count > 0) {
						msg += ", Bilder für folgende Artikel konnten nicht exportiert werden: " + String.Join(", ", this._InvalidFilenames);
					}
					Common.ShowInfoDialog (this, msg);
				});

			} catch(Exception ex) {
				Gtk.Application.Invoke (delegate {
					Common.ShowErrorDialog (this, ex.Message);
				});
			}

			this.buttonOk.Sensitive = this.buttonCancel.Sensitive = true;
		}

		private bool _Validate() {

			if (this.tServer.Text == "" || this.tDataDbName.Text == "" || this.tUser.Text == "" || this.tPassword.Text == "" ||
			   this.tDirectory.Text == "") {
				Common.ShowErrorDialog (this, "Es müssen alle Felder ausgefüllt werden");
				return false;
			}

			return true;
		}

		private void _ExportAllImages() {

			// Check folder
			if (!Directory.Exists (this.tDirectory.Text)) {
				Directory.CreateDirectory (this.tDirectory.Text);
			}

			// Create DB connection
			var dbConn = new SqlConnection (_GetConnectionString ());
			dbConn.Open ();

			var dbCmd = new SqlCommand (_GetQuery (true), dbConn);
			dbCmd.Parameters.Add ("@mesocomp", SqlDbType.Char, 4).Value = this.tMesoComp.Text;
			var cnt = (int)dbCmd.ExecuteScalar ();

			// Receive data
			dbCmd.CommandText = _GetQuery (false);
			var dr = dbCmd.ExecuteReader ();

			// Export images
			var i = 0;
			this._InvalidFilenames = new List<string> ();
			Gtk.Application.Invoke (delegate {
				this.progressbar1.Fraction = 0;
			});

			while (dr.Read ()) {

				i++;

				// Check filename
				var filename = (string)dr ["artnr"] + System.IO.Path.GetExtension ((string)dr ["filename"]).ToLower ();
				filename = filename.Replace ("/", "&slash;");
				if (!this._CheckFilename (filename)) {
					this._InvalidFilenames.Add (filename);
					continue;
				}

				// Write file
				var path = System.IO.Path.Combine (this.tDirectory.Text, filename);
				File.WriteAllBytes (path, (byte[])dr ["content"]);

				// Update progress bar
				Gtk.Application.Invoke (delegate {
					this.progressbar1.Fraction = ((double)i / cnt);
				});
			}

			// Close connections
			dr.Close ();
			dbConn.Close ();
		}

		private bool _CheckFilename(string filename) {

			var invalidChars = System.IO.Path.GetInvalidFileNameChars ();
			foreach (char c in filename) {
				foreach (char ic in invalidChars) {
					if (c == ic) {
						return false;
					}
				}
			}

			return true;
		}

		private string _GetQuery(bool count) {

			// Get DB query
			return String.Format (
				@"
				SELECT {2}
				FROM {0}..t022cmp b
				INNER JOIN {1}..T031 t on b.c000 = t.c076 and t.mesocomp=@mesocomp and t.mesoyear=(
					SELECT MAX(mesoyear) FROM {1}..T001 where mesocomp=@mesocomp
				)
				INNER JOIN {1}..T024 p on p.c092 = t.c016 AND t.mesocomp = p.mesocomp AND t.mesoyear = p.mesoyear
				WHERE p.c038 IS NULL AND p.c014 < 2",
				this.tSystemDbName.Text,
				this.tDataDbName.Text,
				count ? "count(*) as cnt" : "b.mesobin as content, b.c000 as filename, p.c002 as artnr"
			);
		}

		private string _GetConnectionString() {

			// Build connection string
			var builder = new SqlConnectionStringBuilder();
			builder.DataSource = this.tServer.Text;
			builder.InitialCatalog = this.tDataDbName.Text;

			if (!String.IsNullOrEmpty(this.tUser.Text) || !String.IsNullOrEmpty(this.tPassword.Text)) {
				builder.UserID = this.tUser.Text;
				builder.Password = this.tPassword.Text;
			} else {
				builder.IntegratedSecurity = true;
			}

			return builder.ConnectionString;
		}
	}
}

