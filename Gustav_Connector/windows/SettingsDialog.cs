using System;

namespace Gustav_Connector
{
	public partial class SettingsDialog : Gtk.Dialog
	{
		public SettingsDialog ()
		{
			this.Build ();
			Init();
		}

		private void Init() 
		{
			this.txtGustavAddress.Text = GlobalSettings.ServerAddress;
			this.txtApiKey.Text = GlobalSettings.ApiKey;
			this.txtSecureToken.Text = GlobalSettings.ApiSecureToken;
			this.txtOrderNr.Text = GlobalSettings.LastOrderApproveNumber;
			this.txtCheckDate.Text = GlobalSettings.LastCheckDate;
			this.chkErrorMails.Active = GlobalSettings.SendErrorMails;
			this.chkSkipUploads.Active = GlobalSettings.SkipUpload;
			this.chkDoChecks.Active = GlobalSettings.DoChecks;
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			GlobalSettings.ServerAddress = txtGustavAddress.Text;
			GlobalSettings.ApiKey = txtApiKey.Text;
			GlobalSettings.ApiSecureToken = txtSecureToken.Text;
			GlobalSettings.LastOrderApproveNumber = txtOrderNr.Text;
			GlobalSettings.LastCheckDate = txtCheckDate.Text;
			GlobalSettings.SendErrorMails = chkErrorMails.Active;
			GlobalSettings.SkipUpload = chkSkipUploads.Active;
			GlobalSettings.DoChecks = chkDoChecks.Active;
			GlobalSettings.Save();

			this.Hide();

			// Flush cache
			SoapDataCache.Clear();
		}

		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Hide();
		}
	}
}

