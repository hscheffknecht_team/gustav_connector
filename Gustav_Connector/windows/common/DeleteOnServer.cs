﻿using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class DeleteOnServer : Gtk.Window
	{
		public ImportProcessorBase Processor { get; set; }

		public DeleteOnServer () :
			base (Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}

		protected void OnBtnOkClicked (object sender, EventArgs e)
		{
			// Collect non-empty, unique keys
			var lines = this.txtKeys.Buffer.Text.Split ("\r\n".ToCharArray ());
			var keys = new List<Dictionary<string,string>> ();
			foreach (string key in lines) {
				if (String.IsNullOrWhiteSpace (key))
					continue;

				var item = new Dictionary<string,string> ();
				item.Add ("key", key);
				keys.Add (item);
			}

			// Delete on server
			if (keys.Count > 0)
				Processor.DeleteFromServer (keys.ToArray ());

			// Show message
			var msg = String.Format("{0} Löschanforderungen wurden an den Server übermittelt", keys.Count);
			var dialog = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, Gtk.MessageType.Info, Gtk.ButtonsType.Ok, msg);
			dialog.Run ();
			dialog.Destroy ();

			// Close
			this.Destroy ();
		}

		protected void OnBtnCancelClicked (object sender, EventArgs e)
		{
			this.Destroy ();
		}
	}
}
