using Gtk;
using System;

namespace Gustav_Connector
{
	public partial class CommonPluginDialog : Gtk.Dialog
	{

		public PluginSettingsWidget SettingsWidget;

		public CommonPluginDialog()
		{
			this.Build();
		}

		public void Load()
		{
			this.VBox.Add (this.SettingsWidget);
			this.VBox.ShowAll ();

			this.SettingsWidget.Load ();
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			this.SettingsWidget.Save ();
			this.Destroy ();
		}

		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Destroy ();
		}
	}
}

