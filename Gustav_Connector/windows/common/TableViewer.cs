﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public partial class TableViewer : Gtk.Dialog
	{
		public TableViewer ()
		{
			this.Build ();
		}

		public new string Title {
			get { return base.Title; }
			set { base.Title = value; }
		}

		public DataTable ContentTable { get; set; }

		public void LoadContent()
		{
			if (this.ContentTable == null)
				return;

			var types = new List<System.Type>();
			var i=0;
			foreach(DataColumn col in this.ContentTable.Columns)
			{
				types.Add (typeof(String));
				treeview1.AppendColumn (col.ColumnName.Replace("_", "__"), new Gtk.CellRendererText (), "text", i);
				i++;
			}

			Gtk.ListStore musicListStore = new Gtk.ListStore (types.ToArray());
			if(this.ContentTable != null)
			{
				foreach(DataRow row in this.ContentTable.Rows)
				{
					var data = row.ItemArray;

					for(int j=0; j<data.Length; j++) 
					{
						data[j] = data[j].ToString();
					}

					musicListStore.AppendValues(data);
				}	
			}
			treeview1.Model = musicListStore;
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			this.Destroy ();
		}
	}
}

