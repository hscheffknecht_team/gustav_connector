﻿using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class FlushCache : Gtk.Window
	{
		public ImportProcessorBase Processor { get; set; }
		public ReaderPluginBase ReaderPlugin { get; set; }

		public FlushCache () :
			base (Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}

		protected void OnBtnOkClicked (object sender, EventArgs e)
		{
			if (this.txtKeys.Buffer.Text == "*")
				FlushAll ();
			else
				FlushKeys ();

			this.Destroy ();
		}

		private void FlushKeys()
		{
			// Collect non-empty, unique keys
			var keys = new List<string> ();
			var lines = this.txtKeys.Buffer.Text.Split ("\r\n".ToCharArray ());
			foreach (string key in lines) {
				if (String.IsNullOrWhiteSpace (key))
					continue;

				if (!keys.Contains (key))
					keys.Add (key);
			}

			// Flush cache i tems
			ReaderPlugin.FlushCacheItems(keys.ToArray());

			// Show message
			var msg = String.Format("{0} Cache-Einträge wurden gelöscht", keys.Count);
			var dialog = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, Gtk.MessageType.Info, Gtk.ButtonsType.Ok, msg);
			dialog.Run ();
			dialog.Destroy ();
		}

		private void FlushAll()
		{
			// Flush all cache items
			ReaderPlugin.FlushCache ();	

			// Show message
			var dialog = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "Komplette Cache wurde gelöscht");
			dialog.Run ();
			dialog.Destroy ();
		}

		protected void OnBtnCancelClicked (object sender, EventArgs e)
		{
			this.Destroy ();
		}
	}
}

