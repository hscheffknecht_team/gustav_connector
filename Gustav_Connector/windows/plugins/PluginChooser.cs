using System;

namespace Gustav_Connector
{
	public partial class PluginChooser : Gtk.Dialog
	{
		public PluginBase SelectedPlugin { get; set; }
		public PluginBase[] PluginsList { get; set; }

		public PluginChooser ()
		{
			this.Build ();
		}

		public void FillPlugins()
		{
			// Create table
			var table = new Gtk.Table((uint)PluginsList.Length+1, 3, false);
			table.BorderWidth = 10;
			table.ColumnSpacing = 10;
			this.VBox.Add(table);
			((Gtk.VBox.BoxChild)this.VBox[table]).Fill = false;
			((Gtk.VBox.BoxChild)this.VBox[table]).Expand = false;

			var i=0;

			foreach(var plugin in PluginsList)
			{
				// Create button
				var button = new Gtk.Button();
				button.Label = plugin.Name;
				button.Name = "plugin_" + i;
				button.Clicked += ButtonClicked;
				table.Attach(button, 0, 1, (uint)i, (uint)i+1);

				// Create description label
				var descr = new Gtk.Label();
				descr.Text = plugin.Description;
				table.Attach(descr, 1, 2, (uint)i, (uint)i+1);

				i++;
			}

			table.ShowAll();
		}

		private void ButtonClicked (object sender, EventArgs e)
		{
			int index = Int32.Parse(((Gtk.Button)sender).Name.Split('_')[1]);
			this.SelectedPlugin = PluginsList[index];
			this.Destroy();
		}
	}
}

