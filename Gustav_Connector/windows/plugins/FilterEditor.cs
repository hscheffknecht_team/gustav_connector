using System;

namespace Gustav_Connector
{
	public partial class FilterEditor : Gtk.Dialog
	{
		private string[] _SourceColumns;

		private PluginBase _CurrentPlugin;
		public PluginBase CurrentPlugin { 
			get {
				return this._CurrentPlugin;
			}

			set { 
			
				this._CurrentPlugin = value;				
				_Load();
			}
		}

		public FilterEditor ()
		{
			this.Build ();

			// Load Source columns
			this._SourceColumns = SoapDataCache.GetOrderFields(true);
			Gtk.ListStore storeSource1 = (Gtk.ListStore)this.cmbSource1.Model;
			Gtk.ListStore storeSource2 = (Gtk.ListStore)this.cmbSource2.Model;
			Gtk.ListStore storeValue1 = (Gtk.ListStore)this.cmbValue1.Model;
			Gtk.ListStore storeValue2 = (Gtk.ListStore)this.cmbValue2.Model;

			foreach (var item in this._SourceColumns) {
				storeSource1.AppendValues (item);
				storeSource2.AppendValues (item);
				storeValue1.AppendValues (item);
				storeValue2.AppendValues (item);
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			_Save();
		}

		private void _Load()
		{
			// Source fields
			int i=0;
			foreach(string item in this._SourceColumns)
			{
				if(item == this.CurrentPlugin.FilterField1)
					cmbSource1.Active = i;
				if(item == this.CurrentPlugin.FilterField2)
					cmbSource2.Active = i;

				i++;
			}

			// Operators
			switch(this.CurrentPlugin.FilterOperator1)
			{
			case "=":
				this.cmbOperator1.Active = 0;
				break;
			case "!=":
				this.cmbOperator1.Active = 1;
				break;
			case "StartsWith":
				this.cmbOperator1.Active = 2;
				break;
			}

			switch(this.CurrentPlugin.FilterOperator2)
			{
				case "=":
					this.cmbOperator2.Active = 0;
					break;
				case "!=":
					this.cmbOperator2.Active = 1;
					break;
				case "StartsWith":
					this.cmbOperator2.Active = 2;
					break;
			}

			// Values
			txtValue1.Text = this.CurrentPlugin.FilterValue1;
			txtValue2.Text = this.CurrentPlugin.FilterValue2;


			// Source fields
			i=0;
			foreach(string item in this._SourceColumns)
			{
				if(item == this.CurrentPlugin.FilterValueField1)
					cmbValue1.Active = i;
				if(item == this.CurrentPlugin.FilterValueField2)
					cmbValue2.Active = i;

				i++;
			}

			// Temp. table
			if (this.CurrentPlugin is ReaderPluginBase) {
				txtTmpTableName.Text = ((ReaderPluginBase)this.CurrentPlugin).TemporaryTableName;
			}
			txtTmpTableName.Sensitive = this.CurrentPlugin is ReaderPluginBase;
		}

		private void _Save()
		{
			this.CurrentPlugin.FilterField1 = cmbSource1.ActiveText;
			this.CurrentPlugin.FilterValue1 = txtValue1.Text;
			this.CurrentPlugin.FilterValueField1 = cmbValue1.ActiveText;
			this.CurrentPlugin.FilterField2 = cmbSource2.ActiveText;
			this.CurrentPlugin.FilterValue2 = txtValue2.Text;
			this.CurrentPlugin.FilterValueField2 = cmbValue2.ActiveText;

			switch (cmbOperator1.ActiveText) {

				case "Beginnt mit":
					this.CurrentPlugin.FilterOperator1 = "StartsWith";
					break;

				default:
					this.CurrentPlugin.FilterOperator1 = cmbOperator1.ActiveText;
					break;
			}

			switch (cmbOperator2.ActiveText) {

				case "Beginnt mit":
					this.CurrentPlugin.FilterOperator2 = "StartsWith";
					break;

				default:
					this.CurrentPlugin.FilterOperator2 = cmbOperator2.ActiveText;
					break;
			}

			if (this.CurrentPlugin is ReaderPluginBase) {
				((ReaderPluginBase)this.CurrentPlugin).TemporaryTableName = txtTmpTableName.Text;
			}
		}
	}
}

