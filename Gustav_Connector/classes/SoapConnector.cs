using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using Gustav_Connector.SoapService;
using System.Net;

namespace Gustav_Connector
{
	public class SoapConnector 
	{

		private Gustav_SOAP_v1 _Webservice = null;

		public SoapConnector()
		{
			this._Webservice = new Gustav_SOAP_v1 ();
			this._Webservice.Timeout = 30 * 60 * 1000; // 30 Minuten

			// Set Url from settings
			var url = GlobalSettings.ServerAddress;
			if(!url.EndsWith("/")) url += "/";
			url += "soap/v1/";
			this._Webservice.Url = url;

			// Ignore certicate errors
			ServicePointManager.CertificatePolicy = new CertificatePolicy();
		}

		public XmlNode GetOrders() {

			// XML erstellen
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;
			settings.Indent = true;
			
			StringBuilder xml = new StringBuilder();
			XmlWriter writer = XmlWriter.Create(xml, settings);
			this.WriteXml(writer);
			writer.Close();
						
            // Request webserver
            string respXml;
            try 
            {
                respXml = this._Webservice.getOrders(xml.ToString());
            }
            catch (Exception ex) 
            {
                throw new Exception("Fehler beim Laden der Aufträge über die Gustav-API: " + ex.Message);
            }
			
            // Parse response
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(respXml);

			var resultNode = xmlDoc.SelectSingleNode("response/status/successful");
			if(resultNode == null) {
				throw new Exception("Ungültige Antwort beim Laden der Aufträge über die Gustav-API");
			}

			if(!Boolean.Parse(resultNode.InnerText)) {
				throw new Exception("Fehler beim Laden der Aufträge über die Gustav-API: " + xmlDoc.SelectSingleNode("response/status/errorMsg").InnerText);
			}

			return xmlDoc.SelectSingleNode("response/orders");
		}

		public void AddManufacturers(DataTable manufacturers)
		{
			if (manufacturers.Rows.Count == 0)
				return;

			// XML erstellen
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;
			settings.Indent = true;

			StringBuilder xml = new StringBuilder();
			XmlWriter writer = XmlWriter.Create(xml, settings);

			writer.WriteStartElement("request");

			// customer
			this._WriteAccountNode(writer);

			// manufacturers
			foreach(DataRow mfRow in manufacturers.Rows)
			{
				writer.WriteStartElement ("manufacturer");

				writer.WriteElementString ("key", mfRow ["key"].ToString ());
				writer.WriteElementString ("name", mfRow ["name"].ToString ());

				if (manufacturers.Columns.Contains ("logoKey"))
					writer.WriteElementString ("logoKey", mfRow ["logoKey"].ToString ());

				writer.WriteEndElement ();
			}

			writer.WriteEndElement();
			writer.Close();

			var startTime = DateTime.Now;
			_WriteLogFile ("add_manufacturers", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addManufacturers(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Laden der Hersteller über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_manufacturers", startTime, "response", respXml);

			// Parse response
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(respXml);

			var resultNode = xmlDoc.SelectSingleNode("response/status/successful");
			if(resultNode == null) {
				throw new Exception("Ungültige Antwort beim Speichern der Hersteller über die Gustav-API");
			}

			if(!Boolean.Parse(resultNode.InnerText)) {
				throw new Exception("Fehler beim Speichern der Hersteller über die Gustav-API: " + xmlDoc.SelectSingleNode("response/status/errorMsg").InnerText);
			}
		}

		public void AddCustomergroups(DataTable customergroups)
		{
			if (customergroups.Rows.Count == 0)
				return;

			// XML erstellen
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;
			settings.Indent = true;

			StringBuilder xml = new StringBuilder();
			XmlWriter writer = XmlWriter.Create(xml, settings);

			writer.WriteStartElement("request");

			// customer
			this._WriteAccountNode(writer);

			// erp customer group
			foreach(DataRow custgrpRow in customergroups.Rows)
			{
				writer.WriteStartElement ("customergroup");

				writer.WriteElementString ("key", custgrpRow ["key"].ToString ());
				writer.WriteElementString ("name", custgrpRow ["name"].ToString ());
				writer.WriteEndElement ();
			}

			writer.WriteEndElement();
			writer.Close();

			var startTime = DateTime.Now;
			_WriteLogFile ("customergroups", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addCustomergroups(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Laden der Kundengruppen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("customergroups", startTime, "response", respXml);

			// Parse response
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(respXml);

			var resultNode = xmlDoc.SelectSingleNode("response/status/successful");
			if(resultNode == null) {
				throw new Exception("Ungültige Antwort beim Speichern der Kundengruppen über die Gustav-API");
			}

			if(!Boolean.Parse(resultNode.InnerText)) {
				throw new Exception("Fehler beim Speichern der Kundengruppen über die Gustav-API: " + xmlDoc.SelectSingleNode("response/status/errorMsg").InnerText);
			}
		}

		public void AddContacts(DataTable contacts)
		{
			if (contacts.Rows.Count == 0)
				return;

			// XML erstellen
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;
			settings.Indent = true;

			StringBuilder xml = new StringBuilder();
			XmlWriter writer = XmlWriter.Create(xml, settings);

			writer.WriteStartElement("request");

			// customer
			this._WriteAccountNode(writer);

			// erp contact
			foreach(DataRow contactRow in contacts.Rows)
			{
				writer.WriteStartElement ("erpContact");

				foreach (DataColumn col in contacts.Columns) {
					writer.WriteElementString (col.ColumnName, contactRow [col].ToString ());
				}

				writer.WriteEndElement ();
			}

			writer.WriteEndElement();
			writer.Close();

			var startTime = DateTime.Now;
			_WriteLogFile ("add_contacts", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addErpContacts(xml.ToString()); 
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Ansprechpartner über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_contacts", startTime, "response", respXml);

			// Parse response
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(respXml);

			var resultNode = xmlDoc.SelectSingleNode("response/status/successful");
			if(resultNode == null) {
				throw new Exception("Ungültige Antwort beim Hinzufügen der Ansprechpartner über die Gustav-API");
			}

			if(!Boolean.Parse(resultNode.InnerText)) {
				throw new Exception("Fehler beim Hinzufügen der Ansprechpartner über die Gustav-API: " + xmlDoc.SelectSingleNode("response/status/errorMsg").InnerText);
			}
		}

		public void AddProductgroups(DataTable productgroups)
		{
			if (productgroups.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (productgroups, "productGroup");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_productgroups", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Produktgruppen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_productgroups", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Produktgruppen über die Gustav-API: " + errorMsg);
			}
		}

		public void UpdateOrders(DataTable orderStatus)
		{
			if (orderStatus.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (orderStatus, "orders");

			var startTime = DateTime.Now;
			_WriteLogFile ("update_orders", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.updateOrders(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Aktualisieren der Aufträge über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("update_orders", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Aktualisieren der Aufträge über die Gustav-API: " + errorMsg);
			}
		}

		public void AddPricelists(DataTable pricelists)
		{
			if (pricelists.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (pricelists, "pricelist");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_pricelists", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Preislisten über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_pricelists", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Preislisten über die Gustav-API: " + errorMsg);
			}
		}

		public void AddProducts(DataTable products)
		{
			if (products.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (products, "product");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_products", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Produkte über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_products", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Produkte über die Gustav-API: " + errorMsg);
			}
		}


		public void AddTrackingCodes(DataTable trackingCodes)
		{
			if (trackingCodes.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (trackingCodes, "trackingcodes");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_trackingcodes", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addTrackingCodes(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Trackingcodes über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_trackingcodes", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Trackingcodes über die Gustav-API: " + errorMsg);
			}
		}

		public void AddProductgroupsRelations(DataTable prodgrprels)
		{
			if (prodgrprels.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (prodgrprels, "prodgrprel");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_productgrouprelations", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addProductgroupRelations(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Produktgruppenzuordnungen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_productgrouprelations", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Produktgruppenzuordnungen über die Gustav-API: " + errorMsg);
			}
		}

		public void AddPrices(DataTable prices)
		{
			if (prices.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (prices, "price");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_prices", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addPrices(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Preise über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_prices", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Preise über die Gustav-API: " + errorMsg);
			}
		}

		public void AddCustomers(DataTable customers)
		{
			if (customers.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (customers, "erpCustomer");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_customers", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addErpCustomers(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Kunden über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_customers", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Kunden über die Gustav-API: " + errorMsg);
			}
		}

		public void AddImages(DataTable images)
		{
			if (images.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (images, "image");


			var startTime = DateTime.Now;
			_WriteLogFile ("add_images", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addImages(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Bilder über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_images", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Bilder über die Gustav-API: " + errorMsg);
			}
		}

		public void AddProductimages(DataTable images)
		{
			if (images.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (images, "productimage");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_productimages", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addProductimages(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Produktbilder-Zuordnungen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_productimages", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Produktbilder-Zuordnungen über die Gustav-API: " + errorMsg);
			}
		}

		public void AddDiscountRows(DataTable discountRows)
		{
			if (discountRows.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (discountRows, "discountRow");

			var startTime = DateTime.Now;
			_WriteLogFile ("add_discountrows", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.addDiscountRows(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Hinzufügen der Rabatt-Zuordnungen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("add_discountrows", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Hinzufügen der Rabatt-Zuordnungen über die Gustav-API: " + errorMsg);
			}
		}

		public void UpdateStockQtys(DataTable stockQtys)
		{
			if (stockQtys.Rows.Count == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (stockQtys, "stockQty");

			var startTime = DateTime.Now;
			_WriteLogFile ("update_stockqty", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.updateStockQtys(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Aktualisieren der Lagerstände über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("update_stockqty", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Aktualisieren der Lagerstände über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteManufacturers(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "manufacturer");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_manufacturers", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Hersteller über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_manufacturers", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Hersteller über die Gustav-API: " + errorMsg);
			}
		}

		public void DeletePricelists(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "pricelist");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_pricelists", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.deleteProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Preislisten über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_pricelists", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Preislisten über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteProducts(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "product");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_products", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.deleteProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Produkte über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_products", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Produkte über die Gustav-API: " + errorMsg);
			}
		}

		public void DeletePrices(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "price");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_prices", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Preise über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_prices", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Preise über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteProdgrprels(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "prodgrprel");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_prodgrprels", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Prod.grp.-Zuordnungen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_prodgrprels", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Prod.grp.-Zuordnungen über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteProductgroups(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "productGroup");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_productgroups", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.deleteProducts(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Produktgruppen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_productgroups", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Produktgruppen über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteErpContacts(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "erpContact");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_erpcontacts", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der ERP-Ansprechpartner über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_erpcontacts", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der ERP-Ansprechpartner über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteErpCustomers(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "erpCustomer");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_erpcustomers", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der ERP-Kunden über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_erpcustomers", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der ERP-Kunden über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteErpCustomerGroups(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "erpCustomergroup");


			var startTime = DateTime.Now;
			_WriteLogFile ("delete_erpcustomergroups", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der ERP-Kundengruppe über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_erpcustomergroups", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der ERP-Kundengruppe über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteImages(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "image");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_images", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Bilder über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_images", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Bilder über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteProductimages(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "productimage");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_productimages", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Produktbilder-Zuordnungen über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_productimages", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Produktbilder-Zuordnungen über die Gustav-API: " + errorMsg);
			}
		}

		public void DeleteDiscountRows(Dictionary<string,string>[] keys)
		{
			if (keys.Length == 0)
				return;

			// XML erstellen
			var xml = this._GetRequestXml (keys, "discountRow");

			var startTime = DateTime.Now;
			_WriteLogFile ("delete_discountrows", startTime, "request", xml.ToString ());

			// Request webserver
			string respXml;
			try 
			{
				respXml = this._Webservice.delete(xml.ToString());
			}
			catch (Exception ex) 
			{
				throw new Exception("Fehler beim Löschen der Rabatte über die Gustav-API: " + ex.Message);
			}

			_WriteLogFile ("delete_discountrows", startTime, "response", respXml);

			// Check response
			string errorMsg;
			var successful = this._CheckResponseXml (respXml, out errorMsg);

			if (!successful) {
				throw new Exception ("Fehler beim Löschen der Rabatte über die Gustav-API: " + errorMsg);
			}
		}

		public void SendErrorMail(string message) {

			try
			{
				this._Webservice.sendErrorMail (
					"Fehler im Gustav_Connector",
					message,
					GlobalSettings.ApiKey
				);
			} catch (Exception ex) {
				Logger.Current.HandleException (ex, !Context.Current.AutoMode, false);
				Console.WriteLine ("Fehler beim Senden der Fehlermail!");
			}
		}

		public string[] GetKeys(string entity)
		{
			return this._Webservice.getKeys (entity, GlobalSettings.ApiKey, GetSecurityToken());
		}

		private bool _CheckResponseXml(string xml, out string errorMsg)
		{
			// Parse response
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);

			var resultNode = xmlDoc.SelectSingleNode("response/status/successful");
			if(resultNode == null) {
				errorMsg = "Ungültige Antwort";
				return false;
			}

			if(!Boolean.Parse(resultNode.InnerText)) {
				errorMsg = xmlDoc.SelectSingleNode ("response/status/errorMsg").InnerText;
				return false;
			}

			errorMsg = null;
			return true;
		}

		private string _GetRequestXml(Dictionary<string,string>[] keys, string tagName)
		{
			var table = new DataTable ();
			foreach (string keyName in keys[0].Keys) {
				table.Columns.Add (keyName);
			}

			foreach (Dictionary<string,string> keysItem in keys) 
			{
				var row = table.NewRow ();

				foreach (string keyName in keysItem.Keys) {
					row [keyName] = keysItem[keyName];
				}

				table.Rows.Add (row);
			}

			return this._GetRequestXml (table, tagName);
		}

		private string _GetRequestXml(DataTable table, string tagName) 
		{
			// XML erstellen
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;
			settings.Indent = true;

			StringBuilder xml = new StringBuilder();
			XmlWriter writer = XmlWriter.Create(xml, settings);

			writer.WriteStartElement("request");
			writer.WriteElementString ("source", "gustav_connector");

			// Number format
			var nfi = System.Globalization.CultureInfo.GetCultureInfo ("en-US").NumberFormat;

			// customer
			this._WriteAccountNode(writer);

			// write items
			foreach(DataRow tableRow in table.Rows)
			{
				writer.WriteStartElement (tagName);

				foreach (DataColumn col in table.Columns) {
					try {
						_WriteCellValue(writer, tableRow, col, nfi);
					}
					catch (Exception ex) {
						throw new Exception (
							String.Format ("Fehler {0} in Zeile {1} / Spalte {2}", ex.Message, table.Rows.IndexOf(tableRow) + 1, col.ColumnName),
							ex
						);
					}
				}

				writer.WriteEndElement ();
			}

			writer.WriteEndElement();
			writer.Close();

			return xml.ToString ();
		}

		protected void _WriteCellValue(XmlWriter writer, DataRow tableRow, DataColumn col, System.Globalization.NumberFormatInfo nfi)
		{
			if(tableRow [col] == DBNull.Value) {
				writer.WriteElementString (col.ColumnName, null);
			} else if (col.DataType == typeof(DateTime)) {
				writer.WriteElementString (col.ColumnName, ((DateTime)tableRow [col]).ToString ("yyyy-MM-dd HH:mm:ss"));
			} else if (col.DataType == typeof(Double)) {
				writer.WriteElementString (col.ColumnName, ((Double)tableRow [col]).ToString(nfi));
			} else if (col.DataType == typeof(byte[])) {
				writer.WriteElementString (col.ColumnName, Convert.ToBase64String ((byte[])tableRow [col]));
			} else {
				writer.WriteElementString (col.ColumnName, tableRow [col].ToString ());
			}
		}

		private void _WriteAccountNode(XmlWriter writer)
		{
			writer.WriteStartElement("customer");
			
			writer.WriteElementString("soapKey", GlobalSettings.ApiKey);
			writer.WriteElementString("securityToken", this.GetSecurityToken());
			
			writer.WriteEndElement();
		}

		private string GetSecurityToken()
		{
			return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(
				GlobalSettings.ApiKey + GlobalSettings.ApiSecureToken +
				DateTime.Today.ToString("yyyy-MM-dd")
				, "md5");
		}

		private void WriteXml(XmlWriter writer) 
		{
			writer.WriteStartElement("request");

			// customer
			this._WriteAccountNode(writer);

			// last order number
			writer.WriteElementString("lastNumber", GlobalSettings.LastOrderApproveNumber);

			writer.WriteEndElement();
		}

		/// <summary>
		/// Writes the log file for a xml request
		/// </summary>
		/// <param name="name">Function name</param>
		/// <param name="startTime">Start time</param>
		/// <param name="status">Content type (request/response)</param>
		/// <param name="xml">XML content</param>
		private void _WriteLogFile(string name, DateTime startTime, string type, string xml)
		{
			if (!Context.Current.AutoMode)
				return;

			string dir = Common.GetSubdirectory("logfiles");
			string filename = String.Format ("{0}_{1}_{2}.xml", name, startTime.ToString ("yyyy-MM-dd_HH-mm-ss"), type);
			string fullFilename = Path.Combine (dir, filename);
			File.WriteAllText (fullFilename, xml);
		}
	}
}

