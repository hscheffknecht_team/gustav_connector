﻿using System;
using System.Text;

namespace Gustav_Connector
{
	/// <summary>
	/// Contains results of comparing data with local and remote (Gustav) database
	/// </summary>
	public class DataCheckResult
	{
		private string[] _LocalOnly;
		private string[] _RemoteOnly;

		/// <summary>
		/// Returns entity keys which are missing on Gustav server
		/// </summary>
		public string[] LocalOnly {
			get { return _LocalOnly; }
		}

		/// <summary>
		/// Returns entity keys which are not available locally
		/// </summary>
		public string[] RemoteOnly {
			get { return _RemoteOnly; }
		}

		public DataCheckResult (string[] local, string[] remote)
		{
			_LocalOnly = local;
			_RemoteOnly = remote;
		}

		public int getNumberErrors()
		{
			return _LocalOnly.Length + _RemoteOnly.Length;
		}

		public override string ToString ()
		{
			if (getNumberErrors () == 0)
				return "Der Datenstand ist synchron";

			// Bild error string
			var sb = new StringBuilder();

			foreach (string key in _LocalOnly)
				sb.AppendFormat ("{0} ist nur lokal vorhanden\r\n", key);
			
			foreach (string key in _RemoteOnly)
				sb.AppendFormat ("{0} ist nur remote vorhanden\r\n", key);

			return sb.ToString ();
		}
	}
}
