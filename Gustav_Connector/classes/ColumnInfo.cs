using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class ColumnInfo : IXmlSerializable
	{
		#region XmlSerializeable

		public XmlSchema GetSchema ()
		{
			return null;
		}
		
		public void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element)
				{
					switch(reader.Name)
					{
					case "Name":
						reader.Read();
						this.Name = reader.Value;
						break;
						
					case "Type":
						reader.Read();
						this.Type = (ColumnType)Enum.Parse(typeof(ColumnType), reader.Value);
						break;
						
					case "IsPrimaryKey":
						reader.Read();
						this.IsPrimaryKey = Boolean.Parse(reader.Value);
						break;
						
					case "IsInternal":
						reader.Read();
						this.IsInternal = Boolean.Parse(reader.Value);
						break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}

			reader.ReadEndElement();
		}
		
		public void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement("ColumnInfo");
			writer.WriteElementString("Name", this.Name);
			writer.WriteElementString("Type", this.Type.ToString());
			writer.WriteElementString("IsPrimaryKey", this.IsPrimaryKey.ToString());
			writer.WriteElementString("IsInternal", this.IsInternal.ToString());
			writer.WriteEndElement();
		}

		#endregion

		public ColumnInfo ()
		{
		}

		public override bool Equals (object obj)
		{
			return this.Name.Equals(((ColumnInfo)obj).Name);
		}

		public override int GetHashCode ()
		{
			return this.Name.GetHashCode();
		}

		public string Name { get; set; }
		public ColumnType Type { get; set; }
		public bool IsPrimaryKey { get; set; }
		public bool IsInternal { get; set; }
	}
}

