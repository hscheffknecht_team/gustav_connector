using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class RelationsInfo : IXmlSerializable
	{
		public string SourcePrefix { get; set; }
		public string SourceColumn { get; set; }
		public string SourceValue { get; set; }
		public string DestinationColumn { get; set; }

		public RelationsInfo ()
		{
			SourcePrefix = "";
		}

		public XmlSchema GetSchema ()
		{
			return null;
		}
		
		public void ReadXml (XmlReader reader)
		{
			while(true)
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch(reader.Name)
					{
						case "SourcePrefix":
							reader.Read();
							this.SourcePrefix = reader.Value;
							break;

						case "SourceColumn":
							reader.Read();
							this.SourceColumn = reader.Value;
							break;

						case "SourceValue":
							reader.Read();
							this.SourceValue = reader.Value;
							break;
							
						case "DestinationColumn":
							reader.Read();
							this.DestinationColumn = reader.Value;
							break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}

				reader.Read();
			}
		}
		
		public void WriteXml (XmlWriter writer) 
		{
			writer.WriteStartElement("RelationsInfo");
			writer.WriteElementString("SourcePrefix", this.SourcePrefix);
			writer.WriteElementString("SourceColumn", this.SourceColumn);
			writer.WriteElementString("SourceValue", this.SourceValue);
			writer.WriteElementString("DestinationColumn", this.DestinationColumn);
			writer.WriteEndElement();
		}
	}
}

