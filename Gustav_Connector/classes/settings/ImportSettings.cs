using System;

namespace Gustav_Connector
{
	public class ImportSettings : SettingsBase
	{
		private string _Key;

		private SettingsHandler _Handler = null;

		public SettingsCollection Readers 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Readers"); }
			set { this._Handler.SetValue("Readers", value); }
		}

		public SettingsCollection Plugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Plugins"); }
			set { this._Handler.SetValue("Plugins", value); }
		}

		public ImportSettings (string key)
		{
			this._Key = key;

			this._Handler = new SettingsHandler(key);

			if (this.Readers == null)
				this.Readers = new SettingsCollection ();

			if(this.Plugins == null)
				this.Plugins = new SettingsCollection();

			this.Readers.Container = this.Plugins.Container = this;
		}

		public override void Save() {

			_Handler.Save();
		}
	}
}

