using System;

namespace Gustav_Connector
{
	public class MfSettings : SettingsBase
	{
		private static MfSettings _Current = new MfSettings();
		public static MfSettings Current
		{
			get { return _Current; }
		}

		private SettingsHandler _Handler = new SettingsHandler("manufacturers");

		public SettingsCollection ReaderPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Readers"); }
			set { this._Handler.SetValue("Readers", value); }
		}

		public SettingsCollection MfPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Plugins"); }
			set { this._Handler.SetValue("Plugins", value); }
		}

		public MfSettings ()
		{
			if (this.ReaderPlugins == null)
				this.ReaderPlugins = new SettingsCollection ();

			if(this.MfPlugins == null)
				this.MfPlugins = new SettingsCollection();

			this.ReaderPlugins.Container = this.MfPlugins.Container = this;
		}

		public override void Save() {

			_Handler.Save();
		}
	}
}

