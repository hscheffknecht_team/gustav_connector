using System;

namespace Gustav_Connector
{
	public class ContactsSettings : SettingsBase
	{
		private static ContactsSettings _Current = new ContactsSettings();
		public static ContactsSettings Current
		{
			get { return _Current; }
		}

		private SettingsHandler _Handler = new SettingsHandler("contacts");

		public SettingsCollection ReaderPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Readers"); }
			set { this._Handler.SetValue("Readers", value); }
		}

		public SettingsCollection ContactsPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Plugins"); }
			set { this._Handler.SetValue("Plugins", value); }
		}

		public ContactsSettings ()
		{
			if (this.ReaderPlugins == null)
				this.ReaderPlugins = new SettingsCollection ();

			if(this.ContactsPlugins == null)
				this.ContactsPlugins = new SettingsCollection();

			this.ReaderPlugins.Container = this.ContactsPlugins.Container = this;
		}

		public override void Save() {

			_Handler.Save();
		}
	}
}

