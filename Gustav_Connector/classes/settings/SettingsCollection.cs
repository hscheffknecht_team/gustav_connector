using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class SettingsCollection : CollectionBase, IXmlSerializable
	{

		#region IXmlSerializable

		public XmlSchema GetSchema ()
		{
			return null;
		}
		
		public void ReadXml (XmlReader reader) {

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && reader.Name == "Item")
				{
					reader.ReadAttributeValue();
					string className = reader.GetAttribute("class");
					
					var classType = Type.GetType(className);
					var instance = (IXmlSerializable)Activator.CreateInstance(classType);
					if(!reader.IsEmptyElement)
					{
						reader.ReadStartElement();
						instance.ReadXml(reader);
						reader.ReadEndElement();
					}
					this.Add(instance);

				} else if(reader.NodeType == XmlNodeType.EndElement || reader.IsEmptyElement)
				{
					break;
				}
			}
		}
		
		public void WriteXml (XmlWriter writer) 
		{
			writer.WriteStartElement("SettingsCollection");

			foreach(IXmlSerializable item in this.List)
			{
				writer.WriteStartElement("Item");
				writer.WriteAttributeString("class", item.GetType().ToString());
				item.WriteXml(writer);
				writer.WriteEndElement();
			}

			writer.WriteEndElement();
		}

		#endregion

		public SettingsBase Container { get; set; }

		public SettingsCollection ()
		{
		}

		public int IndexOf(IXmlSerializable item)
		{
			return List.IndexOf (item);
		}

        public void MoveUp(int index)
        {
            if(index == 0) return;

            var item = this.List[index];
            this.List.RemoveAt(index);
            this.List.Insert(index-1, item);
        }

        public void MoveDown(int index)
        {
            if(index == this.List.Count-1) return;

            var item = this.List[index];
            this.List.RemoveAt(index);
            this.List.Insert(index+1, item);
        }

		#region Collection

		public IXmlSerializable this[int index]
		{
			get { return (IXmlSerializable)this.List[index]; }
		}

		public void Add(IXmlSerializable item)
		{
			this.List.Add(item);

			if (item is PluginBase) {
				((PluginBase)item).Container = this;
			}
		}

		public void Remove(IXmlSerializable item)
		{
			this.List.Remove(item);
		}

		#endregion
	}
}

