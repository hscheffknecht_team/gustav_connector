using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class OrderSettings : SettingsBase
	{
		private static OrderSettings _Current = new OrderSettings();
		public static OrderSettings Current
		{
			get { return _Current; }
		}

		public static bool HasColumn(string columnName, List<ColumnInfo> list) {
			foreach (ColumnInfo col in list) {
				if (String.Compare (columnName, col.Name, true) == 0) {
					return true;
				}
			}

			return false;
		}

		private SettingsHandler _Handler = new SettingsHandler("order");
		private List<ColumnInfo> _OrdersColumns;
		private List<ColumnInfo> _OrderItemsColumns;
		private List<ColumnInfo> _CustomersColumns;

		public SettingsHandler Handler {
			get { return _Handler; }
		}

		public List<ColumnInfo> OrdersColumns { 
			get {
				if(_OrdersColumns == null)
				{
					_OrdersColumns = LoadColumns("OrdersColumns");
				}
				return _OrdersColumns;
			}
			set {
				_OrdersColumns = value;
			}
		}
		
		public List<ColumnInfo> OrderItemsColumns { 
			get {
				if(_OrderItemsColumns == null)
				{
					_OrderItemsColumns = LoadColumns("OrderItemsColumns");
				}
				return _OrderItemsColumns;
			}
			set {
				_OrderItemsColumns = value;
			}
		}
		
		public List<ColumnInfo> CustomersColumns { 
			get {
				if(_CustomersColumns == null)
				{
					_CustomersColumns = LoadColumns("CustomersColumns");
				}
				return _CustomersColumns;
			}
			set {
				_CustomersColumns = value;
			}
		}
		
		public override void Save() {
			
			var ordersColumns = new SettingsCollection();
			foreach(IXmlSerializable item in OrdersColumns)
			{
				ordersColumns.Add(item);
			}
			
			var orderItemsColumns = new SettingsCollection();
			foreach(IXmlSerializable item in OrderItemsColumns)
			{
				orderItemsColumns.Add(item);
			}
			
			var customersColumns = new SettingsCollection();
			foreach(IXmlSerializable item in CustomersColumns)
			{
				customersColumns.Add(item);
			}

			_Handler.SetValue("OrdersColumns", ordersColumns);
			_Handler.SetValue("OrderItemsColumns", orderItemsColumns);
			_Handler.SetValue("CustomersColumns", customersColumns);

			_Handler.Save();
		}

		private List<ColumnInfo> LoadColumns(string name)
		{
			var columns = new List<ColumnInfo>();
			var collection = (SettingsCollection)_Handler.GetObject(name);
			if(collection != null)
			{
				foreach(ColumnInfo col in collection)
				{
					columns.Add(col);
				}
			}

			return columns;
		}
		
		public SettingsCollection OrdersPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Plugins"); }
			set { this._Handler.SetValue("Plugins", value); }
		}
		
		public SettingsCollection WriterPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Writers"); }
			set { this._Handler.SetValue("Writers", value); }
		}

		public OrderSettings()
		{
			if(this.OrdersPlugins == null)
				this.OrdersPlugins = new SettingsCollection();

			if(this.WriterPlugins == null) 
				this.WriterPlugins = new SettingsCollection();

			this.OrdersPlugins.Container = this.WriterPlugins.Container = this;
		}

	}
}

