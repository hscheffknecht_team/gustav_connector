using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class SettingsHandler
	{
		private string _GroupName;
		private Dictionary<string, object> _List;

		public SettingsHandler (string settingsgroup)
		{
			this._GroupName = settingsgroup;
			this._Load();
		}

		public object GetObject(string name) {

			if(this._List.ContainsKey(name)) {
				return this._List[name];
			} else {
				return null;
			}
		}

		public string GetString(string name) {

			var o = this.GetObject(name);
			if(o != null)
				return o.ToString();
			else
				return null;
		}

		public bool GetBool(string name) {

			var strValue = this.GetString (name);
			bool boolValue = false;
			if (strValue != null) {
				Boolean.TryParse (strValue, out boolValue);
			}

			return boolValue;
		}

		public void SetValue(string name, IXmlSerializable value)
		{
			SetValue(name, (object)value);
		}

		public void SetValue(string name, string value)
		{
			SetValue(name, (object)value);
		}

		public void SetValue(string name, bool value) 
		{
			SetValue (name, value.ToString ());
		}

		private void SetValue(string name, object value) {
			if(this._List.ContainsKey(name)) {
				this._List[name] = value;
			} else {
				this._List.Add(name, value);
			}
		}

		public void Save() {

			var filename = this._GetFilename();

			var sett = new XmlWriterSettings();
			sett.Indent = true;

			var writer = XmlWriter.Create(filename, sett);
			writer.WriteStartElement(this._GroupName);

			foreach(string key in this._List.Keys)
			{
				object item = this._List[key];

				if(item is IXmlSerializable)
				{
					writer.WriteStartElement(key);
					writer.WriteAttributeString("class", item.GetType().ToString());
					((IXmlSerializable)item).WriteXml(writer);
					writer.WriteEndElement();
				}
				else if(item is String)
				{
					writer.WriteStartElement(key);
					writer.WriteAttributeString("class", "String");
					writer.WriteValue((string)item);
					writer.WriteEndElement();
				}
			}

			writer.WriteEndElement();
			writer.Close();
		}



		private void _Load() {

			this._List = new Dictionary<string, object>();
			var filename = this._GetFilename();

			try
			{
				if(File.Exists(filename))
				{
					var reader = XmlReader.Create(filename);

					reader.ReadStartElement();

					while(reader.Read())
					{
						if(reader.NodeType == XmlNodeType.Element)
						{
							string name = reader.Name;
							reader.ReadAttributeValue();
							string className = reader.GetAttribute("class");

							if(className == "String")
							{
								reader.Read();
								if(reader.NodeType == XmlNodeType.Text) {
									this._List.Add(name, reader.Value);
									reader.Read();
								}
							}
							else
							{
								var classType = Type.GetType(className);
								var instance = (IXmlSerializable)Activator.CreateInstance(classType);
								reader.Read();
								instance.ReadXml(reader);
								if(reader.IsEmptyElement)
									reader.Read ();
								else
									reader.ReadEndElement();
								this._List.Add(name, instance);
							}
						}
					}

					reader.Close();
				}
			} catch(Exception ex) {
				throw new Exception (String.Format ("Fehler beim Laden der Konfigurationsdatei {0}: {1}", filename, ex.Message), ex);
			}
		}

		private string _GetFilename() {

			return String.Format("config/{0}.xml", this._GroupName);
		}
	}
}

