using System;

namespace Gustav_Connector
{
	public static class GlobalSettings
	{
		private static SettingsHandler _Handler = new SettingsHandler("global");

		public static string ServerAddress {
			get { return _Handler.GetString("ServerAddress"); }
			set { _Handler.SetValue("ServerAddress", value); }
		}

		public static string ApiKey {
			get { return _Handler.GetString("ApiKey"); }
			set { _Handler.SetValue("ApiKey", value); }
		}
		
		public static string ApiSecureToken {
			get { return _Handler.GetString("ApiSecureToken"); }
			set { _Handler.SetValue("ApiSecureToken", value); }
		}

		public static string LastOrderApproveNumber {
			get { return _Handler.GetString("LastOrderApproveNumber"); }
			set { _Handler.SetValue("LastOrderApproveNumber", value); }
		}

		public static string LastCheckDate {
			get { return _Handler.GetString("LastCheckDate"); }
			set { _Handler.SetValue("LastCheckDate", value); }
		}

		public static bool SendErrorMails {
			get { return _Handler.GetBool ("SendErrorMails"); }
			set { _Handler.SetValue ("SendErrorMails", value); }
		}

		public static bool SkipUpload {
			get { return _Handler.GetBool ("SkipUpload"); }
			set { _Handler.SetValue ("SkipUpload", value); }
		}

		public static bool DoChecks {
			get { return _Handler.GetBool ("DoChecks"); }
			set { _Handler.SetValue ("DoChecks", value); }
		}

		public static void Save() {
			_Handler.Save ();
		}
	}
}
