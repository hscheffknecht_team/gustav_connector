using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class CustgrpSettings : SettingsBase
	{
		private static CustgrpSettings _Current = new CustgrpSettings();
		public static CustgrpSettings Current
		{
			get { return _Current; }
		}

		private SettingsHandler _Handler = new SettingsHandler("custgrp");

		public SettingsCollection ReaderPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Readers"); }
			set { this._Handler.SetValue("Readers", value); }
		}

		public SettingsCollection CustgrpPlugins 
		{ 
			get { return (SettingsCollection)this._Handler.GetObject("Plugins"); }
			set { this._Handler.SetValue("Plugins", value); }
		}

		public CustgrpSettings ()
		{
			if (this.ReaderPlugins == null)
				this.ReaderPlugins = new SettingsCollection ();

			if(this.CustgrpPlugins == null)
				this.CustgrpPlugins = new SettingsCollection();

			this.ReaderPlugins.Container = this.CustgrpPlugins.Container = this;
		}

		public override void Save() {

			_Handler.Save();
		}
	}
}

