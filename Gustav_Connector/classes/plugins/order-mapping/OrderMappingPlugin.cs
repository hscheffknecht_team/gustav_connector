using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderMappingPlugin : OrderPluginBase
	{
		public List<RelationsInfo> Relations { get; set; }

		public OrderTable TargetTable { get; set; }

		public OrderMappingPlugin () : base ()
		{
			this.Name = "Mapping Plugin";
			this.Description = "Kopiert und konvertiert die Auftrags- und Kundendaten";
			this.SettingsDialogType = typeof(OrderMappingSettings);
			this.Relations = new List<RelationsInfo> ();
		}

		public override string GetStatusInfo ()
		{
			return String.Format ("Zieltabelle: {0}, {1} Zuordnungen", this.TargetTable, this.Relations.Count);
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString ("TargetTable", this.TargetTable.ToString ());
			foreach (var relation in this.Relations) {
				relation.WriteXml (writer);
			}
		}

		public override void ReadXml (XmlReader reader)
		{
			this.Relations = new List<RelationsInfo> ();
			base.ReadXml (reader);
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch (nodeName) {
			case "TargetTable":
				this.TargetTable = (OrderTable)Enum.Parse (typeof(OrderTable), reader.Value);
				break;
				
			case "RelationsInfo":
				var relation = new RelationsInfo ();
				relation.ReadXml (reader);
				this.Relations.Add (relation);
				break;
			}
		}

		#endregion

		public override void ProcessOrderNode (XmlNode input, OrderProcessor processor)
		{
			// Find table
			DataTable table = null;
			switch (this.TargetTable) {
			case OrderTable.Orders:
				table = processor.AllOrders;
				break;
			case OrderTable.OrderItems:
				table = processor.AllOrderItems;
				break;
			case OrderTable.Customers:
				table = processor.AllCustomers;
				break;
			}

			if (this.TargetTable == OrderTable.OrderItems) 
			{
				foreach (XmlNode itemNode in input.SelectNodes("items/item")) {
					DataRow row = table.NewRow ();

					foreach (var relation in this.Relations) {
						if (row.Table.Columns.Contains (relation.DestinationColumn)) {

							if (!String.IsNullOrEmpty (relation.SourceColumn)) {
								if (relation.SourceColumn.Contains ("items")) {
									_SetNewValue (relation, row, input, itemNode.SelectSingleNode (relation.SourceColumn.Replace ("items/item/", "")).InnerText);
								} else {
									_SetNewValue (relation, row, input, input.SelectSingleNode (relation.SourceColumn).InnerText);
								}
							} else {
								_SetNewValue (relation, row, input, "");
							}
						}
					}

					// Add Gustav ID
					row ["gustav_id"] = Int32.Parse (input.SelectSingleNode ("id").InnerText);
					
					_AddRowToTable (table, row);
				}
			} else {
				DataRow row = table.NewRow ();

				// Iterate relations
				foreach (var relation in this.Relations) {
					var sourceColumnValue = "";
					if (!String.IsNullOrEmpty (relation.SourceColumn)) {
						var sourceNode = input.SelectSingleNode (relation.SourceColumn);
						if (sourceNode != null)
							sourceColumnValue = sourceNode.InnerText;
					}
					_SetNewValue (relation, row, input, sourceColumnValue);
				}

				// Add Gustav ID
				row ["gustav_id"] = Int32.Parse (input.SelectSingleNode ("id").InnerText);

				_AddRowToTable (table, row);
			}
		}

		private void _SetNewValue (RelationsInfo relation, DataRow row, XmlNode input, string sourceColumnValue)
		{
			if (String.IsNullOrEmpty (relation.DestinationColumn)) {
				throw new Exception ("Fehler in einem Mapping-Plugin für Tabelle " + this.TargetTable + ": Keine Zielspalte festgelegt!");
			}

			var newValue = "";
			if (!String.IsNullOrEmpty (row [relation.DestinationColumn].ToString ()))
				newValue = row [relation.DestinationColumn].ToString () + " ";

			if (row.Table.Columns.Contains (relation.DestinationColumn))
				newValue += sourceColumnValue;

			row [relation.DestinationColumn] = relation.SourcePrefix + newValue + relation.SourceValue;
		}

		private void _AddRowToTable (DataTable table, DataRow row)
		{
			var primaryKeys = table.PrimaryKey;
			var primaryCols = new Object[primaryKeys.Length];

			for (int i = 0; i < primaryKeys.Length; i++)
				primaryCols [i] = row [primaryKeys [i]];

			var existingRow = table.Rows.Find (primaryCols);
			if (existingRow != null) {
				// Merge
				foreach (DataColumn col in table.Columns) {
					if (row [col] != DBNull.Value)
						existingRow [col] = row [col];
				}

			} else {
				// Add
				try {
					table.Rows.Add (row);
				} catch (NoNullAllowedException ex) {
					throw new Exception ("Fehler beim Mapping Plugin in Tabelle " + this.TargetTable + ": Fehlender Primary Key");
				}
			}
		}
	}
}