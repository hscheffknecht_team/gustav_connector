using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class OrderMappingSettings : PluginSettingsDialog
	{
		private OrderMappingPlugin _CurrentPlugin;
		private string[] _SourceColumns;
		private string[] _DestColumns;
		private int _NextRowNr;

		public Dictionary<string,string> _Relations { get; set; }

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (OrderMappingPlugin)value;
				Load ();
			}
		}

		public OrderMappingSettings ()
		{
			this.TypeHint = Gdk.WindowTypeHint.Normal;
			this.Build();

			// Load source columns
			this._SourceColumns = SoapDataCache.GetOrderFields();

			// Load destination columns
			this.LoadDestinationColumns();
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			// Target table
			this.cmbTable.Active = (int)this._CurrentPlugin.TargetTable;
			this.LoadDestinationColumns();

			// Relations
			foreach(RelationsInfo relation in this._CurrentPlugin.Relations)
			{
				AddRelation(relation.SourcePrefix, relation.SourceColumn, relation.SourceValue, relation.DestinationColumn);
			}

			this._NextRowNr += (int)tblRelations.NRows;
		}

		private void Save()
		{
			// Target table
			this._CurrentPlugin.TargetTable = (OrderTable)this.cmbTable.Active;

			// Relations
			var dict_sourcePrefixes = new Dictionary<int, string>();
			var dict_sourceColumns = new Dictionary<int, string>();
			var dict_sourceValues = new Dictionary<int, string>();
			var dict_destColumns = new Dictionary<int, string>();

			foreach(Gtk.Widget w in tblRelations.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblRelations[w];
				var keys = child.Child.Name.Split('_');
				var key_name = keys[0];

				if(child.Child is Gtk.ComboBox || child.Child is Gtk.Entry)
				{
					var row = Int32.Parse(keys[1]);
					var value = "";
					if(child.Child is Gtk.ComboBox)
						value = ((Gtk.ComboBox)child.Child).ActiveText;
					else
						value = ((Gtk.Entry)child.Child).Text;

					switch(key_name)
					{
						case "sourcePrefix":
							dict_sourcePrefixes.Add(row, value);
							break;

						case "sourceColumn":							
							dict_sourceColumns.Add(row, value);							
							break;

						case "sourceValue":
							dict_sourceValues.Add(row, value);
							break;

						case "dest":
							dict_destColumns.Add(row, value);
							break;
					}
				}
			}

			var relations = new List<RelationsInfo>((int)tblRelations.NRows-1);

			for (int i = 0; i < this._NextRowNr; i++) {
				if (dict_sourceColumns.ContainsKey (i) && dict_sourceValues [i] != null) {
					relations.Add (new RelationsInfo () {
						SourcePrefix = dict_sourcePrefixes [i],
						SourceColumn = dict_sourceColumns [i],
						SourceValue = dict_sourceValues [i],
						DestinationColumn = dict_destColumns [i]
					});
				}
			}
			
			this._CurrentPlugin.Relations = relations;
		}

		private void LoadDestinationColumns()
		{
			var columns = new List<string>();
			List<ColumnInfo> settColumns = null;

			// Load correct columnslist
			switch(cmbTable.Active)
			{
			case (int)OrderTable.Orders:
				settColumns = OrderSettings.Current.OrdersColumns;
				break;
			case (int)OrderTable.OrderItems:
				settColumns = OrderSettings.Current.OrderItemsColumns;
				break;
			case (int)OrderTable.Customers:
				settColumns = OrderSettings.Current.CustomersColumns;
				break;
			}

			foreach(ColumnInfo column in settColumns)
			{
				columns.Add(column.Name);
			}

			this._DestColumns = columns.ToArray();
		}

		protected void OnBtnAddRelationClicked (object sender, EventArgs e)
		{
			AddRelation(null, null, null, null);
		}

		protected void AddRelation(string srcPrefix, string srcColumn, string srcValue, string dst)
		{
			// Source prefix textbox			
			var txtPrefix = new Gtk.Entry(srcPrefix);			
			txtPrefix.Name = "sourcePrefix_" + this._NextRowNr;

			// Source column combobox
			var cmbSource = new Gtk.ComboBox(_SourceColumns);
			cmbSource.Name = "sourceColumn_" + this._NextRowNr;

			// Select source item
			var columnFound = false;
			if(srcColumn != null)
			{
				for(int i=0; i<_SourceColumns.Length; i++) {
					if(_SourceColumns[i] == srcColumn) {
						cmbSource.Active = i;
						columnFound = true;
					}
				}
			}

			// Add missing item to combobox
			if (!columnFound) {
				cmbSource.AppendText (srcColumn);
				cmbSource.Active = _SourceColumns.Length;
			}

			// Source value textbox
			var txtSource = new Gtk.Entry(srcValue);
			txtSource.Name = "sourceValue_" + this._NextRowNr;

			// Destination combobox
			var cmbDest = new Gtk.ComboBox(_DestColumns);
			cmbDest.Name = "dest_" + this._NextRowNr;

			// Select destination item
			if(dst != null)
			{
				for(int i=0; i<_DestColumns.Length; i++) {
					if(_DestColumns[i] == dst) {
						cmbDest.Active = i;
					}
				}
			}

			// Delete button
			var deleteButton = new Gtk.Button();
			deleteButton.Label = "Löschen";
			deleteButton.Clicked += OnBtnDeleteRelationsClicked;
			deleteButton.Name = "btnDeleteColumn_" + this._NextRowNr;

			// Add to table
			tblRelations.NRows++;
			tblRelations.Attach (txtPrefix, 0, 1, tblRelations.NRows-1, tblRelations.NRows);
			tblRelations.Attach (cmbSource, 1, 2, tblRelations.NRows-1, tblRelations.NRows);
			tblRelations.Attach (txtSource, 2, 3, tblRelations.NRows-1, tblRelations.NRows);
			tblRelations.Attach (cmbDest, 3, 4, tblRelations.NRows-1, tblRelations.NRows);
			tblRelations.Attach (deleteButton, 4, 5, tblRelations.NRows-1, tblRelations.NRows);
			tblRelations.ShowAll();

			this._NextRowNr++;
		}

		protected void OnBtnDeleteRelationsClicked (object sender, EventArgs e)
		{
			// Get row number
			var buttonChild =  (Gtk.Table.TableChild)tblRelations[(Gtk.Widget)sender];
			int rowNr = (int)buttonChild.TopAttach;
			
			// Move or delete widgets
			foreach(Gtk.Widget w in tblRelations.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblRelations[w];
				
				if(child.TopAttach > rowNr)
				{
					child.TopAttach--;
					child.BottomAttach--;
				} 
				else if(child.TopAttach == rowNr)
				{
					tblRelations.Remove(child.Child);
				}
			}
			
			// Remove row
			tblRelations.NRows--;
		}

		protected void OnCmbTableChanged (object sender, EventArgs e)
		{
			LoadDestinationColumns();
		}
	}
}

