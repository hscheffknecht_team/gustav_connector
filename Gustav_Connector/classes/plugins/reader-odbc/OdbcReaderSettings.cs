using System;

namespace Gustav_Connector
{
	public partial class OdbcReaderSettings : PluginSettingsDialog
	{
		private OdbcReaderPlugin _CurrentPlugin;

		public OdbcReaderSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (OdbcReaderPlugin)value;
				Load ();
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			this.txtConnectionString.Buffer.Text = this._CurrentPlugin.ConnectionString;
			this.txtDbQuery.Buffer.Text = this._CurrentPlugin.Query;
			this.nrLastTimestamp.Value = this._CurrentPlugin.LastTimestamp;
		}

		private void Save()
		{
			this._CurrentPlugin.ConnectionString = this.txtConnectionString.Buffer.Text;
			this._CurrentPlugin.Query = this.txtDbQuery.Buffer.Text;
			this._CurrentPlugin.LastTimestamp = (int)this.nrLastTimestamp.Value;
		}
	}
}

