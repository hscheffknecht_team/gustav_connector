using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using System.Xml;

namespace Gustav_Connector
{
	public class OdbcReaderPlugin : ReaderPluginBase
	{
		public string ConnectionString { get; set; }
		public string Query { get; set; }

		public OdbcReaderPlugin ()
		{
			this.Name = "ODBC Reader";
			this.Description = "Liest Daten aus einer beliebigen ODBC Quelle";
			this.SettingsDialogType = typeof(OdbcReaderSettings);
		}

		#region XmlSerializeable
		
		public override void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch (reader.Name) {
						
					case "ConnectionString":
						reader.Read ();
						this.ConnectionString = reader.Value;
						break;

					case "Query":
						reader.Read ();
						this.Query = reader.Value;
						break;

					case "LastTimestamp":
						reader.Read ();
						this.LastTimestamp = Int32.Parse(reader.Value);
						break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
			
			reader.ReadEndElement();
		}
		
		public override void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement("ColumnInfo");
			writer.WriteElementString("ConnectionString", this.ConnectionString);
			writer.WriteElementString("Query", this.Query);
			writer.WriteElementString("LastTimestamp", this.LastTimestamp.ToString());
			writer.WriteEndElement();
		}
		
		#endregion

		public override DataTable GetData (ImportProcessorBase processor)
		{
			// Connect DB and send query
			var cmd = new OdbcDataAdapter (this.Query, this.ConnectionString);

			var nrParameters = this.Query.Split (new String[] {"@timestamp"}, StringSplitOptions.None).Length - 1;
			for (int i=1; i<= nrParameters; i++) {
				cmd.SelectCommand.Parameters.AddWithValue ("", this.LastTimestamp);
			}

			var datatable = new DataTable ();
			cmd.Fill (datatable);

			return datatable;
		}

		public override void SaveState (DataTable databaseTable, ImportProcessorBase processor)
		{
			if (!databaseTable.Columns.Contains ("timestamp") || databaseTable.Columns["timestamp"].DataType != typeof(Int32))
				return;

			foreach (DataRow row in databaseTable.Rows) 
			{
				if(row["timestamp"] != DBNull.Value)
					this.LastTimestamp = Math.Max(this.LastTimestamp, (int)row["timestamp"]);
			}
		}

		public override string GetStatusInfo ()
		{
			return this.ConnectionString;
		}
	}
}

