using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderCouponPercentPlugin : OrderPluginBase
	{
		protected NumberFormatInfo _NumberFormat;

		public OrderCouponPercentPlugin () : base()
		{
			this.Name = "Gutschein-Prozent Plugin";
			this.Description = "Wundelt Gutschein-Ermäßigungen pro Artikelzeile in Rabatte (%) um (für Winline)";
			this.SettingsDialogType = typeof(PluginSettingsDialog);

			this._NumberFormat = new System.Globalization.NumberFormatInfo();
			this._NumberFormat.NumberDecimalSeparator = ".";
		}

		public override void ProcessOrderNode (XmlNode input, OrderProcessor processor)
		{
			var percentageSum = 0m;
			foreach (XmlNode item in input.SelectNodes("items/item"))
			{
				var sum = Decimal.Parse(item.SelectSingleNode("price_sum_gross").InnerText, _NumberFormat);
				var discount = Decimal.Parse(item.SelectSingleNode("price_couponvalue_gross").InnerText, _NumberFormat);

				var percentage = 0m;
                if(discount > 0)
                {
					percentage = Decimal.Round (discount / sum * 100, 2, MidpointRounding.AwayFromZero);
					percentageSum += Decimal.Round (sum * percentage / 100, 2, MidpointRounding.AwayFromZero);
                }

				var node = input.OwnerDocument.CreateElement ("price_couponvalue_percent");
				node.InnerText = (-percentage).ToString (this._NumberFormat);
				item.AppendChild(node);
            }

			var remainingValueNode = input.OwnerDocument.CreateElement ("price_couponvalue_remaining");
			var orderDiscountSum = Decimal.Parse (input.SelectSingleNode ("price_couponvalue_gross").InnerText, _NumberFormat);
			remainingValueNode.InnerText = (percentageSum - orderDiscountSum).ToString (_NumberFormat);
			input.AppendChild(remainingValueNode);
		}
	}
}