using System;
using System.Collections.Generic;
using System.Reflection;

namespace Gustav_Connector
{
	public class PluginsBrowser
	{
		private static PluginsBrowser _Current = new PluginsBrowser();

		public static PluginsBrowser Current { 
			get { return _Current; }
		}

		public List<OrderPluginBase> OrderPlugins { get; set; }
		public List<ReaderPluginBase> ReaderPlugins { get; set; }
		public List<WriterPluginBase> WriterPlugins { get; set; }
		public List<CommonPluginBase> CommonPlugins { get; set; }

		public Dictionary<string, PluginBase> AllPluginInstances { get; set; }

		public PluginsBrowser ()
		{
			OrderPlugins = new List<OrderPluginBase> ();
			ReaderPlugins = new List<ReaderPluginBase> ();
			WriterPlugins = new List<WriterPluginBase> ();
			CommonPlugins = new List<CommonPluginBase> ();

			var types = Assembly.GetCallingAssembly().GetTypes();
			foreach(var type in types)
			{
				switch (type.BaseType.ToString ()) {
				case "Gustav_Connector.OrderPluginBase":
					OrderPlugins.Add ((OrderPluginBase)Activator.CreateInstance (type));
					break;
				case "Gustav_Connector.ReaderPluginBase":
					ReaderPlugins.Add ((ReaderPluginBase)Activator.CreateInstance (type));
					break;
				case "Gustav_Connector.WriterPluginBase":
					WriterPlugins.Add ((WriterPluginBase)Activator.CreateInstance (type));
					break;
				case "Gustav_Connector.CommonPluginBase":
					CommonPlugins.Add ((CommonPluginBase)Activator.CreateInstance (type));
					break;
				}
			}

			this.AllPluginInstances = new Dictionary<string, PluginBase> ();
		}

	}
}

