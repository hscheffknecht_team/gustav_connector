using System;
using System.Data;

namespace Gustav_Connector
{
	public class CommonPluginBase : PluginBase
	{
		protected bool _CreatesOrDeletesRows = false;

		public Type SettingsWidgetType { get; set; }

		public bool CreatesOrDeletesRows {
			get { return _CreatesOrDeletesRows; }
		}

		public CommonPluginBase ()
		{
		}

		public virtual void ProcessTable (DataTable table, ImportProcessorBase processor) 
		{
		}

		public virtual void PrepeareDeleteCache (DataTable table, ImportProcessorBase processor)
		{
		}
	}
}
