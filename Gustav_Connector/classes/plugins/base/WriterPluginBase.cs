using System;

namespace Gustav_Connector
{
	public class WriterPluginBase : PluginBase
	{
		public Type SettingsDialogType { get; set; }

		public WriterPluginBase ()
		{
		}

		public virtual void Process(OrderProcessor processor) 
		{
		}
	}
}

