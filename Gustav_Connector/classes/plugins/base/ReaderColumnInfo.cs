using System;

namespace Gustav_Connector
{
	public class ReaderColumnInfo
	{
		public string ColumnName { get; set; }
		public string Title { get; set; }
		public Type DataType { get; set; }
		
		public ReaderColumnInfo (string columnName, string title)
		{
			this.ColumnName = columnName;
			this.Title = title;
			this.DataType = typeof(String);
		}

		public ReaderColumnInfo (string columnName, string title, Type dataType)
		{
			this.ColumnName = columnName;
			this.Title = title;
			this.DataType = dataType;
		}
	}
}

