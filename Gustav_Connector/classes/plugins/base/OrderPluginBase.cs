using System;
using System.Collections.Generic;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderPluginBase : PluginBase
	{
		public Type SettingsDialogType { get; set; }

		public virtual void ProcessOrderNode(XmlNode input, OrderProcessor processor)
		{
		}

		public virtual void ProcessEnd(OrderProcessor processor)
		{
		}

		public virtual void CheckAfterExport()
		{
		}
	}

}

