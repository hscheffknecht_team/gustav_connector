using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class PluginBase : IXmlSerializable
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		public string FilterField1 { get; set; }
		public string FilterOperator1 { get; set; }
		public string FilterValue1 { get; set; }
		public string FilterValueField1 { get; set; }

		public string FilterField2 { get; set; }
		public string FilterOperator2 { get; set; }
		public string FilterValue2 { get; set; }
		public string FilterValueField2 { get; set; }

		public SettingsCollection Container { get; set; }

		public PluginBase ()
		{
			System.Threading.Thread.Sleep (new TimeSpan (0, 0, 0, 0, 10)); // Get sure that the tick number is different than last time when loading many plugins

			this.Id = DateTime.Now.Ticks.ToString();
			this.FilterOperator1 = "=";
			this.FilterValue1 = "";
			this.FilterOperator2 = "=";
			this.FilterValue2 = "";
		}
		
		public virtual string GetStatusInfo()
		{
			return "Keine Informationen verfügbar";
		}
		
		public virtual string GetFilterInfo()
		{
			if(String.IsNullOrEmpty(FilterField1))
				return "Kein Filter";
			else
				return String.Format(
					"{0} {1} \"{2}\"",
					FilterField1,
					FilterOperator1.Replace("StartsWith", "beginnt mit"),
					FilterValue1
					);
		}

		public virtual bool ProcessFilter(XmlNode orderNode)
		{
			// No filter defined
			if (String.IsNullOrEmpty (this.FilterField1))
				return true;

			// Get value
			var valueNode = orderNode.SelectSingleNode (this.FilterField1);
			var value = "";
			if (valueNode != null)
				value = valueNode.InnerText;

			// Process filter
			var result = true;

			// Destination value
			var destValue = this.FilterValue1;
			XmlNode destNode;
			if (!String.IsNullOrEmpty (this.FilterValueField1)) {
				destNode = orderNode.SelectSingleNode (this.FilterValueField1);
				if (destNode != null) {
					destValue += destNode.InnerText;
				}
			}

			switch (this.FilterOperator1) {
				case "=":
					result = String.Equals (destValue, value, StringComparison.CurrentCultureIgnoreCase);
					break;
				case "!=":
					result = !String.Equals (destValue, value, StringComparison.CurrentCultureIgnoreCase);
					break;
				case "StartsWith":
					result = value.StartsWith (destValue, StringComparison.CurrentCultureIgnoreCase);
					break;
				default:
					throw new Exception ("Ungültiger Operand in Filter: " + this.FilterOperator1);
			}
		

			if (!result || String.IsNullOrEmpty(this.FilterField2)) {
				return result;
			}

			// Get value
			valueNode = orderNode.SelectSingleNode(this.FilterField2);
			value = "";
			if(valueNode != null)
				value = valueNode.InnerText;

			// Destination value
			destValue = this.FilterValue2;
			if (!String.IsNullOrEmpty(this.FilterValueField2)) {
				destNode = orderNode.SelectSingleNode (this.FilterValueField2);
				if (destNode != null) {
					destValue += destNode.InnerText;
				}
			}

			switch (this.FilterOperator2) {
				case "=":
					result = String.Equals (destValue, value, StringComparison.CurrentCultureIgnoreCase);
					break;
				case "!=":
					result = !String.Equals (destValue, value, StringComparison.CurrentCultureIgnoreCase);
					break;
				case "StartsWith":
					result = value.StartsWith (destValue, StringComparison.CurrentCultureIgnoreCase);
					break;
				default:
					throw new Exception ("Ungültiger Operand in Filter: " + this.FilterOperator2);
			}

			return result;
		}

		public virtual void SaveSettings()
		{
		}

		protected string GetConfigKey(string subkey)
		{
			return String.Format("{0}_{1}_{2}", this.GetType().Name, this.Id, subkey);
		}

		#region IXmlSerializable

		public XmlSchema GetSchema () 
		{
			return null;
		}

		public virtual void ReadXmlSub (XmlReader reader, string nodeName)
		{
		}

		public virtual void WriteXmlSub (XmlWriter writer)
		{
		}
		
		public virtual void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					string nodeName = reader.Name;
					reader.Read();

					switch (nodeName) {						
						case "FilterField1":
							this.FilterField1 = reader.Value;
							break;

						case "FilterOperator1":
							this.FilterOperator1 = reader.Value;
							break;

						case "FilterValue1":
							this.FilterValue1 = reader.Value;
							break;	

						case "FilterValueField1":
							this.FilterValueField1 = reader.Value;
							break;		

						case "FilterField2":
							this.FilterField2 = reader.Value;
							break;

						case "FilterOperator2":
							this.FilterOperator2 = reader.Value;
							break;

						case "FilterValue2":
							this.FilterValue2 = reader.Value;
							break;

						case "FilterValueField2":
							this.FilterValueField2 = reader.Value;
							break;
					}

					this.ReadXmlSub(reader, nodeName);

					if (reader.NodeType != XmlNodeType.EndElement) {
						reader.Read ();
					}
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
			
			reader.ReadEndElement();
		}
		
		public virtual void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement(this.GetType().Name);

			writer.WriteElementString("FilterField1", this.FilterField1);
			writer.WriteElementString("FilterOperator1", this.FilterOperator1);
			writer.WriteElementString("FilterValue1", this.FilterValue1);
			writer.WriteElementString("FilterValueField1", this.FilterValueField1);
			writer.WriteElementString("FilterField2", this.FilterField2);
			writer.WriteElementString("FilterOperator2", this.FilterOperator2);
			writer.WriteElementString("FilterValue2", this.FilterValue2);
			writer.WriteElementString("FilterValueField2", this.FilterValueField2);

			this.WriteXmlSub(writer);

			writer.WriteEndElement();
		}


		#endregion
	}
}

