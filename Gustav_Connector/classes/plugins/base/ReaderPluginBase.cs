using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Gustav_Connector
{
	public class ReaderPluginBase : PluginBase
	{
		public Type SettingsDialogType { get; set; }
		public int LastTimestamp { get; set; }
		public DbQueryData QueryData { get; set; }
		public EntityKeysCache KeysCache { get; set; }

		/// <summary>
		/// Redirects output to a temporary table
		/// </summary>
		/// <value>The name of the temporary table.</value>
		public string TemporaryTableName { get; set; }

		public ReaderPluginBase ()
		{
			this.QueryData = new DbQueryData ();
		}

		public virtual DataTable GetData(ImportProcessorBase processor)
		{
			return new DataTable ();
		}

		public virtual void SaveState(DataTable databaseTable, ImportProcessorBase processor)
		{
			if (!databaseTable.Columns.Contains ("timestamp")) {
				throw new Exception ("Es wurde keine gültige Int32 timestamp Spalte gefunden");
			}

			foreach (DataRow row in databaseTable.Rows) 
			{
				if(row["timestamp"] is Int32)
					this.LastTimestamp = Math.Max(this.LastTimestamp, (int)row["timestamp"]);
			}
		}

		public virtual Dictionary<string,string>[] GetDeletedKeys(ImportProcessorBase processor) {

			return new Dictionary<string,string>[0];
		}

		public virtual void FlushCache() {

			if (this.KeysCache != null)
				this.KeysCache.ClearItems ();
		}

		public virtual void FlushCacheItems(string[] keys) {
			if (this.KeysCache != null)
				foreach (string key in keys)
					this.KeysCache.DeleteItem (key);
		}

		public virtual void SaveCache() {

			if (this.KeysCache != null) {
				this.KeysCache.Save ();
			}
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch (nodeName) {
				case "TemporaryTableName":
					this.TemporaryTableName = reader.Value;
					break;
			}

			base.ReadXmlSub (reader, nodeName);
		}

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("TemporaryTableName", this.TemporaryTableName);

			base.WriteXmlSub (writer);
		}

		public virtual string[] GetCurrentKeys(ImportProcessorBase processor)
		{
			throw new NotSupportedException (this.GetType ().Name + " unterstützt diese Funktion nicht");
		}
	}
}

