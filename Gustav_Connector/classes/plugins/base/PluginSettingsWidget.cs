using System;

namespace Gustav_Connector
{
	public abstract class PluginSettingsWidget : Gtk.Bin
	{
		public virtual PluginBase CurrentPlugin { get; set; }
		public ImportProcessorBase CurrentProcessor { get; set; }

		public abstract void Load();
		public abstract void Save();
	}
}

