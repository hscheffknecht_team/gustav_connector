using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class FilesReaderSettings : PluginSettingsDialog
	{
		private FilesReaderPlugin _CurrentPlugin;

		public FilesReaderSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (FilesReaderPlugin)value;
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			this.txtDirectory.Text = this._CurrentPlugin.Directory;
			this.txtAllowedExtensions.Text = this._CurrentPlugin.AllowedExtensions;

			ShowColumns ();
		}

		private void Save()
		{
			this._CurrentPlugin.Directory = this.txtDirectory.Text;
			this._CurrentPlugin.AllowedExtensions = this.txtAllowedExtensions.Text;

			this._CurrentPlugin.SelectedFields = new Dictionary<string, string> ();
			this._CurrentPlugin.Prefixes = new Dictionary<string, string> ();

			foreach (Gtk.Widget w in table1.AllChildren) {
				if (w.Name.StartsWith ("select_")) {
					var dd = (Gtk.ComboBox)w;
					var key = dd.Name.Split ('_') [1];

					var selectedField = this._FindFieldKey (dd.ActiveText);
					if (selectedField != null)
						this._CurrentPlugin.SelectedFields [key] = selectedField;
				} else if (w.Name.StartsWith ("prefix_")) {
					var e = (Gtk.Entry)w;
					var key = e.Name.Split ('_') [1];
					this._CurrentPlugin.Prefixes [key] = e.Text;
				}
			}
		}

		private string _FindFieldKey(string fieldTitle) {

			foreach (var key in this._CurrentPlugin.Fields.Keys) {
				if (this._CurrentPlugin.Fields [key] == fieldTitle)
					return key;
			}

			return null;
		}

		public void ShowColumns()
		{
			uint index = 2;
			var data = this._CurrentPlugin.SelectedFields;

			/*
			 * SELECT
			 */
			foreach (ReaderColumnInfo col in this.SelectColumns) {

				this._InsertRow (index);

				// Label
				var label = new Gtk.Label (col.Title);

				// Prefix input
				var prefix = new Gtk.Entry ();
				prefix.Name = "prefix_" + col.ColumnName;
				if (this._CurrentPlugin.Prefixes.ContainsKey (col.ColumnName)) {
					prefix.Text = this._CurrentPlugin.Prefixes [col.ColumnName];
				}

				// DropDown
				var dd = Gtk.ComboBox.NewText ();
				dd.Name = "select_" + col.ColumnName;
				dd.AppendText ("");

				foreach (var fieldKey in this._CurrentPlugin.Fields.Keys) {
					dd.AppendText(this._CurrentPlugin.Fields[fieldKey]);

					if (data.ContainsKey (col.ColumnName) && data [col.ColumnName] == fieldKey)
						dd.Active = dd.Model.IterNChildren() - 1;
				}

				// Add to table
				table1.Attach (label, 0, 1, index, index+1);
				if (col.DataType != typeof(byte[])) {
					table1.Attach (prefix, 1, 2, index, index + 1);
				}
				table1.Attach (dd, 2, 3, index, index+1);
				index++;
			}

			/*
			 * Show all
			 */
			table1.ShowAll ();
		}

		private void _InsertRow(uint index)
		{
			table1.NRows++;

			foreach (Gtk.Widget widget in table1.AllChildren) {

				var child = (Gtk.Table.TableChild)table1 [widget];

				if (child.TopAttach >= index) {
					child.BottomAttach++;
					child.TopAttach++;
				}
			}
		}
	}
}

