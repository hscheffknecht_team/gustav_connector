using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class FilesReaderPlugin : ReaderPluginBase
	{
		public string Directory { get; set; }

		public Dictionary<string,string> Fields { get; set; }
		public Dictionary<string,string> SelectedFields { get; set; }
		public Dictionary<string,string> Prefixes { get; set; }
		public string AllowedExtensions = "jpg,jpeg,png,tif,tiff";

		private ImagesLoader _ImagesLoader = null;

		public string[] _GetAllowedExtensionsArray() {

			if (this.AllowedExtensions == null) {
				return new String[0];
			} else {
				return this.AllowedExtensions.Split (',');
			}
		}

		public FilesReaderPlugin ()
		{
			this.Name = "Files Reader";
			this.Description = "Liest Dateidaten von Bildern aus einem Verzeichnis";
			this.SettingsDialogType = typeof(FilesReaderSettings);

			this.Fields = new Dictionary<string, string> ();
			this.Fields.Add ("imagekey", "Bild-Key");
			this.Fields.Add ("productkey", "Produkt-Key");
			this.Fields.Add ("filename", "Dateiname");
			this.Fields.Add ("filesize", "Dateigröße");
			this.Fields.Add ("filedata", "Dateiinhalt");
			this.Fields.Add ("sort", "Sortierung");
			this.Fields.Add ("exif-descr", "EXIF Beschreibung");
			this.Fields.Add ("plugindata", "Plugin-Daten");
			this.Fields.Add ("lastchange", "Letzte Änderung");

			this.SelectedFields = new Dictionary<string, string> ();
			this.Prefixes = new Dictionary<string, string> ();
		}

		private EntityKeysCache GetKeysCache(bool refresh) {

			if(this.KeysCache == null || refresh)
				this.KeysCache = new EntityKeysCache ("files-reader-"+this.Id);

			return this.KeysCache;
		}

		#region XmlSerializeable

		public override void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();

			this.SelectedFields.Clear ();
			this.Prefixes.Clear ();

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch (reader.Name) {

						case "Id":
							reader.Read ();
							this.Id = reader.Value;
							break;

						case "Directory":
							reader.Read ();
							this.Directory = reader.Value;
							break;

						case "AllowedExtensions":
							reader.Read ();
							this.AllowedExtensions = reader.Value;
							break;
					}

					if (reader.Name.StartsWith ("SelectedField_")) {

						var key = reader.Name.Split ("_".ToCharArray (), 2) [1];
						reader.Read ();
						this.SelectedFields.Add (key, reader.Value);
					}

					if (reader.Name.StartsWith ("Prefix_")) {

						var key = reader.Name.Split ("_".ToCharArray (), 2) [1];
						reader.Read ();
						this.Prefixes.Add (key, reader.Value);
					}

					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}

			reader.ReadEndElement();
		}

		public override void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement("ColumnInfo");
			writer.WriteElementString("Id", this.Id);
			writer.WriteElementString("Directory", this.Directory);
			writer.WriteElementString("AllowedExtensions", this.AllowedExtensions);

			foreach (var key in this.SelectedFields.Keys) {
				writer.WriteElementString ("SelectedField_" + key, this.SelectedFields [key]);
			}

			foreach (var key in this.Prefixes.Keys) {
				writer.WriteElementString ("Prefix_" + key, this.Prefixes [key]);
			}

			writer.WriteEndElement();
		}

		#endregion

		public override string GetStatusInfo ()
		{
			return this.Directory;
		}

		public override DataTable GetData (ImportProcessorBase processor)
		{
			return _GetData (processor, true);
		}

		public DataTable GetAllData(ImportProcessorBase processor)
		{
			var cache = this.GetKeysCache (false);
			cache.ClearItems ();
			var result = _GetData (processor, false);
			this.KeysCache = null;

			return result;
		}

		private DataTable _GetData (ImportProcessorBase processor, bool renewCache)
		{
			// Load cache
			var oldCache = this.GetKeysCache (renewCache);

			// Create images loader instance
			this._ImagesLoader = new ImagesLoader (this.Directory, oldCache.GetAllItems(), this._GetAllowedExtensionsArray());

			// Load new images table
			var imgTable = this._ImagesLoader.GetDataAsTable (false);

			// Create table for remapped columns
			var datatable = new DataTable ();

			// Create columns
			foreach (var col in processor.GetSelectColumns()) {
				datatable.Columns.Add (col.ColumnName, col.DataType);
			}

			// Copy data and remap columns
			foreach (DataRow srcRow in imgTable.Rows) {

				var dstRow = datatable.NewRow ();

				foreach (var key in this.SelectedFields.Keys) {

					if (datatable.Columns [key].DataType == typeof(byte[])) {
						if (srcRow [this.SelectedFields [key]] is byte[]) {
							dstRow [key] = srcRow [this.SelectedFields [key]];
						} else {
							dstRow [key] = System.Text.Encoding.Default.GetBytes (srcRow [this.SelectedFields [key]].ToString ());
						}
					} else {
						dstRow [key] = srcRow [this.SelectedFields [key]].ToString ();

						if (this.Prefixes.ContainsKey (key)) {
							dstRow [key] = this.Prefixes [key] + dstRow [key];
						}
					}
				}

				datatable.Rows.Add (dstRow);
			}

			// Return remapped data table
			return datatable;
		}

		public override Dictionary<string,string>[] GetDeletedKeys (ImportProcessorBase processor)
		{
			// Get current available images
			var curImages = this._ImagesLoader.GetDataAsDictionary (true);

			// Load keys from cache
			var oldCache = this.GetKeysCache (false);

			// Find keys to delete
			var keyColumns = processor.GetKeyColumns ();
			var deletedKeys = new Dictionary<string,Dictionary<string,string>> ();
			foreach (string key in oldCache.GetAllItems().Keys) {
				var found = curImages.ContainsKey (key.ToLower());

				if (!found) {
					var deletedKey = new Dictionary<string,string> ();
					foreach (var keyColumn in keyColumns) {
						deletedKey.Add (keyColumn.ColumnName, oldCache.GetItem(key)[this.SelectedFields[keyColumn.ColumnName]]);
						if (this.Prefixes.ContainsKey (keyColumn.ColumnName)) {
							deletedKey [keyColumn.ColumnName] = this.Prefixes [keyColumn.ColumnName] + deletedKey [keyColumn.ColumnName];
						}
					}
					deletedKeys.Add (key, deletedKey);
				}
			}

			// Delete from cache
			foreach (var key in deletedKeys.Keys) {
				oldCache.DeleteItem (key);
			}

			// Return array
			var arr = new Dictionary<string,string>[deletedKeys.Count];
			deletedKeys.Values.CopyTo (arr, 0);
			return arr;
		}

		public override void FlushCache ()
		{
			var cache = this.GetKeysCache (false);
			cache.ClearItems ();
			cache.Save ();
		}

		public override void FlushCacheItems (string[] keys)
		{
			var cache = this.GetKeysCache (false);
			foreach (string key in keys)
				cache.DeleteItem (key);
			cache.Save ();
		}

		public override void SaveState (DataTable databaseTable, ImportProcessorBase processor)
		{
			// Get cache
			var cache = this.GetKeysCache (false);

			// Get all current available images as dictionary
			var allImages = this._ImagesLoader.GetDataAsDictionary (true);

			// Save current images in cache
			cache.SetAllItems (allImages);
			cache.Save ();
		}

		public override string[] GetCurrentKeys (ImportProcessorBase processor)
		{
			// Load keys from cache
			var oldCache = this.GetKeysCache (false);

			// Find keys to delete
			var currentKeys = oldCache.GetAllItems ().Keys;

			// Return keys
			var result = new String[currentKeys.Count];
			currentKeys.CopyTo (result, 0);
			return result;
		}
	}
}
