using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class OrderWinlineSettings : PluginSettingsDialog
	{
		private OrderWinlinePlugin _CurrentPlugin;
		
		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (OrderWinlinePlugin)value;
				_Load ();
			}
		}

		public OrderWinlineSettings ()
		{
			this.Build ();
		}

		private void _Load() 
		{
			txtDbData.Text = this._CurrentPlugin.DbNameData;
			txtMesoComp.Text = this._CurrentPlugin.MesoCompany;

			txtNrPrefix.Text = this._CurrentPlugin.CustomerNrPrefix;
			nrCustomerNr.Value = this._CurrentPlugin.FirstCustomerNr;
			chkCustomerDesc.Active = this._CurrentPlugin.CustomerNrDesc;
			chkCusterOnlyInRange.Active = this._CurrentPlugin.CustomerOnlyInRange;

			nrText1.Value = this._CurrentPlugin.TextModule1Number;
			txtText1.Text = this._CurrentPlugin.TextModule1Placeholder;
		}

		private void _Save()
		{
			this._CurrentPlugin.DbNameData = txtDbData.Text;
			this._CurrentPlugin.MesoCompany = txtMesoComp.Text;

			this._CurrentPlugin.CustomerNrPrefix = txtNrPrefix.Text;
			this._CurrentPlugin.FirstCustomerNr = (int)nrCustomerNr.Value;
			this._CurrentPlugin.CustomerNrDesc = chkCustomerDesc.Active;
			this._CurrentPlugin.CustomerOnlyInRange = chkCusterOnlyInRange.Active;

			this._CurrentPlugin.TextModule1Number = (int)nrText1.Value;
			this._CurrentPlugin.TextModule1Placeholder = txtText1.Text;
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			_Save();
			this.Destroy();
		}
	}
}

