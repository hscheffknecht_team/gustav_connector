using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderWinlinePlugin : OrderPluginBase
	{
		public string DbNameData { get; set; }
		public string DbNameSystem { get; set; }
		public string CustomerNrPrefix { get; set; }
		public int FirstCustomerNr { get; set; }
		public bool CustomerNrDesc { get; set; }
		public bool CustomerOnlyInRange { get; set; }
		public string MesoCompany { get; set; }
		public string TextModule1Placeholder { get; set; }
		public int TextModule1Number { get; set; }

		private OrderProcessor _Processor;
		private string _DataConnectionString;
		private string _WriterConnectionString;
		private int _MesoYear;
		private string _CustomersImportTable;
		private int _LastCustomerNr;
		private List<int> _CustomerNrsList;

		public OrderWinlinePlugin ()
		{
			this.Name = "Winline Auftragsimport Plugin";
			this.Description = "Führt diverse Aufgaben für den Auftragsimport in Mesonic Winline durch";
			this.SettingsDialogType = typeof(OrderWinlineSettings);
			this.CustomerNrPrefix = "";
		}

		
		#region XmlSerializeable
		
		public override void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					string nodeName = reader.Name;
					reader.Read();

					switch (nodeName) {						
						case "DbNameData":
							this.DbNameData = reader.Value;
							break;
						
						case "DbNameSystem":
							this.DbNameSystem = reader.Value;
							break;
						
						case "MesoCompany":
							this.MesoCompany = reader.Value;
							break;
						
						case "FirstCustomerNr":
							this.FirstCustomerNr = Int32.Parse (reader.Value);
							break;
						
						case "CustomerNrPrefix":
							this.CustomerNrPrefix = reader.Value;
							break;

						case "CustomerNrDesc":
							this.CustomerNrDesc = Boolean.Parse (reader.Value);
							break;

						case "CustomerOnlyInRange":
							this.CustomerOnlyInRange = Boolean.Parse (reader.Value);
							break;
						
						case "TextModule1Placeholder":
							this.TextModule1Placeholder = reader.Value;
							break;
						
						case "TextModule1Number":
							this.TextModule1Number = Int32.Parse (reader.Value);
							break;
					}

					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
			
			reader.ReadEndElement();
		}
		
		public override void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement ("ColumnInfo");
			writer.WriteElementString ("DbNameData", this.DbNameData);
			writer.WriteElementString ("DbNameSystem", this.DbNameSystem);
			writer.WriteElementString ("MesoCompany", this.MesoCompany);
			writer.WriteElementString ("CustomerNrPrefix", this.CustomerNrPrefix);
			writer.WriteElementString ("FirstCustomerNr", this.FirstCustomerNr.ToString());
			writer.WriteElementString ("CustomerNrDesc", this.CustomerNrDesc.ToString());
			writer.WriteElementString ("CustomerOnlyInRange", this.CustomerOnlyInRange.ToString ());
			writer.WriteElementString ("TextModule1Placeholder", this.TextModule1Placeholder);
			writer.WriteElementString ("TextModule1Number", this.TextModule1Number.ToString());
			writer.WriteEndElement();
		}
		
		#endregion

		public override void ProcessEnd (OrderProcessor processor)
		{
			// Save processor
			this._Processor = processor;

            // Shrink postal code to correct size
            if(OrderSettings.Current.CustomersColumns.Contains(new ColumnInfo() { Name="Postleitzahl" })) {
                foreach(DataRow row in this._Processor.AllCustomers.Rows)
                {
                    if(((string)row["Postleitzahl"]).Length > 10) {
                        row["Postleitzahl"] = ((string)row["Postleitzahl"]).Substring(0, 10);
                    }
                }
            }

            if(OrderSettings.Current.OrdersColumns.Contains(new ColumnInfo() { Name="FaktPLZ" }) && OrderSettings.Current.OrdersColumns.Contains(new ColumnInfo() { Name="LiefPLZ" })) {
                foreach(DataRow row in this._Processor.AllOrders.Rows)
                {
                    if(((string)row["FaktPLZ"]).Length > 10) {
                        row["FaktPLZ"] = ((string)row["FaktPLZ"]).Substring(0, 10);
                    }
                    if(((string)row["LiefPLZ"]).Length > 10) {
                        row["LiefPLZ"] = ((string)row["LiefPLZ"]).Substring(0, 10);
                    }
                }
            }

			// Check settings
			_CheckSettings();

			// Load data
			_LoadData();

			// Find customer numbers
			_FindCustomerNumbers();

			// Fill text module and order reference placeholders
			_FillPlaceholders();

			// Delete cache
			this._CustomerNrsList = null;

			// Shorten phone number to 30 chars
			if(OrderSettings.Current.CustomersColumns.Contains(new ColumnInfo() { Name="Telefon" })) {
				foreach(DataRow row in this._Processor.AllCustomers.Rows)
				{
					if(row["Telefon"] != DBNull.Value) {
						var phonenr = (string)row["Telefon"];
						if(phonenr.Length > 30) phonenr = phonenr.Substring(0, 30);
						row["Telefon"] = phonenr;
					}
				}
			}

			// Replace pricelist, "Belegart" and payment conditions for existing customers
			_LoadExistingCustomerValues ();

		}

		/// <summary>
		/// Load pricelist, "Belegart" and payment conditions from existing customers and write them to orders and customers tables
		/// </summary>
		private void _LoadExistingCustomerValues()
		{
            var query = @"SELECT c066 as pl, c077 as belegart, c100 as pcFibu, c107 as pcFakt, c065 as vertreter, c116 as email,
				(SELECT c001 FROM t226 WHERE c010 = v050.c107 AND mesocomp = @mesocomp and mesoyear = @mesoyear) as pcFaktName
                FROM v050 
                WHERE c002 = @custnr and mesocomp = @mesocomp and mesoyear = @mesoyear";

            var conn = new SqlConnection(this._DataConnectionString);
            var cmd = new SqlCommand(query, conn);

            cmd.Parameters.Add("@custnr", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
            cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;

            conn.Open();

			foreach (DataRow row in this._Processor.AllCustomers.Rows)
            {
                var custnr = (string)row["Kontonummer"];

                cmd.Parameters["@custnr"].Value = custnr;
                var dr = cmd.ExecuteReader();
                if(dr.Read()) 
                {
                    // Update customer
					if (row.Table.Columns.Contains ("Preisliste")) {
						row ["Preisliste"] = dr ["pl"];
					}
					if (row.Table.Columns.Contains ("Belegart")) {
						row ["Belegart"] = dr ["belegart"];
					}
					if (row.Table.Columns.Contains ("ZahlungskonditionFIBU")) {
						row ["ZahlungskonditionFIBU"] = dr ["pcFibu"];
					}
					if (row.Table.Columns.Contains ("ZahlungskonditionFAKT")) {
						row ["ZahlungskonditionFAKT"] = dr ["pcFakt"];
					}
					if (row.Table.Columns.Contains ("Vertreter")) {
						row ["Vertreter"] = dr ["vertreter"];
					}

					if (row.Table.Columns.Contains("EMailAdresse") && String.IsNullOrEmpty(row["EMailAdresse"].ToString())) {
						row ["EMailAdresse"] = dr ["email"];
					}

                    // Update Belegart in orders
                    var orderRows = this._Processor.AllOrders.Select("Kontonummer='" + custnr + "'");

                    foreach(DataRow orderRow in orderRows)
                    {
						if (orderRow.Table.Columns.Contains ("Belegart")) {
							orderRow ["Belegart"] = dr ["belegart"];
						}

                        // Search for paymentCondition placeholder
                        foreach(DataColumn col in this._Processor.AllOrders.Columns)
                        {
                            if(orderRow[col].ToString().Contains("{paymentCondition}"))
                                orderRow[col] = orderRow[col].ToString().Replace("{paymentCondition}", (string)dr["pcFaktName"].ToString());
                        }
                    }
                }
                dr.Close();
            }

			// Load data for orders without customers
			if (this._Processor.AllOrders.Columns.Contains ("Zahlungskondition") && this._Processor.AllOrders.Columns.Contains ("Kontonummer")) {
				var orders = this._Processor.AllOrders.Select ("(Zahlungskondition = '' OR Zahlungskondition IS NULL)");
				foreach (DataRow row in orders) {
					cmd.Parameters["@custnr"].Value = row["Kontonummer"].ToString();
					var dr = cmd.ExecuteReader();
					if (dr.Read ()) {
						row ["Zahlungskondition"] = dr ["pcFaktName"];
					}
					dr.Close ();
				}
			}

            conn.Close();
		}

		private void _CheckSettings() 
		{
			// Check orders table
			foreach(string colname in new String[] { "OrderID" })
			{
				if(!OrderSettings.Current.OrdersColumns.Contains(new ColumnInfo() { Name=colname }))
					throw new Exception("Die Auftragstabelle muss eine Spalte \"" + colname + "\" enthalten!");
			}

			// Check customers table
			foreach(string colname in new String[] { "Kontonummer", "Kontoname", "Straße", "Postleitzahl", "OrderID" })
			{
				if(!OrderSettings.Current.CustomersColumns.Contains(new ColumnInfo() { Name=colname }))
					throw new Exception("Die Kundentabelle muss eine Spalte \"" + colname + "\" enthalten!");
			}
		}

		private void _LoadData()
		{
			this._LoadWritersData();
			this._MesoYear = this._GetMesoYear();
		}

		private void _FindCustomerNumbers()
		{
			// Reset
			this._LastCustomerNr = 0;

			// Iterate customers
			foreach (DataRow row in this._Processor.AllCustomers.Rows) {
				if (this.FirstCustomerNr > 0 && String.IsNullOrEmpty (row ["Kontonummer"].ToString())) {
					// Find customer number for existing customers
					if (!_SearchCustomer (row)) {
						// Find next customer number for new customers
						row ["Kontonummer"] = this.CustomerNrPrefix + _GetNextCustomerNr ();
					}

					// Add customer number to order
					_UpdateOrder (row);
				}
			}
		}

		private bool _SearchCustomer(DataRow row) 
        {
			// Search in winline DB
			var query = @"SELECT c.c002 FROM t055 c 
				INNER JOIN t051 a ON c.c002 = a.c001 AND c.mesocomp = a.mesocomp AND c.mesoyear = a.mesoyear
				WHERE c.c003 = @name AND a.c050 = @street AND a.c051 = @postal AND c.c105 is null and c.c004 = 2
					AND c.mesocomp = @mesocomp AND c.mesoyear = @mesoyear
				ORDER BY c.c002 DESC";

			var conn = new SqlConnection(this._DataConnectionString);
			var cmd = new SqlCommand(query, conn);

			cmd.Parameters.Add("@name", SqlDbType.VarChar, 255).Value = row["Kontoname"].ToString();
			cmd.Parameters.Add("@street", SqlDbType.VarChar, 255).Value = row["Straße"].ToString();
			cmd.Parameters.Add("@postal", SqlDbType.VarChar, 10).Value = row["Postleitzahl"].ToString();
			cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
			cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;

			conn.Open();
			var result = cmd.ExecuteScalar();
			conn.Close();

			if(result != null && result != DBNull.Value && _IsValidCustomerNumber(result.ToString()))
			{
				// Save value
				row["Kontonummer"] = result;
				return true;
			}

			// Search in internal table
			query = String.Format(
				"Kontoname='{0}' AND [Straße]='{1}' AND Postleitzahl='{2}' AND Kontonummer is not null AND Kontonummer <> ''",
				row["Kontoname"].ToString().Replace("'", "''"),
				row["Straße"].ToString().Replace("'", "''"),
				row["Postleitzahl"].ToString().Replace("'","''")
				);
			var dataRows = this._Processor.AllCustomers.Select(query);
			if(dataRows.Length > 0 && _IsValidCustomerNumber(dataRows[0]["Kontonummer"].ToString()))
			{
				row["Kontonummer"] = dataRows[0]["Kontonummer"];
				return true;
			}

			// Search in import DB
			query = String.Format(
				"SELECT * FROM [{0}] WHERE Kontoname=@name AND [Straße]=@street AND Postleitzahl=@postal",
				this._CustomersImportTable
				);

			conn = new SqlConnection(this._WriterConnectionString);
			cmd = new SqlCommand(query, conn);
			
			cmd.Parameters.Add("@name", SqlDbType.VarChar, 255).Value = row["Kontoname"].ToString();
			cmd.Parameters.Add("@street", SqlDbType.VarChar, 255).Value = row["Straße"].ToString();
			cmd.Parameters.Add("@postal", SqlDbType.VarChar, 10).Value = row["Postleitzahl"].ToString();
			
			conn.Open();
			result = cmd.ExecuteScalar();
			conn.Close();

			if(result != null && result != DBNull.Value && _IsValidCustomerNumber(result.ToString()))
			{
				// Save value
				row["Kontonummer"] = result;
				return true;
			}

			// Customer not found
			return false;
		}

		private bool _IsValidCustomerNumber(string customerNumber)
		{
			if (CustomerOnlyInRange) {
				return _IsCustomerNumberInRange (customerNumber);
			} else {
				return true;
			}
		}

		private bool _IsCustomerNumberInRange(string customerNumber)
		{
			if (!String.IsNullOrWhiteSpace (CustomerNrPrefix)) {
				if (!customerNumber.StartsWith (CustomerNrPrefix))
					return false;

				customerNumber = customerNumber.Substring (CustomerNrPrefix.Length);
			}

			int number = 0;
			if (Int32.TryParse (customerNumber, out number)) {
				if (CustomerNrDesc)
					return number <= FirstCustomerNr;
				else 
					return number >= FirstCustomerNr;
			}

			return false;
		}

		private void _LoadWritersData()
		{
			var stringbuilder_data = new SqlConnectionStringBuilder();
			var stringbuilder_writer = new SqlConnectionStringBuilder();

			// Find writer for correct database and login
			foreach(WriterPluginBase writer in OrderSettings.Current.WriterPlugins)
			{
				if(writer is MssqlWriterPlugin)
				{
					var mssqlwriter = (MssqlWriterPlugin)writer;
					if(mssqlwriter.TargetTable == OrderTable.Customers)
					{
						stringbuilder_data.DataSource = mssqlwriter.DbServer;
						stringbuilder_data.UserID = mssqlwriter.DbUser;
						stringbuilder_data.Password = mssqlwriter.DbPassword;
						stringbuilder_data.InitialCatalog = this.DbNameData;

						stringbuilder_writer.DataSource = mssqlwriter.DbServer;
						stringbuilder_writer.UserID = mssqlwriter.DbUser;
						stringbuilder_writer.Password = mssqlwriter.DbPassword;
						stringbuilder_writer.InitialCatalog = mssqlwriter.Database;

						this._CustomersImportTable = mssqlwriter.DbTable;
					}
				}
			}

			// Check if writer was found
			if(String.IsNullOrEmpty(stringbuilder_data.DataSource))
				throw new Exception("Ein MSSQL Writer für Kunden muss vorhanden und konfiguriert sein!");

			// Save data
			this._DataConnectionString = stringbuilder_data.ConnectionString;
			this._WriterConnectionString = stringbuilder_writer.ConnectionString;
		}

		private int _GetMesoYear() 
		{
			var query = @"SELECT max(mesoyear) FROM t001 WHERE mesocomp=@mesocomp";

			var conn = new SqlConnection(this._DataConnectionString);
			var cmd = new SqlCommand(query, conn);

			cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;

			conn.Open();
			var result = cmd.ExecuteScalar();
			conn.Close();

			if(result is Int32)
				return (int)result;
			else
				throw new Exception("Der Mandant wurde nicht gefunden!");
		}

		private string _GetNextCustomerNr()
		{
			// Create list
			if(this._CustomerNrsList == null)
			{
				var list = new List<int>();

				// Fill from import table
				var query = String.Format(
					@"SELECT Kontonummer FROM [{1}] c 
						WHERE Kontonummer LIKE @prefix
						ORDER BY Kontonummer {0}",
					this.CustomerNrDesc ? "DESC" : "ASC",
					this._CustomersImportTable
					);
				
				var conn = new SqlConnection(this._WriterConnectionString);
				var cmd = new SqlCommand(query, conn);
				cmd.Parameters.Add ("@prefix", SqlDbType.VarChar, 50).Value = this.CustomerNrPrefix + "%";
				
				conn.Open();
				var reader = cmd.ExecuteReader();
				while(reader.Read())
				{
					string accountnr = reader ["Kontonummer"].ToString ().Substring (this.CustomerNrPrefix.Length);
					var intAccountNr = Int32.Parse (accountnr);
					if (!list.Contains(intAccountNr))
						list.Add(intAccountNr);
				}
				reader.Close();
				conn.Close();

				// Fill from winline
				string filter = String.IsNullOrEmpty (this.CustomerNrPrefix) ?
					"ISNUMERIC(c002) = 1" :
					"c002 LIKE @prefix";

				string orderBy = String.IsNullOrEmpty (this.CustomerNrPrefix) ?
					"CAST(c.c002 as int)" :
					"c.c002";

				query = String.Format(
					@"SELECT c.c002 FROM t055 c 
					WHERE c004 = 2 AND {2} AND LEN(c002) < 10 AND c002 {0} @minnr and mesocomp = @mesocomp and mesoyear=@mesoyear
					ORDER BY {3} {1}",
				    this.CustomerNrDesc ? "<=" : ">=",
				    this.CustomerNrDesc ? "DESC" : "ASC",
					filter,
					orderBy
					);

				conn = new SqlConnection(this._DataConnectionString);
				cmd = new SqlCommand(query, conn);

				cmd.Parameters.Add("@minnr", SqlDbType.VarChar, 50).Value = this.FirstCustomerNr;
				cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
				cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;

				if (!String.IsNullOrEmpty (this.CustomerNrPrefix)) {
					cmd.Parameters.Add ("@prefix", SqlDbType.VarChar, 50).Value = this.CustomerNrPrefix + "%";
				}

				conn.Open();
				reader = cmd.ExecuteReader();
				while(reader.Read())
				{
					string accountNr = reader ["c002"].ToString ().Substring (this.CustomerNrPrefix.Length);
					var nr = 0;
					if (Int32.TryParse(accountNr, out nr)) {
						if(!list.Contains(nr))
							list.Add(nr);
					}
				}
				reader.Close();
				conn.Close();

				// Prepare list
				list.Sort();

				if(this.CustomerNrDesc)
					list.Reverse();

				this._CustomerNrsList = list;
			}

			// Find next number
			var i = 0;
			int currentNr = this.FirstCustomerNr;

			while(true)
			{
				var skip = (this.CustomerNrDesc && this._LastCustomerNr > 0 && this._LastCustomerNr <= currentNr) || (!this.CustomerNrDesc && this._LastCustomerNr >= currentNr);
				if(!skip && !_CustomerNrsList.Contains(currentNr))
				{
					// Free number found, return value
					this._LastCustomerNr = currentNr;
					this._CustomerNrsList.Add(currentNr);
					return currentNr.ToString();
				}
				else
				{
					// Number not found, increase index
					i++;

					if(this.CustomerNrDesc)
						currentNr--;
					else
						currentNr++;
				}
			}
		}

		/// <summary>
		/// Update customer numbers of orders, which belong to the specified customer
		/// </summary>
		/// <param name="customerRow">Customer row.</param>
		private void _UpdateOrder(DataRow customerRow)
		{
			var orderId = customerRow["OrderID"].ToString();

			if (orderId.EndsWith("_del")) {
				_UpdateOrderByDelivery(customerRow);
				return;
			}

			var orderRows = this._Processor.AllOrders.Select("OrderID = '" + orderId + "'");
			if(orderRows.Length > 0)
			{
				foreach (DataRow orderRow in orderRows) {
					orderRow ["Kontonummer"] = customerRow ["Kontonummer"];

					if (this._Processor.AllOrders.Columns.Contains ("KontoRechnungsadresse")) {
						orderRow ["KontoRechnungsadresse"] = orderRow ["Kontonummer"];
					}
				}
			}
		}

		/// <summary>
		/// Update add delivery-invoice customer relation
		/// </summary>
		/// <param name="customerRow">Customer row.</param>
		private void _UpdateOrderByDelivery(DataRow customerRow)
		{
			var orderId = customerRow ["OrderID"].ToString ().Replace ("_del", "");
			var orderRows = this._Processor.AllOrders.Select("OrderID = '" + orderId + "'");

			// Rechnungsempfänger aus Auftrag übernehmen
			if (customerRow.Table.Columns.Contains ("Rechnungsempfanger")) {
				foreach (DataRow orderRow in orderRows) {
					customerRow ["Rechnungsempfanger"] = orderRow ["Kontonummer"];
				}
			}

			// Konto Lieferadresse bei Auftrag hinzufügen
			if (this._Processor.AllOrders.Columns.Contains ("KontoLieferadresse")) {
				foreach (DataRow orderRow in orderRows) {
					orderRow ["KontoLieferadresse"] = customerRow ["Kontonummer"];
				}
			}
		}


		private void _FillPlaceholders()
		{
			/*
			 * Text module 
			 */
			string placeholderContent = null;

			foreach(DataRow row in this._Processor.AllOrderItems.Rows)
			{
				foreach(DataColumn col in this._Processor.AllOrderItems.Columns)
				{
					var value = row[col].ToString();
					// Textbaustein
					if(this.TextModule1Number != 0 && !String.IsNullOrEmpty(this.TextModule1Placeholder) && 
					   value != null && value.Contains(this.TextModule1Placeholder))
					{
						if(placeholderContent == null)
							placeholderContent = _LoadPlaceholderContent();

						value = value.Replace(this.TextModule1Placeholder, placeholderContent);
						row[col] = value;
					}

					// Load original price
					if(value != null && value.Contains("{originalPrice}") && this._Processor.AllOrderItems.Columns.Contains("Artikelnummer")) {

						// Load customer nr
						object rows = this._Processor.AllOrders.Select("BELEGKEY='" + (string)row["BELEGKEY"] + "'");
						var rows2 = (DataRow[])rows; // Workaround wegen Mono Bug (InvalidCastException)
						if(rows2.Length > 0) {
							var customer = (string)rows2[0]["Kontonummer"];
							var artnr = (string)row["Artikelnummer"];
							var newValue = this._ReadOriginalPrice(artnr, customer).ToString(CultureInfo.GetCultureInfo("en-US"));
							row[col] = value.Replace("{originalPrice}", newValue);
						}
					}
				}
			}

			/*
			 * Order reference number from customer
			 */

			foreach (DataRow row in this._Processor.AllOrders.Rows) {
				foreach (DataColumn col in this._Processor.AllOrders.Columns) {
					if (row [col].ToString ().Contains ("{orderReferenceNr}")) {
						var customerNr = (string)row ["Kontonummer"];
						var reference = this._GetOrderReferenceNr (customerNr);
						row [col] = row [col].ToString ().Replace ("{orderReferenceNr}", reference);
					}
				}
			}
		}

		private string _GetOrderReferenceNr(string customerNr) 
		{
			var query = @"select c012, mesoyear from t057 where c000=@nr and mesocomp=@mesocomp and mesoyear=@mesoyear";

			var conn = new SqlConnection(this._DataConnectionString);
			var cmd = new SqlCommand(query, conn);

			cmd.Parameters.Add ("@nr", SqlDbType.NVarChar, 30).Value = "050-" + customerNr;
			cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
			cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;

			conn.Open();
			var result = cmd.ExecuteScalar();
			conn.Close();

			if(result is String)
				return (string)result;
			else
				return "";
		}

		private string _LoadPlaceholderContent()
		{
			var query = @"SELECT c004 FROM T326
				WHERE c002 = @nr and mesocomp=@mesocomp and mesoyear=@mesoyear";
			
			var conn = new SqlConnection(this._DataConnectionString);
			var cmd = new SqlCommand(query, conn);
			
			cmd.Parameters.Add("@nr", SqlDbType.NVarChar, 30).Value = this.TextModule1Number;
			cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
			cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;
			
			conn.Open();
			var result = cmd.ExecuteScalar();
			conn.Close();

			if(result is String)
				return (string)result;
			else
				return "TEXTBAUSTEIN WURDE NICHT GEFUNDEN";
		}

		private double _ReadOriginalPrice(string artNr, string custnr)
		{
			var query = @"SELECT c013 FROM t043 
				WHERE mesocomp = @mesocomp and mesoyear = @mesoyear
				and c000 = (SELECT c090 FROM t024 where mesocomp = @mesocomp and mesoyear = @mesoyear and c002 = @artnr)
				and c001 = 1 
				and c002 = (SELECT c066 FROM t054 WHERE mesocomp='LOIT' AND mesoyear = @mesoyear AND c112 = @custnr)";
			
			var conn = new SqlConnection(this._DataConnectionString);
			var cmd = new SqlCommand(query, conn);
			
			cmd.Parameters.Add("@artnr", SqlDbType.VarChar, 50).Value = artNr;
			cmd.Parameters.Add("@custnr", SqlDbType.VarChar, 30).Value = custnr;
			cmd.Parameters.Add("@mesocomp", SqlDbType.Char, 4).Value = this.MesoCompany;
			cmd.Parameters.Add("@mesoyear", SqlDbType.Int).Value = this._MesoYear;
			
			conn.Open();
			var result = cmd.ExecuteScalar();
			conn.Close();
			
			if(result is Double)
				return (double)result;
			else
				return 0;
		}

		public override void CheckAfterExport ()
		{
			if (!OrderSettings.HasColumn ("FaktName", OrderSettings.Current.OrdersColumns)) {
				return;
			}

			if (!OrderSettings.HasColumn ("Kontonummer", OrderSettings.Current.OrdersColumns)) {
				return;
			}

			foreach (WriterPluginBase writer in OrderSettings.Current.WriterPlugins) {
				if (writer is MssqlWriterPlugin) {
					var mssqlWriter = (MssqlWriterPlugin)writer;
					if (mssqlWriter.TargetTable == OrderTable.Orders) {
						var query = String.Format (
							            "SELECT Kontonummer FROM {0} GROUP BY Kontonummer HAVING COUNT(DISTINCT FaktName) > 1",
							            mssqlWriter.DbTable);

						var conn = new SqlConnection (mssqlWriter.GetConnectionString ());
						var cmd = new SqlCommand (query, conn);
						conn.Open ();
						var dr = cmd.ExecuteReader ();
						while (dr.Read ()) {
							var soap = new SoapConnector ();
							soap.SendErrorMail (String.Format ("Fehler: Mehrere Kunden auf gleicher Kundennummer!!! ({0})", dr ["Kontonummer"]));
						}
						conn.Close ();
					}
				}
			}
		}
	}
}

