using System;

namespace Gustav_Connector
{
	public partial class StaticReaderSettings : PluginSettingsDialog
	{
		private StaticReaderPlugin _CurrentPlugin;

		public StaticReaderSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (StaticReaderPlugin)value;
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			if (this._CurrentPlugin.Headers.Length > 0) {
				this.txtHeaders.Text = String.Join (",", this._CurrentPlugin.Headers);
			} else {
				var headers = new String[this.SelectColumns.Length];
				for (int i=0; i<headers.Length; i++) {
					headers [i] = this.SelectColumns [i].ColumnName;
				}
				this.txtHeaders.Text = String.Join (",", headers);
			}

			this.txtData.Buffer.Text = this._CurrentPlugin.GetDataAsString ();
			this.nrLastTimestamp.Value = this._CurrentPlugin.LastTimestamp;
		}

		private void Save()
		{
			this._CurrentPlugin.Headers = this.txtHeaders.Text.Split (',');
			this._CurrentPlugin.SetDataFromString (this.txtData.Buffer.Text);
			this._CurrentPlugin.LastTimestamp = (int)this.nrLastTimestamp.Value;
		}
	}
}

