using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Gustav_Connector
{
	public class StaticReaderPlugin : ReaderPluginBase
	{
		public string[] Headers = new String[0];
		public List<string[]> Rows = new List<string[]>();

		public StaticReaderPlugin ()
		{
			this.Name = "Statische Daten";
			this.Description = "Importiert statische Daten";
			this.SettingsDialogType = typeof(StaticReaderSettings);
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("Headers", String.Join(",", this.Headers));

			StringBuilder rows = new StringBuilder ();
			bool first = true;
			foreach (string[] row in this.Rows) {
				if (first) {
					first = false;
				} else {
					rows.AppendLine ();
				}

				rows.Append (
					String.Join (",", row)
				);
			}

			writer.WriteElementString ("Rows", rows.ToString());

			base.WriteXmlSub (writer);
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch(nodeName)
			{
			case "Headers":
				this.Headers = reader.Value.Split (',');
				break;
			case "Rows":
				this.Rows = new List<string[]> ();
				foreach (string row in reader.Value.Split(new String[] { "\n" }, StringSplitOptions.None)) {
					this.Rows.Add (row.Split (','));
				}
				break;
			}

			base.ReadXmlSub (reader, nodeName);
		}

		#endregion

		public override void SaveState (DataTable databaseTable, ImportProcessorBase processor)
		{
			// Find timestamp column
			var colNr = -1;
			for (int i=0; i<this.Headers.Length; i++) {
				if (this.Headers [i] == "timestamp")
					colNr = i;
			}

			// Return if timestamp column in not available
			if (colNr == -1)
				return;

			// Find biggest timestamp value
			foreach (string[] row in this.Rows) 
			{
				int curTimestamp;
				if (Int32.TryParse (row [colNr], out curTimestamp))
					this.LastTimestamp = Math.Max (this.LastTimestamp, curTimestamp);
			}
		}

		public override System.Data.DataTable GetData (ImportProcessorBase processor)
		{
			// Create table
			var datatable = new DataTable ();

			// Add columns
			foreach (string header in this.Headers) {
				datatable.Columns.Add (header);
			}

			// Add rows
			foreach (string[] row in this.Rows) {

				var dataRow = datatable.NewRow ();

				for (int i=0; i<this.Headers.Length; i++) {

					string value = "";

					if (row.Length >= i + 1) {
						value = row [i];
					}

					dataRow [this.Headers [i]] = value;
				}

				// Check timestamp
				bool addRow = true;
				if (datatable.Columns.Contains ("timestamp")) {
					int timestamp;
					if (Int32.TryParse ((string)dataRow ["timestamp"], out timestamp)) {
						if (timestamp <= this.LastTimestamp)
							addRow = false;
					}
				}

				// Add row to table
				if (addRow) {
					datatable.Rows.Add (dataRow);
				}
			}

			return datatable;
		}

		public string GetDataAsString()
		{
			if (this.Rows == null)
				return null;

			var str = new StringBuilder ();

			foreach (string[] row in this.Rows) {

				if (str.Length > 0)
					str.AppendLine ();

				str.Append (String.Join (",", row));
			}

			return str.ToString ();
		}

		public void SetDataFromString(string str)
		{
			this.Rows = new  List<string[]> ();

			string lineSeperator;

			if (str.Contains ("\r\n")) {
				lineSeperator = "\r\n";
			} else if (str.Contains ("\r")) {
				lineSeperator = "\r";
			} else {
				lineSeperator = "\n";
			}

			foreach (string row in str.Split(new String[] { lineSeperator } , StringSplitOptions.None)) {

				this.Rows.Add (
					row.Split (',')
				);
			}
		}

		public override string[] GetCurrentKeys (ImportProcessorBase processor)
		{
			return base.GetCurrentKeys (processor);
		}
	}
}

