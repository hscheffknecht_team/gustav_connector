using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Xml;

namespace Gustav_Connector
{
	public class OledbReaderPlugin : ReaderPluginBase
	{
		public string ConnectionString { get; set; }

		public OledbReaderPlugin ()
		{
			this.Name = "OLEDB Reader";
			this.Description = "Liest Daten aus einer beliebigen OLEDB Quelle";
			this.SettingsDialogType = typeof(OledbReaderSettings);
		}

		#region XmlSerializeable
		
		public override void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch (reader.Name) {

					case "Id":
						reader.Read ();
						this.Id = reader.Value;
						break;

					case "ConnectionString":
						reader.Read ();
						this.ConnectionString = reader.Value;
						break;

					case "QueryData":
						this.QueryData.ReadXml (reader);
						break;

					case "LastTimestamp":
						reader.Read ();
						this.LastTimestamp = Int32.Parse(reader.Value);
						break;

					case "Query":
						reader.Read ();
						// Ignore (backward compatibility)
						break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
			
			reader.ReadEndElement();
		}
		
		public override void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement("ColumnInfo");
			writer.WriteElementString("Id", this.Id);
			writer.WriteElementString("ConnectionString", this.ConnectionString);
			writer.WriteElementString("LastTimestamp", this.LastTimestamp.ToString());

			// QueryData
			writer.WriteStartElement ("QueryData");
			this.QueryData.WriteXml (writer);
			writer.WriteEndElement ();

			writer.WriteEndElement();
		}
		
		#endregion

		public override DataTable GetData (ImportProcessorBase processor)
		{
			// Connect DB and send query
			string query = this.QueryData.ToString().Replace("@timestamp", this.LastTimestamp.ToString());
			var cmd = new OleDbDataAdapter (query, this.ConnectionString);

			var datatable = new DataTable ();
			cmd.Fill (datatable);

			return datatable;
		}

		public override string GetStatusInfo ()
		{
			return this.QueryData.From;
		}

		public override Dictionary<string,string>[] GetDeletedKeys (ImportProcessorBase processor)
		{
			// Get key columns
			var keyColumns = processor.GetKeyColumns ();

			// Connect to DB
			var query = this.QueryData.GetAllKeysQuery (processor);
			var cmd = new OleDbDataAdapter (query, this.ConnectionString);

			// Load data
			var datatable = new DataTable ();
			cmd.Fill (datatable);

			// Add db keys to list (combine more than one key with "§§§")
			var dbKeys = new Dictionary<string,Dictionary<string,string>> ();
			foreach (DataRow row in datatable.Rows) {
				var value = new Dictionary<string,string> ();
				string key = null;
				foreach (var keyColumn in keyColumns) {

					if(key == null) 
						key = row[keyColumn.ColumnName].ToString();
					else
						key += "§§§" + (string)row[keyColumn.ColumnName];

					value.Add (keyColumn.ColumnName, (string)row [keyColumn.ColumnName]);
				}

				if (dbKeys.ContainsKey (key)) {
					dbKeys [key] = value;
				} else {
					dbKeys.Add (key, value);
				}
			}

			// Load keys from cache
			if (this.KeysCache == null) {
				this.KeysCache = new EntityKeysCache ("ole-db-reader-" + this.Id);
			}

			// Find keys to delete
			var deletedKeys = new List<Dictionary<string,string>> ();
			foreach (string key in this.KeysCache.GetAllItems().Keys) {

				var found = dbKeys.ContainsKey (key);

				if (!found) {
					var deleteItem = this.KeysCache.GetItem (key);
					deletedKeys.Add (deleteItem);
				}
			}

			// Set db keys as new cache
			this.KeysCache.SetAllItems(dbKeys);

			// Return array
			return deletedKeys.ToArray ();
		}
	}
}

