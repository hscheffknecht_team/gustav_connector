using System;

namespace Gustav_Connector
{
	public partial class OledbReaderSettings : PluginSettingsDialog
	{
		private OledbReaderPlugin _CurrentPlugin;

		public OledbReaderSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (OledbReaderPlugin)value;
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			this.txtConnectionString.Buffer.Text = this._CurrentPlugin.ConnectionString;
			this.nrLastTimestamp.Value = this._CurrentPlugin.LastTimestamp;
			
			this.dbquery.SelectColumns = this.SelectColumns;
			this.dbquery.WhereColumns = this.WhereColumns;
			this.dbquery.ShowColumns (this._CurrentPlugin.QueryData);
		}

		private void Save()
		{
			this._CurrentPlugin.ConnectionString = this.txtConnectionString.Buffer.Text;
			this._CurrentPlugin.LastTimestamp = (int)this.nrLastTimestamp.Value;

			this._CurrentPlugin.QueryData = this.dbquery.GetQuery ();
		}
	}
}

