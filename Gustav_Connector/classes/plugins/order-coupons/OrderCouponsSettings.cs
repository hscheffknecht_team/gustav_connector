using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
    public partial class OrderCouponsSettings : PluginSettingsDialog
    {
        private OrderCouponsPlugin _CurrentPlugin;
        private string CouponsArtNr { get; set; }

		public List<OrderCouponsRelation> _Values { get; set; }

        public override PluginBase CurrentPlugin {
            get {
                return (PluginBase)this._CurrentPlugin;
            }
            set {
                this._CurrentPlugin = (OrderCouponsPlugin)value;
                Load ();
            }
        }
        
        public OrderCouponsSettings ()
        {
            this.Build();
        }
        
        protected void OnButtonOkClicked (object sender, EventArgs e)
        {
            Save();
            this.Destroy();
        }
        
		public override void Load()
        {
            this.txtCouponsArtNr.Text = this._CurrentPlugin.CouponsArtNr;
			// Values
			foreach(OrderCouponsRelation relattion in this._CurrentPlugin.Relations)
			{
				AddRelation(relattion);
			}
        }

        private int _GetSrcActiveId(string value)
        {
            int i=0;
            foreach(string field in SoapDataCache.GetOrderFields())
            {
                if(field == value)
                    return i;

                i++;
            }

            return -1;
        }

        private int _GetDstActiveId(string value)
        {
            int i=0;
            foreach(ColumnInfo col in OrderSettings.Current.OrderItemsColumns)
            {
                if(col.Name == value)
                    return i;

                i++;
            }

            return -1;
        }
        
        private void Save()
        {
            this._CurrentPlugin.CouponsArtNr = txtCouponsArtNr.Text;

			var values = new List<OrderCouponsRelation> ((int)tblValues.NRows-1);

			while(values.Count < values.Capacity) {
				values.Add(new OrderCouponsRelation());
			}

			foreach(Gtk.Widget w in tblValues.AllChildren)
			{
				
				var child = (Gtk.Table.TableChild)tblValues[w];

				if (child.Child.Name == null) {
					continue;
				}

				var keys = child.Child.Name.Split('_');
				if (keys.Length != 2) {
					continue;
				}

				var key_name = keys[0];
				var row = Int32.Parse(keys[1]);
				var relation = values [row-1];
			
				switch(key_name)
				{
				case "allowZero":
					relation.AllowZero = ((Gtk.CheckButton)child.Child).Active;
					break;

				case "code":
					relation.Code = ((Gtk.Entry)child.Child).Text;
					break;

				case "artNr":
					relation.ArtNr = ((Gtk.Entry)child.Child).Text;
					break;
				}
			}
				
			this._CurrentPlugin.Relations = values;
        }

		protected void OnBtnAddValueClicked (object sender, EventArgs e)
		{
			AddRelation(null);
		}

		protected void AddRelation(OrderCouponsRelation relation=null)
		{
			// Source entry
			var cmbCode = new Gtk.Entry();
			cmbCode.Name = "code_" + tblValues.NRows;

			// Destination entry
			var allowZero = new Gtk.CheckButton();
			allowZero.Name = "allowZero_" + tblValues.NRows;

			// Destination entry
			var cmbArtnr = new Gtk.Entry();
			cmbArtnr.Name = "artNr_" + tblValues.NRows;

			// Delete button
			var deleteButton = new Gtk.Button();
			deleteButton.Label = "Löschen";
			deleteButton.Clicked += OnBtnDeleteRelationsClicked;
			deleteButton.Name = "btnDeleteColumn_" + tblValues.NRows;

			if (relation != null) {
				cmbCode.Text = relation.Code;
				cmbArtnr.Text = relation.ArtNr;
				allowZero.Active = relation.AllowZero;
			}


			// Add to table
			tblValues.NRows++;
			tblValues.Attach (cmbCode, 0, 1, tblValues.NRows-1, tblValues.NRows);
			tblValues.Attach (allowZero, 1, 2, tblValues.NRows-1, tblValues.NRows);
			tblValues.Attach (cmbArtnr, 2, 3, tblValues.NRows-1, tblValues.NRows);
			tblValues.Attach (deleteButton, 3, 4, tblValues.NRows-1, tblValues.NRows);
			tblValues.ShowAll();
		}

		protected void OnBtnDeleteRelationsClicked (object sender, EventArgs e)
		{
			// Get row number
			var buttonChild =  (Gtk.Table.TableChild)tblValues[(Gtk.Widget)sender];
			int rowNr = (int)buttonChild.TopAttach;

			// Move or delete widgets
			foreach(Gtk.Widget w in tblValues.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblValues[w];

				if(child.TopAttach > rowNr)
				{
					child.TopAttach--;
					child.BottomAttach--;
				} 
				else if(child.TopAttach == rowNr)
				{
					tblValues.Remove(child.Child);
				}
			}

			// Remove row
			tblValues.NRows--;
		}

    }
}

