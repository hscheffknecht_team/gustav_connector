using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderCouponsPlugin : OrderPluginBase
	{
        public static int _NextItemId = 0;

        public string CouponsArtNr { get; set; }
		public List<OrderCouponsRelation> Relations { get; set; }


        public OrderCouponsPlugin () : base()
		{
			this.Name = "Gutscheine Plugin";
			this.Description = "Importiert Gutscheine als Artikel";
			this.SettingsDialogType = typeof(OrderCouponsSettings);

			this.Relations = new List<OrderCouponsRelation>();
		}

		public override string GetStatusInfo ()
		{
            return String.Format("Artikel-Nr.: {0}", this.CouponsArtNr);
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
        {
            writer.WriteElementString("CouponsArtNr", this.CouponsArtNr);
			foreach (var relation in this.Relations) {
				relation.WriteXml (writer);
			}

		}
			
		public override void ReadXml (XmlReader reader)
		{
			this.Relations = new List<OrderCouponsRelation> ();
			base.ReadXml (reader);
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch(nodeName)
            {
                case "CouponsArtNr":
                    this.CouponsArtNr = reader.Value;
                    break;
				case "OrderCouponsRelation":
					var relation = new OrderCouponsRelation ();
					relation.ReadXml (reader);
					this.Relations.Add (relation);
					break;
			}
		}

		#endregion

		private OrderCouponsRelation FindRelationForCode(String code)
		{
			var search = code.Trim ().ToLower ();
			if (search.Length > 0) {
				foreach (var relation in this.Relations) {
					if (relation.Code.Trim ().ToLower () == search) {
						return relation;
					}
				}
			}

			return new OrderCouponsRelation() {
				ArtNr = this.CouponsArtNr,
				AllowZero = false,
			};
		}

		public override void ProcessOrderNode (XmlNode input, OrderProcessor processor)
		{
			var numberformat = new System.Globalization.NumberFormatInfo();
			numberformat.NumberDecimalSeparator = ".";
		
			var codeNode = input.SelectSingleNode ("coupon_code");
            var priceNode = input.SelectSingleNode("items/item/price_couponvalue");
			if(priceNode != null && codeNode != null)
            {
				var code = codeNode.InnerText;
				var priceValue = Decimal.Parse(priceNode.InnerText, numberformat);
				var relation = this.FindRelationForCode (code);

				if(relation != null && relation.AcceptPrice(priceValue))
                {
                    var itemNode = input.SelectSingleNode("items/item").Clone();

                    itemNode.SelectSingleNode("id").InnerText = (_NextItemId--).ToString();
					itemNode.SelectSingleNode("product_number").InnerText = relation.ArtNr;
					itemNode.SelectSingleNode("product_name").InnerText = String.Format("Gutscheincode \"{0}\"", code);
                    itemNode.SelectSingleNode("product_qty").InnerText = "1";
					itemNode.SelectSingleNode("price_once").InnerText = itemNode.SelectSingleNode("price_sum").InnerText = (Decimal.Parse(input.SelectSingleNode("price_couponvalue").InnerText, numberformat)*-1).ToString(numberformat); 
					itemNode.SelectSingleNode("price_once_gross").InnerText = itemNode.SelectSingleNode("price_sum_gross").InnerText = (Decimal.Parse(input.SelectSingleNode("price_couponvalue_gross").InnerText, numberformat)*-1).ToString(numberformat);
                    itemNode.SelectSingleNode("price_taxvalue").InnerText = (Decimal.Parse(itemNode.SelectSingleNode("price_once_gross").InnerText, numberformat) - Decimal.Parse(itemNode.SelectSingleNode("price_once").InnerText, numberformat)).ToString();
                    itemNode.SelectSingleNode("price_couponvalue").InnerText = "0";
                    itemNode.SelectSingleNode("price_couponvalue_gross").InnerText = "0";

                    input.SelectSingleNode("items").AppendChild(itemNode);
                }
            }
		}
	}
}