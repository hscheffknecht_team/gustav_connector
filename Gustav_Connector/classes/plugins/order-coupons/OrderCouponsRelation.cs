using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class OrderCouponsRelation : IXmlSerializable
	{
		public string Code { get; set; }
		public string ArtNr { get; set; }
		public bool AllowZero { get; set; }

		public OrderCouponsRelation ()
		{
		}

		public bool AcceptPrice (decimal priceValue)
		{
			if (this.AllowZero) {
				return true;
			}
			return (priceValue != 0);
		}

		public XmlSchema GetSchema ()
		{
			return null;
		}
		
		public void ReadXml (XmlReader reader)
		{
			while(true)
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch(reader.Name)
					{
						case "Code":
							reader.Read();
							this.Code = reader.Value;
							break;

						case "ArtNr":
							reader.Read();
							this.ArtNr = reader.Value;
							break;

						case "AllowZero":
							reader.Read();
							this.AllowZero = Boolean.Parse(reader.Value);
							break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}

				reader.Read();
			}
		}
		
		public void WriteXml (XmlWriter writer) 
		{
			writer.WriteStartElement("OrderCouponsRelation");
			writer.WriteElementString("Code", this.Code);
			writer.WriteElementString("ArtNr", this.ArtNr);
			writer.WriteElementString("AllowZero", this.AllowZero.ToString ());
			writer.WriteEndElement();
		}
	}
}

