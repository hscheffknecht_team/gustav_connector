using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace Gustav_Connector
{
	public class MssqlReaderPlugin : ReaderPluginBase
	{
		public string DbServer { get; set; }
		public string DbUser { get; set; }
		public string DbPassword { get; set; }
		public string Database { get; set; }

		public MssqlReaderPlugin ()
		{
			this.Name = "MS-SQL Reader";
			this.Description = "Liest Daten aus einer Microsoft SQL Datenbank";
			this.SettingsDialogType = typeof(MssqlReaderSettings);
		}

		#region XmlSerializeable

		public override void ReadXmlSub (XmlReader reader, string nodeName)
		{
			switch (nodeName) {

				case "Id":
					this.Id = reader.Value;
					break;

				case "DbServer":
					this.DbServer = reader.Value;
					break;

				case "DbUser":
					this.DbUser = reader.Value;
					break;

				case "DbPassword":
					this.DbPassword = reader.Value;
					break;

				case "Database":
					this.Database = reader.Value;
					break;

				case "QueryData":
					this.QueryData.ReadXml (reader);
					break;

				case "LastTimestamp":
					this.LastTimestamp = Int32.Parse(reader.Value);
					break;

				case "Query":
					// Ignore (backward compatibility)
					break;
			}

			base.ReadXmlSub (reader, nodeName);
		}

		public override void WriteXmlSub (XmlWriter writer)
		{
			writer.WriteElementString("Id", this.Id);
			writer.WriteElementString("DbServer", this.DbServer);
			writer.WriteElementString("DbUser", this.DbUser);
			writer.WriteElementString("DbPassword", this.DbPassword);
			writer.WriteElementString("Database", this.Database);
			writer.WriteElementString("LastTimestamp", this.LastTimestamp.ToString());

			// QueryData
			writer.WriteStartElement ("QueryData");
			this.QueryData.WriteXml (writer);
			writer.WriteEndElement ();

			base.WriteXmlSub (writer);
		}
		
		#endregion

		public override DataTable GetData (ImportProcessorBase processor)
		{
			// Connect DB and send query
			var query = this.QueryData.GetQuery(processor);
			if (ConfigurationManager.AppSettings ["UseForceOrder"] != null && Boolean.Parse(ConfigurationManager.AppSettings ["UseForceOrder"])) {
				query += " OPTION (FORCE ORDER)";
			}
			var cmd = new SqlDataAdapter (query, _GetConnectionString());

			cmd.SelectCommand.Parameters.Add ("@timestamp", SqlDbType.Int).Value = this.LastTimestamp;
			cmd.SelectCommand.CommandTimeout = 300; // 5 Minuten

			var datatable = new DataTable ();
			cmd.Fill (datatable);

			return datatable;
		}

		public override string GetStatusInfo ()
		{
			return this.QueryData.From;
		}

		private string _GetConnectionString() {

			// Build connection string
			var builder = new SqlConnectionStringBuilder();
			builder.DataSource = this.DbServer;
			builder.InitialCatalog = this.Database;

			if (!String.IsNullOrEmpty(this.DbUser) || !String.IsNullOrEmpty(this.DbPassword)) {
				builder.UserID = this.DbUser;
				builder.Password = this.DbPassword;
			} else {
				builder.IntegratedSecurity = true;
			}

			return builder.ConnectionString;
		}

		public override Dictionary<string,string>[] GetDeletedKeys (ImportProcessorBase processor)
		{
			// Get key columns
			var keyColumns = processor.GetKeyColumns ();
			if (keyColumns.Length == 0)
				return new Dictionary<string, string>[0];

			// Connect to DB
			var query = this.QueryData.GetAllKeysQuery (processor);
			if (ConfigurationManager.AppSettings ["UseForceOrder"] != null && Boolean.Parse(ConfigurationManager.AppSettings ["UseForceOrder"])) {
				query += " OPTION (FORCE ORDER)";
			}
			var cmd = new SqlDataAdapter (query, _GetConnectionString());
			cmd.SelectCommand.CommandTimeout = 300;

			// Load data
			var datatable = new DataTable ();
			cmd.Fill (datatable);

			foreach (CommonPluginBase plugin in processor.Settings.Plugins) {
				plugin.PrepeareDeleteCache (datatable, processor);
			}

			// Add db keys to list (combine more than one key with "§§§")
			var dbKeys = new Dictionary<string,Dictionary<string,string>> ();
			foreach (DataRow row in datatable.Rows) {
				var value = new Dictionary<string,string> ();
				string key = null;
				foreach (var keyColumn in keyColumns) {

					if(key == null) 
						key = row[keyColumn.ColumnName].ToString();
					else
						key += "§§§" + row[keyColumn.ColumnName].ToString();

					value.Add (keyColumn.ColumnName, row[keyColumn.ColumnName].ToString());
				}

				if (!dbKeys.ContainsKey (key)) {
					dbKeys.Add (key, value);
				} else {
					dbKeys [key] = value;
				}
			}

			// Load keys from cache
			this.KeysCache = new EntityKeysCache ("ole-db-reader-" + this.Id);

			// Find keys to delete
			var deletedKeys = new List<Dictionary<string,string>> ();
			foreach (string key in this.KeysCache.GetAllItems().Keys) {

				var found = dbKeys.ContainsKey (key);

				if (!found) {
					var deleteItem = this.KeysCache.GetItem (key);
					deletedKeys.Add (deleteItem);
				}
			}

			// Set db keys as new cache
			this.KeysCache.SetAllItems(dbKeys);

			// Avoid deleting (almost) all data when DB server returns no rows by mistake
			if (Context.Current.AutoMode && deletedKeys.Count > 10 && deletedKeys.Count > datatable.Rows.Count / 2) {
				string msg = String.Format ("Mehr als 50% der Daten sollten gelöscht werden!\r\n\r\nAbfrage:\r\n{0}", query);
				datatable.TableName = processor.GetType ().ToString ();
				datatable.WriteXml ("Error_datatable.xml");
				throw new Exception (msg);
			}

			// Return array
			return deletedKeys.ToArray ();
		}

		public override string[] GetCurrentKeys(ImportProcessorBase processor)
		{
			// Get key columns
			var keyColumns = processor.GetKeyColumns ();
			if (keyColumns.Length == 0)
				return new String[0];
			
			// Connect to DB
			var query = this.QueryData.GetAllKeysQuery (processor);
			if (ConfigurationManager.AppSettings ["UseForceOrder"] != null && Boolean.Parse(ConfigurationManager.AppSettings ["UseForceOrder"])) {
				query += " OPTION (FORCE ORDER)";
			}
			var cmd = new SqlDataAdapter (query, _GetConnectionString());
			cmd.SelectCommand.CommandTimeout = 300;

			// Load data
			var datatable = new DataTable ();
			cmd.Fill (datatable);

			foreach (CommonPluginBase plugin in processor.Settings.Plugins) {
				plugin.PrepeareDeleteCache (datatable, processor);
			}

			// Add db keys to list (combine more than one key with "§§§")
			var dbKeys = new Dictionary<string,Dictionary<string,string>> ();
			foreach (DataRow row in datatable.Rows) {
				var value = new Dictionary<string,string> ();
				string key = null;
				foreach (var keyColumn in keyColumns) {

					if(key == null) 
						key = row[keyColumn.ColumnName].ToString();
					else
						key += "§§§" + row[keyColumn.ColumnName].ToString();

					value.Add (keyColumn.ColumnName, row[keyColumn.ColumnName].ToString());
				}

				if (!dbKeys.ContainsKey (key)) {
					dbKeys.Add (key, value);
				} else {
					dbKeys [key] = value;
				}
			}

			// Return keys
			var result = new String[dbKeys.Keys.Count];
			dbKeys.Keys.CopyTo (result, 0);
			return result;
		}
	}
}

