using System;

namespace Gustav_Connector
{
	public partial class MssqlReaderSettings : PluginSettingsDialog
	{
		private MssqlReaderPlugin _CurrentPlugin;

		public MssqlReaderSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (MssqlReaderPlugin)value;
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			this.txtDbServer.Text = this._CurrentPlugin.DbServer;
			this.txtDbUser.Text = this._CurrentPlugin.DbUser;
			this.txtDbPassword.Text = this._CurrentPlugin.DbPassword;
			this.txtDatabase.Text = this._CurrentPlugin.Database;
			this.nrLastTimestamp.Value = this._CurrentPlugin.LastTimestamp;

			this.dbquery.SelectColumns = this.SelectColumns;
			this.dbquery.WhereColumns = this.WhereColumns;
			this.dbquery.ShowColumns (this._CurrentPlugin.QueryData);
		}

		private void Save()
		{
			this._CurrentPlugin.DbServer = this.txtDbServer.Text;
			this._CurrentPlugin.DbUser = this.txtDbUser.Text;
			this._CurrentPlugin.DbPassword = this.txtDbPassword.Text;
			this._CurrentPlugin.Database = this.txtDatabase.Text;
			this._CurrentPlugin.LastTimestamp = (int)this.nrLastTimestamp.Value;

			this._CurrentPlugin.QueryData = this.dbquery.GetQuery ();
		}
	}
}

