﻿using System;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class SplitFieldSettings : PluginSettingsWidget
	{
		public SplitFieldSettings ()
		{
			this.Build ();
		}

		protected SplitFieldPlugin GetPlugin()
		{
			return (SplitFieldPlugin)this.CurrentPlugin;
		}

		public override void Load ()
		{
			var plugin = GetPlugin ();
			FieldName.Text = plugin.FieldName;
			Separator.Text = plugin.Separator;
		}

		public override void Save ()
		{
			var plugin = GetPlugin ();
			plugin.FieldName = FieldName.Text;
			plugin.Separator = Separator.Text;
		}
	}
}

