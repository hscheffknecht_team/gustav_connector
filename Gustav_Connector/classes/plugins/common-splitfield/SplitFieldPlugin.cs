﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class SplitFieldPlugin : CommonPluginBase
	{
		public string FieldName { get; set; }
		public string Separator { get; set; }

		public SplitFieldPlugin ()
		{
			this.Name = "Split field";
			this.Description = "Trennt mehrere Werte in einem Feld in mehrere Datensätze. Die anderen Felder werden dabei dupliziert.";
			this.SettingsWidgetType = typeof(SplitFieldSettings);
			this._CreatesOrDeletesRows = true;
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("FieldName", this.FieldName);
			writer.WriteElementString ("Separator", this.Separator);
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch (nodeName) {
				case "FieldName":
					this.FieldName = reader.Value;
					break;
				case "Separator":
					this.Separator = reader.Value;
					break;
			}
		}

		#endregion

		public override void ProcessTable (DataTable table, ImportProcessorBase processor)
		{
			SplitDatatable (table);
		}


		public override void PrepeareDeleteCache (DataTable table, ImportProcessorBase processor)
		{
			SplitDatatable (table);
		}

		protected void SplitDatatable(DataTable table)
		{
			if (String.IsNullOrEmpty (FieldName) || String.IsNullOrEmpty (Separator)) {
				return;
			}

			if (!table.Columns.Contains (FieldName)) {
				return;
			}

			var newRows = new List<DataRow> ();

			// Split values and create new rows
			foreach (DataRow row in table.Rows) {
				var value = row [FieldName].ToString ();
				var values = value.Split (new String[] { Separator }, StringSplitOptions.None);
				if (values.Length > 1) {
					for (int i = 0; i < values.Length; i++) {
						if (i == 0) {
							row [FieldName] = values [0];
						} else {
							var newRow = table.NewRow ();
							newRow.ItemArray = row.ItemArray;
							newRow [FieldName] = values [i];
							newRows.Add (newRow);
						}
					}
				}
			}

			// Add new rows
			foreach (DataRow row in newRows) {
				table.Rows.Add (row);
			}
		}
	}
}
