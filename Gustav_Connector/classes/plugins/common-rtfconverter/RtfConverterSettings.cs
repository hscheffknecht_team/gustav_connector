using System;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class RtfConverterSettings : PluginSettingsWidget
	{
		public RtfConverterSettings ()
		{
			this.Build ();
		}

		public override void Load ()
		{
			this.txtColumn.Text = ((RtfConverterPlugin)this.CurrentPlugin).RtfColumn;
			this.baseFontSize.Value = ((RtfConverterPlugin)this.CurrentPlugin).BaseFontSize;
			this.htmlCheck.Active = ((RtfConverterPlugin)this.CurrentPlugin).Html;
		}

		public override void Save ()
		{
			((RtfConverterPlugin)this.CurrentPlugin).RtfColumn = txtColumn.Text;
			((RtfConverterPlugin)this.CurrentPlugin).BaseFontSize = (int)this.baseFontSize.Value;
			((RtfConverterPlugin)this.CurrentPlugin).Html = htmlCheck.Active;
		}
	}
}

