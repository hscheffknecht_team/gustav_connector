using System;
using System.Data;
using System.Xml;

namespace Gustav_Connector
{
	public class RtfConverterPlugin : CommonPluginBase
	{
		public string RtfColumn { get; set; }
		public int BaseFontSize { get; set; }
		public bool Html { get; set; }

		public RtfConverterPlugin ()
		{
			this.Name = "RTF Umwandler";
			this.Description = "Wandelt RTF Daten in Text/HTML um";
			this.SettingsWidgetType = typeof(RtfConverterSettings);
			this.Html = true;
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("RtfColumn", this.RtfColumn);
			writer.WriteElementString ("BaseFontSize", this.BaseFontSize.ToString ());
			writer.WriteElementString ("Html", this.Html.ToString ());
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch (nodeName) {
				case "RtfColumn":
					this.RtfColumn = reader.Value;
					break;
				case "BaseFontSize":
					this.BaseFontSize = Int32.Parse (reader.Value);
					break;
				case "Html":
					this.Html = Boolean.Parse (reader.Value);
					break;
			}
		}

		#endregion

		public override void ProcessTable (DataTable table, ImportProcessorBase processor)
		{
			if (!String.IsNullOrEmpty (this.RtfColumn) && table.Columns.Contains (this.RtfColumn)) {
				RtfHelper.BaseFontSize = BaseFontSize;
				foreach (DataRow row in table.Rows) {
					if (row [this.RtfColumn] != DBNull.Value) {
						var value = (string)row [this.RtfColumn];
						if (value.Length > 5 && value.Substring (0, 5) == @"{\rtf") {
							if (Html)
								row [this.RtfColumn] = RtfHelper.ToHtml ((string)row [this.RtfColumn], false);
							else
								row [this.RtfColumn] = RtfHelper.ToText ((string)row [this.RtfColumn]);
						}
					}
				}
			}
		}
	}
}

