using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public partial class OrderValueFinderSettings : PluginSettingsDialog
	{
		private OrderValueFinderPlugin _CurrentPlugin;
		
		public Dictionary<string,string> _Values { get; set; }
		
		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (OrderValueFinderPlugin)value;
				Load ();
			}
		}

		public OrderValueFinderSettings ()
		{
			this.Build ();

			// Load columns
			this.LoadColumns(null, null);
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			// Target table
			this.cmbTargetTable.Active = (int)this._CurrentPlugin.TargetTable;

			// Columns
			this.LoadColumns(this._CurrentPlugin.SourceColumn, this._CurrentPlugin.TargetColumn);
			
			// Values
			foreach(string key in this._CurrentPlugin.Values.Keys)
			{
				AddValue(key, this._CurrentPlugin.Values[key]);
			}
		}

		private void Save()
		{
			// Target table
			this._CurrentPlugin.TargetTable = (OrderTable)this.cmbTargetTable.Active;

			// Columns
			this._CurrentPlugin.SourceColumn = this.cmbSourceColumn.ActiveText;
			this._CurrentPlugin.TargetColumn = this.cmbTargetColumn.ActiveText;
			
			// Values
			var dict_keys = new Dictionary<int, string>();
			var dict_values = new Dictionary<int, string>();
			
			foreach(Gtk.Widget w in tblValues.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblValues[w];
				var keys = child.Child.Name.Split('_');
				var key_name = keys[0];
				
				if(child.Child is Gtk.Entry)
				{
					var row = Int32.Parse(keys[1]);
					var value = ((Gtk.Entry)child.Child).Text;
					
					switch(key_name)
					{
					case "source":
						dict_keys.Add(row, value);
						break;
						
					case "dest":
						dict_values.Add(row, value);
						break;
					}
				}
			}
			
			var values = new Dictionary<string, string>((int)tblValues.NRows-1);
			for(int i=1; i<tblValues.NRows+100; i++)
			{
				if (dict_keys.ContainsKey (i)) {
					var key = dict_keys [i];
					var value = dict_values [i];

					if (values.ContainsKey (key))
						values [key] = value;
					else
						values.Add (key, value);
				}
			}
			
			this._CurrentPlugin.Values = values;
		}

		private void LoadColumns(string sourceValue, string targetValue)
		{
			var columns = new List<string>();
			List<ColumnInfo> settColumns = null;
			
			// Load correct columnslist
			switch(cmbTargetTable.Active)
			{
			case (int)OrderTable.Orders:
				settColumns = OrderSettings.Current.OrdersColumns;
				break;
			case (int)OrderTable.OrderItems:
				settColumns = OrderSettings.Current.OrderItemsColumns;
				break;
			case (int)OrderTable.Customers:
				settColumns = OrderSettings.Current.CustomersColumns;
				break;
			}
			
			foreach(ColumnInfo column in settColumns)
			{
				columns.Add(column.Name);
			}


			// Empty combo boxes
			((Gtk.ListStore)cmbSourceColumn.Model).Clear();
			((Gtk.ListStore)cmbTargetColumn.Model).Clear();

			// Fill combo boxes
			foreach(var column in columns) 
			{
				cmbSourceColumn.AppendText(column);
				cmbTargetColumn.AppendText(column);
			}

			// Fill values
			if(sourceValue != null || targetValue != null) 
			{
				var i=0;
				foreach(var column in columns)
				{
					if(column == sourceValue)
						cmbSourceColumn.Active = i;

					if(column == targetValue)
						cmbTargetColumn.Active = i;

					i++;
				}
			}
		}

		protected void OnBtnAddValueClicked (object sender, EventArgs e)
		{
			AddValue(null, null);
		}

		protected void AddValue(string src, string dst)
		{
			// Source entry
			var cmbSource = new Gtk.Entry(src);
			cmbSource.Name = "source_" + tblValues.NRows;
						
			// Destination entry
			var cmbDest = new Gtk.Entry(dst);
			cmbDest.Name = "dest_" + tblValues.NRows;
						
			// Delete button
			var deleteButton = new Gtk.Button();
			deleteButton.Label = "Löschen";
			deleteButton.Clicked += OnBtnDeleteRelationsClicked;
			deleteButton.Name = "btnDeleteColumn_" + tblValues.NRows;
			
			// Add to table
			tblValues.NRows++;
			tblValues.Attach (cmbSource, 0, 1, tblValues.NRows-1, tblValues.NRows);
			tblValues.Attach (cmbDest, 1, 2, tblValues.NRows-1, tblValues.NRows);
			tblValues.Attach (deleteButton, 2, 3, tblValues.NRows-1, tblValues.NRows);
			tblValues.ShowAll();
		}

		protected void OnBtnDeleteRelationsClicked (object sender, EventArgs e)
		{
			// Get row number
			var buttonChild =  (Gtk.Table.TableChild)tblValues[(Gtk.Widget)sender];
			int rowNr = (int)buttonChild.TopAttach;
			
			// Move or delete widgets
			foreach(Gtk.Widget w in tblValues.AllChildren)
			{
				var child = (Gtk.Table.TableChild)tblValues[w];
				
				if(child.TopAttach > rowNr)
				{
					child.TopAttach--;
					child.BottomAttach--;
				} 
				else if(child.TopAttach == rowNr)
				{
					tblValues.Remove(child.Child);
				}
			}
			
			// Remove row
			tblValues.NRows--;
		}

		protected void OnCmbTargetTableChanged (object sender, EventArgs e)
		{
			LoadColumns(null, null);
		}
	}
}

