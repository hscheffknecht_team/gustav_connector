using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderValueFinderPlugin : OrderPluginBase
	{
		public OrderTable TargetTable { get; set; }
		public string SourceColumn { get; set; }
		public string TargetColumn { get; set; }
		public Dictionary<string,string> Values { get; set; }

		public OrderValueFinderPlugin ()
		{
			this.Name = "Value Finder";
			this.Description = "Findet Werte auf Basis einer Spalte und schreibt diese auf Basis einer Wertetabelle in eine Zielspalte.";
			this.SettingsDialogType = typeof(OrderValueFinderSettings);

			this.Values = new Dictionary<string, string>();
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("TargetTable", this.TargetTable.ToString());
			writer.WriteElementString("SourceColumn", this.SourceColumn);
			writer.WriteElementString("TargetColumn", this.TargetColumn);

			foreach(var sourceValue in this.Values.Keys)
			{
				writer.WriteStartElement("ValueInfo");
				writer.WriteElementString("SourceValue", sourceValue);
				writer.WriteElementString("TargetValue", this.Values[sourceValue]);
				writer.WriteEndElement();
			}
		}

		public override void ReadXml (XmlReader reader)
		{
			this.Values = new Dictionary<string, string>();
			base.ReadXml (reader);
		}
		
		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch(nodeName)
			{
			case "TargetTable":
				this.TargetTable = (OrderTable)Enum.Parse(typeof(OrderTable), reader.Value);
				break;
			case "SourceColumn":
				this.SourceColumn = reader.Value;
				break;
			case "TargetColumn":
				this.TargetColumn = reader.Value;
				break;
			case "ValueInfo":
				this._ReadValuesXml(reader);
				break;
			}
		}

		private void _ReadValuesXml(XmlReader reader)
		{
			string sourceValue = null;

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element)
				{
					switch(reader.Name)
					{
					case "SourceValue":
						if(!reader.IsEmptyElement)
							reader.Read();
						sourceValue = reader.Value;
						break;
						
					case "TargetValue":
						if(!reader.IsEmptyElement)
							reader.Read();
						this.Values.Add(sourceValue, reader.Value);
						break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
		}

		#endregion

		public override void ProcessEnd (OrderProcessor processor)
		{
			// Find table
			DataTable table = null;
			switch(this.TargetTable)
			{
			case OrderTable.Orders:
				table = processor.AllOrders;
				break;
			case OrderTable.OrderItems:
				table = processor.AllOrderItems;
				break;
			case OrderTable.Customers:
				table = processor.AllCustomers;
				break;
			}

			foreach(DataRow row in table.Rows)
			{

				// Check filter
				var xmlNode = SoapDataCache.FinalOrders.SelectSingleNode ("order[id=" + row ["gustav_id"].ToString () + "]");
				if (!this.ProcessFilter (xmlNode))
					continue;

				// Copy values
				var columns = row.Table.Columns;
				if(columns.Contains(this.SourceColumn) && columns.Contains(this.TargetColumn))
				{
					var oldValue = row[this.SourceColumn].ToString();
					var newValue = row[this.TargetColumn].ToString();

					if(this.Values.ContainsKey(oldValue))
						newValue = this.Values[oldValue];

					row[this.TargetColumn] = newValue;
				}
			}
		}
	}
}

