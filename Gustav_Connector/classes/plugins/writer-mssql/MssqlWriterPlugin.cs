using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace Gustav_Connector
{
	public class MssqlWriterPlugin : WriterPluginBase
	{
		public OrderTable TargetTable { get; set; }
		public string DbServer { get; set; }
		public string DbUser { get; set; }
		public string DbPassword { get; set; }
		public string Database { get; set; }
		public string DbTable { get; set; }

		public MssqlWriterPlugin ()
		{
			this.Name = "MS-SQL Writer";
			this.Description = "Schreibt die Daten in eine Microsoft SQL Datenbank";
			this.SettingsDialogType = typeof(MssqlWriterSettings);
		}

		#region XmlSerializeable
		
		public override void ReadXml (XmlReader reader)
		{
			reader.ReadStartElement();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					switch(reader.Name)
					{						
					case "TargetTable":
						reader.Read();
						this.TargetTable = (OrderTable)Enum.Parse(typeof(OrderTable), reader.Value);
						break;
						
					case "DbServer":
						reader.Read();
						this.DbServer = reader.Value;
						break;
						
					case "DbUser":
						reader.Read();
						this.DbUser = reader.Value;
						break;
						
					case "DbPassword":
						reader.Read();
						this.DbPassword = reader.Value;
						break;
						
					case "Database":
						reader.Read();
						this.Database = reader.Value;
						break;
						
					case "DbTable":
						reader.Read();
						this.DbTable = reader.Value;
						break;
					}
					reader.Read();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
			
			reader.ReadEndElement();
		}
		
		public override void WriteXml (XmlWriter writer)
		{
			writer.WriteStartElement("ColumnInfo");
			writer.WriteElementString("TargetTable", this.TargetTable.ToString());
			writer.WriteElementString("DbServer", this.DbServer);
			writer.WriteElementString("DbUser", this.DbUser);
			writer.WriteElementString("DbPassword", this.DbPassword);
			writer.WriteElementString("Database", this.Database);
			writer.WriteElementString("DbTable", this.DbTable);
			writer.WriteEndElement();
		}
		
		#endregion
	
		public override string GetStatusInfo ()
		{
			return String.Format("Zieltabelle: {0}", this.TargetTable);
		}

		public override void Process (OrderProcessor processor)
		{
			// Get data
			DataTable data = null;
			List<ColumnInfo> columns = null;

			switch(this.TargetTable)
			{
			case OrderTable.Orders:
				data = OrdersDataCache.Orders;
				columns = OrderSettings.Current.OrdersColumns;
				break;
			case OrderTable.OrderItems:
				data = OrdersDataCache.OrderItems;
				columns = OrderSettings.Current.OrderItemsColumns;
				break;
			case OrderTable.Customers:
				data = OrdersDataCache.Customers;
				columns = OrderSettings.Current.CustomersColumns;
				break;
			}

			// Return, if no data are available
			if(data.Rows.Count == 0)
				return;

			// Build query
			var query = new StringBuilder();
			query.AppendFormat("SET ANSI_WARNINGS  OFF SET DATEFORMAT ymd INSERT INTO [{0}] (", this.DbTable);

			// Add columns to query
			var first = true;
			foreach(var column in columns)
			{
				// Don't write internal columns
				if(column.IsInternal)
					continue;

				if(first == true)
					first = false;
				else
					query.Append(",");

				query.AppendFormat("[{0}]", column.Name);
			}

			query.Append(") VALUES ");

			// Iterate rows
			first = true;
			foreach(DataRow row in data.Rows)
			{
				if(first)
					first = false;
				else
					query.Append(",");

				query.Append("(");

				// Iterate columns
				var firstCol = true;
				foreach(var column in columns)
				{
					// Don't write internal columns
					if(column.IsInternal)
						continue;

					if(firstCol)
						firstCol = false;
					else
						query.Append(",");

					query.AppendFormat("'{0}'", row[column.Name].ToString().Replace("'", "''"));
				}

				query.Append(")");
			}

			query.Append (" SET ANSI_WARNINGS  ON");

			// Build connection string

			// Connect DB and send query
			var conn = new SqlConnection(this.GetConnectionString());
			var cmd = new SqlCommand(query.ToString(), conn);

			conn.Open();
			cmd.CommandTimeout = 300; // 5 Minuten
			cmd.ExecuteNonQuery();
			conn.Close();
		}

		public string GetConnectionString() {
			var builder = new SqlConnectionStringBuilder();
			builder.DataSource = this.DbServer;
			builder.InitialCatalog = this.Database;
			builder.UserID = this.DbUser;
			builder.Password = this.DbPassword;
			return builder.ConnectionString;
		}
	}
}

