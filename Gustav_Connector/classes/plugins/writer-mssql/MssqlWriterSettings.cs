using System;

namespace Gustav_Connector
{
	public partial class MssqlWriterSettings : PluginSettingsDialog
	{
		private MssqlWriterPlugin _CurrentPlugin;

		public MssqlWriterSettings ()
		{
			this.Build ();
		}

		public override PluginBase CurrentPlugin {
			get {
				return (PluginBase)this._CurrentPlugin;
			}
			set {
				this._CurrentPlugin = (MssqlWriterPlugin)value;
				Load ();
			}
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			Save();
			this.Destroy();
		}

		public override void Load()
		{
			this.cmbTargetTable.Active = (int)this._CurrentPlugin.TargetTable;
			this.txtDbServer.Text = this._CurrentPlugin.DbServer;
			this.txtDbUser.Text = this._CurrentPlugin.DbUser;
			this.txtDbPassword.Text = this._CurrentPlugin.DbPassword;
			this.txtDatabase.Text = this._CurrentPlugin.Database;
			this.txtDbTable.Text = this._CurrentPlugin.DbTable;
		}

		private void Save()
		{
			this._CurrentPlugin.TargetTable = (OrderTable)this.cmbTargetTable.Active;
			this._CurrentPlugin.DbServer = this.txtDbServer.Text;
			this._CurrentPlugin.DbUser = this.txtDbUser.Text;
			this._CurrentPlugin.DbPassword = this.txtDbPassword.Text;
			this._CurrentPlugin.Database = this.txtDatabase.Text;
			this._CurrentPlugin.DbTable = this.txtDbTable.Text;
		}
	}
}

