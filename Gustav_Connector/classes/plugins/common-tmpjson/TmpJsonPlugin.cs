﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class TmpJsonPlugin : CommonPluginBase
	{
		public string TemporaryTableName { get; set; }
		public string TemporaryKeyColumn { get; set; }
		public string TargetKeyColumn { get; set; }
		public string TargetColumnName { get; set; }

		public TmpJsonPlugin ()
		{
			this.Name = "Temp-2-Json";
			this.Description = "Lädt die Daten aus einer tmp. Tabelle und speichert die Daten als JSON in der gewählten Spalte";
			this.SettingsWidgetType = typeof(TmpJsonSettings);
		}

		#region IXmlSerializable

		public override void WriteXmlSub (System.Xml.XmlWriter writer)
		{
			writer.WriteElementString("TemporaryTableName", this.TemporaryTableName);
			writer.WriteElementString ("TemporaryKeyColumn", this.TemporaryKeyColumn);
			writer.WriteElementString("TargetKeyColumn", this.TargetKeyColumn);
			writer.WriteElementString ("TargetColumnName", this.TargetColumnName);
		}

		public override void ReadXmlSub (System.Xml.XmlReader reader, string nodeName)
		{
			switch (nodeName) {
				case "TemporaryTableName":
					this.TemporaryTableName = reader.Value;
					break;
				case "TemporaryKeyColumn":
					this.TemporaryKeyColumn = reader.Value;
					break;
				case "TargetKeyColumn":
					this.TargetKeyColumn = reader.Value;
					break;
				case "TargetColumnName":
					this.TargetColumnName = reader.Value;
					break;
			}
		}

		#endregion

		public override void ProcessTable (DataTable table, ImportProcessorBase processor)
		{
			if (String.IsNullOrEmpty (this.TargetColumnName)) {
				return;
			}

			if (String.IsNullOrEmpty (this.TemporaryTableName)) {
				return;
			}

			var tmpTable = processor.TemporaryTables.Tables [this.TemporaryTableName];
			if (tmpTable == null) {
				return;
			}

			if (!tmpTable.Columns.Contains (this.TemporaryKeyColumn)) {
				return;
			}

			if (!table.Columns.Contains (this.TargetKeyColumn)) {
				return;
			}

			if (!table.Columns.Contains (this.TargetColumnName)) {
				table.Columns.Add (this.TargetColumnName);
			}

			// Each article contains multiple columns, each columns can contain multiple values
			var data = new Dictionary<string, Dictionary<string, List<string>>> ();

			// Load data from tmp. table
			foreach (DataRow row in processor.TemporaryTables.Tables[this.TemporaryTableName].Rows) {
				string key = row [this.TemporaryKeyColumn].ToString();
				if (!data.ContainsKey (key)) {
					data.Add (key, new Dictionary<string, List<string>> ());
				}

				var articleData = data [key];

				foreach (DataColumn col in tmpTable.Columns) {
					var cellData = row [col].ToString ();
					if (!String.IsNullOrEmpty(cellData) && col.ColumnName != this.TemporaryKeyColumn && col.ColumnName != "timestamp") {
						if (!articleData.ContainsKey (col.ColumnName)) {
							articleData.Add (col.ColumnName, new List<string> ());
						}
						var columnData = articleData [col.ColumnName];
						if (!columnData.Contains (cellData)) {
							columnData.Add (cellData);
						}
					}
				}
			}

			// Convert data to json and write them to Gustav table
			foreach (DataRow row in table.Rows) {
				var keyValue = row [this.TargetKeyColumn].ToString ();
				if (data.ContainsKey (keyValue)) {
					var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer ();
					var jsonData = jsonSerializer.Serialize (data [keyValue]);
					row [this.TargetColumnName] = jsonData;
				}
			}
		}
	}
}

