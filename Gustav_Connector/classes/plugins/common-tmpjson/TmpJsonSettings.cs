﻿using System;

namespace Gustav_Connector
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class TmpJsonSettings : PluginSettingsWidget
	{
		public TmpJsonSettings ()
		{
			this.Build ();
		}

		public override void Load ()
		{
			FillColumnsDropdown (this.cmbTargetColumn, ((TmpJsonPlugin)this.CurrentPlugin).TargetColumnName);
			FillColumnsDropdown (this.cmbGustavKeyColumn, ((TmpJsonPlugin)this.CurrentPlugin).TargetKeyColumn);
			this.txtTmpTableName.Text = ((TmpJsonPlugin)this.CurrentPlugin).TemporaryTableName;
			this.txtTmpKeyColumn.Text = ((TmpJsonPlugin)this.CurrentPlugin).TemporaryKeyColumn;
		}

		public override void Save ()
		{
			var columns = this.CurrentProcessor.GetSelectColumns ();
			((TmpJsonPlugin)this.CurrentPlugin).TargetColumnName = columns [this.cmbTargetColumn.Active].ColumnName;
			((TmpJsonPlugin)this.CurrentPlugin).TargetKeyColumn = columns [this.cmbGustavKeyColumn.Active].ColumnName;
			((TmpJsonPlugin)this.CurrentPlugin).TemporaryTableName = this.txtTmpTableName.Text;
			((TmpJsonPlugin)this.CurrentPlugin).TemporaryKeyColumn = this.txtTmpKeyColumn.Text;
		}

		private void FillColumnsDropdown(Gtk.ComboBox combobox, string currentValue)
		{
			var columns = this.CurrentProcessor.GetSelectColumns ();

			foreach (var col in columns) {

				combobox.AppendText (col.Title);

				if (currentValue == col.ColumnName) {
					combobox.Active = combobox.Model.IterNChildren() - 1;
				}
			}

			combobox.ShowAll ();
		}
	}
}

