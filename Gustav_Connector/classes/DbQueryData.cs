using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Gustav_Connector
{
	public class DbQueryData : IXmlSerializable
	{
		public Dictionary<string,string> SelectColumns { get; set; }
		public string From { get; set; }
		public Dictionary<string, string> WhereColumns { get; set; }
		public bool SkipBinaryColumns;

		public DbQueryData ()
		{
			this.SelectColumns = new Dictionary<string, string> ();
			this.WhereColumns = new Dictionary<string, string> ();
		}

		#region IXmlSerialization

		public XmlSchema GetSchema () 
		{
			return null;
		}

		public void ReadXml (XmlReader reader) 
		{
			this.SelectColumns.Clear ();
			this.WhereColumns.Clear ();

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && !reader.IsEmptyElement)
				{
					var keys = reader.Name.Split ("_".ToCharArray (), 2);
					switch (keys [0]) {

					case "select":
						reader.Read ();
						this.SelectColumns.Add (keys [1], reader.Value);
						break;

					case "from":
						reader.Read ();
						this.From = reader.Value;
						break;

					case "where":
						reader.Read ();
						this.WhereColumns.Add (keys [1], reader.Value);
						break;
					}

					reader.Read ();
				}
				else if(reader.NodeType == XmlNodeType.EndElement)
				{
					break;
				}
			}
		}

		public void WriteXml (XmlWriter writer) 
		{
			// Select
			foreach (string key in this.SelectColumns.Keys) {
				writer.WriteElementString ("select_" + key, this.SelectColumns [key]);
			}

			// From
			writer.WriteElementString ("from", this.From);

			// where
			foreach (string key in this.WhereColumns.Keys) {
				writer.WriteElementString ("where_" + key, this.WhereColumns [key]);
			}
		}

		#endregion

		public string GetAllKeysQuery(ImportProcessorBase processor)
		{
			var query = new StringBuilder ("SELECT ");

			var firstColumn = true;
			foreach (var col in processor.GetKeyColumns()) {

				if (SkipBinaryColumns && col.DataType == typeof(byte[]))
					continue;

				if (firstColumn)
					firstColumn = false;
				else
					query.Append (", ");

				query.AppendFormat ("{0} as [{1}]", this.SelectColumns[col.ColumnName], col.ColumnName);
			}

			query.AppendFormat (" FROM {0}", this.From);

			if (this.WhereColumns.ContainsKey("others") && !String.IsNullOrEmpty(this.WhereColumns["others"].Trim())) {
				query.AppendFormat (" WHERE {0}", this.WhereColumns ["others"]);
			}

			return query.ToString ();
		}

		public override string ToString ()
		{
			return GetQuery (null);
		}

		public string GetQuery(ImportProcessorBase processor)
		{
			var sb = new StringBuilder ();

			/*
			 * SELECT
			 */
			sb.Append ("SELECT ");
			var first = true;

			foreach (string key in this.SelectColumns.Keys)
			{
				if (this.SelectColumns [key].Trim () == "")
					continue;
			
				// When SkipBinryColumns is true: Check if processor is given and current column is no binary
				if (SkipBinaryColumns && processor != null) {
					var doContinue = false;
					foreach (ReaderColumnInfo selectColumn in processor.GetSelectColumns()) {
						if (selectColumn.ColumnName == key) {
							if (selectColumn.DataType == typeof(byte[]))
								doContinue = true;
							break;
						}
					}

					if (doContinue)
						continue;
				}

				if (first)
					first = false;
				else
					sb.Append (", ");

				if(key == "others")
					sb.Append(this.SelectColumns[key]);
				else
					sb.AppendFormat ("{0} as [{1}]", this.SelectColumns [key], key);
			}

			/*
			 * FROM
			 */
			sb.AppendFormat (" FROM {0}", this.From);

			/*
			 * WHERE
			 */
			if (this.WhereColumns.Count > 0) {

				sb.Append (" WHERE ");
				first = true;

				foreach (string key in this.WhereColumns.Keys) {

					if (this.WhereColumns [key].Trim () != "") {
						if (first)
							first = false;
						else
							sb.Append (" AND ");

						sb.Append (this.WhereColumns [key]);
					}
				}
			}

			return sb.ToString ();
		}
	}
}

