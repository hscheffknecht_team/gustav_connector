﻿using System;
using System.IO;

namespace Gustav_Connector
{
	public static class Common
	{
		public static void ShowErrorDialog(Gtk.Window window, string text) {
			var msg = new Gtk.MessageDialog (
				window,
				Gtk.DialogFlags.Modal,
				Gtk.MessageType.Error,
				Gtk.ButtonsType.Ok,
				text
			);
			msg.Run ();
			msg.Destroy ();
		}

		public static void ShowInfoDialog(Gtk.Window window, string text) {
			var msg = new Gtk.MessageDialog (
				window,
				Gtk.DialogFlags.Modal,
				Gtk.MessageType.Info,
				Gtk.ButtonsType.Ok,
				text
			);
			msg.Run ();
			msg.Destroy ();
		}

		/// <summary>
		/// Create the specified directory within the executable path and returns the full path
		/// </summary>
		/// <param name="directoryName">Directory name</param>
		public static string GetSubdirectory(string directoryName) {
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, directoryName);
			if (!Directory.Exists (path)) {
				Directory.CreateDirectory (path);
			}

			return path;
		}

		public static string GetVersion() {
			var version = System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Version;
			return String.Format ("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
		}
	}
}

