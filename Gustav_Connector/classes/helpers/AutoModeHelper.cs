using System;

namespace Gustav_Connector
{
	public class AutoModeHelper
	{
		public AutoModeHelper ()
		{
		}

		public void DoWork()
		{
			this._StartProcessor (new ManufacturersProcessor (), "Hersteller");
			this._StartProcessor (new ImagesProcessor (), "Bilder");
			this._StartProcessor (new ProductgroupsProcessor (), "Produktgruppen");
			this._StartProcessor (new PricelistsProcessor (), "Preislisten");
			this._StartProcessor (new ProductsProcessor (), "Produkte");
			this._StartProcessor (new ProdgrprelProcessor (), "Prod.grp.-Zuordnungen");
			this._StartProcessor (new PricesProcessor (), "Preise");
			this._StartProcessor (new ProductimagesProcessor (), "Produktbild-Zuordnungen");
			this._StartProcessor (new DiscountsProcessor (), "Rabattspalten und -zeilen");
			this._StartProcessor (new StockProcessor (), "Lagerstände");
			this._StartProcessor (new CustomergroupsProcessor (), "ERP-Kundengruppen");
			this._StartProcessor (new CustomersProcessor (), "ERP-Kunden");
			this._StartProcessor (new ContactsProcessor (), "ERP-Ansprechpartner");
			this._StartProcessor (new OrderProcessor (), "Aufträge");
			this._StartProcessor (new OrderStatusProcessor (), "Auftragsstatus");
			this._StartProcessor (new TrackingCodesProcessor (), "Trackingnummern");

			if (GlobalSettings.DoChecks) {
				GlobalSettings.LastCheckDate = DateTime.Today.ToShortDateString ();
				GlobalSettings.Save ();
			}
		}

		private void _StartProcessor(ProcessorBase processor, string entitiesName)
		{
			Console.Write("Verarbeite: {0}... ", entitiesName);
			var result = processor.Start(false);
			if(result) {
				Console.WriteLine("abgeschlossen");
			} else {
				Console.WriteLine ("fehlgeschlagen");
				if (Logger.Current.LastException != null) {
					Console.WriteLine (Logger.Current.LastException.ToString ());
				}
			}

			if (_DoCheck(processor)) {
				Console.WriteLine ("Prüfe {0}...", entitiesName);
				try {
					var checkResult = ((ImportProcessorBase)processor).Check ();
					if (checkResult.getNumberErrors() > 0) {
						var soap = new SoapConnector ();
						soap.SendErrorMail (String.Format("Fehler in {0}:\r\n{1}", entitiesName, checkResult));
					}
				} catch (NotSupportedException ex) {
					Console.WriteLine (ex.Message);
				} catch (Exception ex) {
					Logger.Current.HandleException (ex, false);
				}
			}
		}

		private bool _DoCheck(ProcessorBase processor)
		{
			if (!GlobalSettings.DoChecks)
				return false;

			if (!(processor is ImportProcessorBase))
				return false;

			string date = DateTime.Today.ToShortDateString ();
			return date != GlobalSettings.LastCheckDate;
		}
	}
}
