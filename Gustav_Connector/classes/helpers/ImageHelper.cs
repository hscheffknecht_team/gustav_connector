using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Gustav_Connector
{
	public static class ImageHelper
	{
		public static bool IsImage(string filename) {

			string[] imageExtensions = new String[] { "jpg", "jpeg", "png", "tif", "tiff" };
			string ext = Path.GetExtension (filename);
			if(ext != "") 
				ext = ext.Substring (1).ToLower ();
			return Array.IndexOf (imageExtensions, ext) >= 0;
		}

		/// <summary>
		/// Reduziert auf eine bestimmte Größe und konvertiert die Datei zu JPG
		/// </summary>
		/// <param name="imgData"></param>
		/// <returns></returns>
		public static byte[] ReduceAndConvertToJPG(byte[] imgData)
		{
			using (Stream inputStream = new MemoryStream (imgData)) {
				using (Bitmap image = new Bitmap (inputStream)) {

					// Größe anpassen
					Size newSize = GetReducedSize (image.Size);

					using (Bitmap reducedImage = new Bitmap (newSize.Width, newSize.Height)) {

						// Draw white background (for transparent PNGs) and image
						using (var graphics = Graphics.FromImage (reducedImage)) {
							graphics.Clear (Color.White);
							graphics.DrawImage (image, new Rectangle (new Point (), newSize));
							graphics.Dispose ();
						}

						image.Dispose ();
						inputStream.Dispose ();

						// Als JPEG speichern
						using (MemoryStream outputStream = new MemoryStream ()) {
							SaveJpeg (outputStream, reducedImage, 80);
							byte[] outputData = _GetDataFromStream (outputStream);

							reducedImage.Dispose ();
							outputStream.Dispose ();

							return outputData;
						}
					}
				}
			}
		}

		/// <summary>
		/// Reduziert die Größe auf mindestens 1181x1181px (10cmx10cm bei 300DPI) und behält das Seitenverhältnis bei
		/// </summary>
		/// <param name="originalSize"></param>
		/// <returns></returns>
		public static Size GetReducedSize(Size originalSize)
		{
			Size newSize = new Size(1181, 1181);

			if (originalSize.Width < 1181 || originalSize.Height < 1181)
				return originalSize;

			if (originalSize.Width > originalSize.Height)
			{
				// Querformat
				return new Size(
					(int)((float)originalSize.Width / (float)originalSize.Height * (float)newSize.Height),
					newSize.Height
					);
			}
			else
			{
				// Hochformat
				return new Size(
					newSize.Width,
					(int)((float)originalSize.Height / (float)originalSize.Width * (float)newSize.Width)
					);
			}
		}

		public static void SaveJpeg(Stream stream, Bitmap img, int quality)
		{
			if (quality < 0 || quality > 100)
				throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");


			// Encoder parameter for image quality 
			EncoderParameter qualityParam =
				new EncoderParameter(Encoder.Quality, quality);
			// Jpeg image codec 
			ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

			EncoderParameters encoderParams = new EncoderParameters(1);
			encoderParams.Param[0] = qualityParam;

			img.Save(stream, jpegCodec, encoderParams);
		}

		/// <summary> 
		/// Returns the image codec with the given mime type 
		/// </summary> 
		private static ImageCodecInfo GetEncoderInfo(string mimeType)
		{
			// Get image codecs for all image formats 
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

			// Find the correct image codec 
			for (int i = 0; i < codecs.Length; i++)
				if (codecs[i].MimeType == mimeType)
					return codecs[i];
			return null;
		}

		public static byte[] GetBinaryData(byte[] data) {

			if (data.Length < 1000) {

				string uri = System.Text.Encoding.Default.GetString (data);

				if (uri.StartsWith ("file://")) {
					return _GetBinaryDataFromFile (uri.Substring (7));
				}
			}

			return data;
		}

		private static byte[] _GetBinaryDataFromFile(string filename)
		{
			if(File.Exists(filename))
			{
				// Datei ist im Verzeichnis
				return File.ReadAllBytes(filename);
			}
			else
			{
				throw new FileNotFoundException (filename);
			}
		}

		public static bool CheckFileSize(string imageKey, string imagesPath)
		{
			string filename = Path.Combine(imagesPath, imageKey);
			FileInfo file = new FileInfo(filename);
			return file.Length < 10 * 1024 * 1024;
		}

		public static string ReadExifDescription(System.Drawing.Image image)
		{
			PropertyItem[] items = image.PropertyItems;
			foreach (PropertyItem pi in items)
			{
				if (pi.Id == 270) // Image Description
				{
					string val =
						System.Text.Encoding.Default.GetString
							(pi.Value);
					return val;
				}
			}
			return String.Empty;
		}

		public static string ReadIptcDescription(string filename)
		{
			try
			{
				//JpegMetaData data = new JpegMetaData(filename);
				return null; // (string)data.PS3TagContents["PS3SectionCaptionTag"];
			}
			catch (Exception ex)
			{
				Console.WriteLine("Fehler beim Auslesen der IPTC Daten aus " + filename + ": " + ex.Message);
				return "";
			}
		}

		private static byte[] _GetDataFromStream(Stream stream) {
			stream.Position = 0;
			var data = new Byte[(int)stream.Length];
			stream.Read (data, 0, data.Length);
			return data;
		}
	}
}
