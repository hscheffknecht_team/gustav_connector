using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Gustav_Connector
{
	public class ImagesLoader
	{
		private Dictionary<string,Dictionary<string,string>> _OldCache;
		private string _Directory;
		private DataSet _Data;
		private string _ImageNameSeparator = "_";
		private bool _LoadMultiProducts = false;
		private bool _LoadExifDescription = false;
		private string[] _AllowedExtensions;

		public ImagesLoader (string directory, Dictionary<string,Dictionary<string,string>> oldCache, string[] allowedExtenstions)
		{
			this._Directory = directory;
			this._OldCache = oldCache;
			this._AllowedExtensions = allowedExtenstions;

			this._CreateDataSet ();
			this._LoadData (directory);
			this._LoadNewData ();
		}

		private void _CreateDataSet() {

			var newImages = new DataTable ("new");
			
			newImages.Columns.Add ("imagekey");
			newImages.Columns.Add ("productkey");
			newImages.Columns.Add ("filename");
			newImages.Columns.Add ("filesize", typeof(Int32));
			newImages.Columns.Add ("filedata");
			newImages.Columns.Add ("content-type");
			newImages.Columns.Add ("sort", typeof(Int32));
			newImages.Columns.Add ("exif-descr");
			newImages.Columns.Add ("plugindata");
			newImages.Columns.Add ("lastchange", typeof(DateTime));
			newImages.Columns.Add ("timestamp");

			newImages.PrimaryKey = new DataColumn[] { newImages.Columns ["imageKey"] };

			var allImages = newImages.Clone ();
			allImages.TableName = "all";

			// Create dataset
			this._Data = new DataSet ();
			this._Data.Tables.Add (newImages);
			this._Data.Tables.Add (allImages);
		}

		public Dictionary<string,Dictionary<string,string>> GetDataAsDictionary(bool getAllFiles)
		{
			DataTable table;

			if (getAllFiles)
				table = this._Data.Tables ["all"];
			else
				table = this._Data.Tables ["new"];

			var data = new Dictionary<string,Dictionary<string,string>> ();
			var columnsToCopy = new String[] { "imagekey", "productkey", "filesize", "lastchange" };

			foreach (DataRow row in table.Rows) {

				var item = new Dictionary<string,string> ();
				foreach (string col in columnsToCopy) {
					if (row [col] is DateTime) {
						item.Add (col, ((DateTime)row [col]).ToString ("yyyy-MM-dd HH:mm:ss"));
					} else {
						item.Add (col, row [col].ToString ());
					}
				}
				var key = ((string)row ["imagekey"]).ToLower ();
				data [key] = item;
			}

			return data;
		}

		public DataTable GetDataAsTable(bool getAllFiles) {

			if (getAllFiles)
				return this._Data.Tables ["all"];
			else
				return this._Data.Tables ["new"];
		}

		private void _LoadData(string directory) {
			var allImages = this._Data.Tables ["all"];

			foreach (string fullFilename in Directory.GetFiles(directory))
			{
				string filename = Path.GetFileName(fullFilename);
				string ext = Path.GetExtension(filename).ToLower();

				if (Array.IndexOf (this._AllowedExtensions, "*") == -1 &&
					ext != "" &&
					Array.IndexOf (this._AllowedExtensions, ext.Substring (1).ToLower()) == -1) {
					continue;
				}

				string shortFilename = Path.GetFileNameWithoutExtension(filename);

				shortFilename = shortFilename.Replace ("&slash;", "/");
				filename = filename.Replace ("&slash;", "/");

				if (String.IsNullOrEmpty (shortFilename) || allImages.Rows.Contains(new object[] { filename}))
					continue;

				string[] fnParts = shortFilename.Split(new String[] {this._ImageNameSeparator}, StringSplitOptions.RemoveEmptyEntries);

				FileInfo file = new FileInfo(fullFilename);
				var lastChange = file.CreationTime > file.LastWriteTime ? file.CreationTime : file.LastWriteTime;

				if (this._LoadMultiProducts)
				{
					new NotImplementedException ("multiProducts ist noch nicht implementiert");
				}

				// Read data
				var row = allImages.NewRow ();
				row ["imagekey"] = filename;
				row ["productkey"] = fnParts[0];
				row ["filename"] = filename;
				row ["filesize"] = (int)file.Length;
				row ["filedata"] = "file://" + fullFilename;
				row ["lastchange"] = lastChange;

				// EXIF Daten erst lesen, wenn es sich um ein neues/geändertes Bild handelt
				if (this._LoadExifDescription)
				{
					new NotImplementedException ("exif-descr ist noch nicht implementiert");
					//row["exif-descr"] = InterfaceLibrary.ImageHelper.ReadIptcDescription(fullFilename);
				}

				// Plugindata
				if (fnParts.Length > 2)
				{
					row["plugindata"] = fnParts[2];
				}

				// Sort
				int sort = 0;
				if(fnParts.Length > 1) {
					if (!Int32.TryParse(fnParts[1], out sort) && fnParts.Length == 2)
					{
						// Keine Zahl an 2. Stelle und es gibt keine 3. Stelle -> 2. Stelle sind Plugin Daten
						row["plugindata"] = fnParts[1];
					}
				}

				row ["sort"] = sort;

				allImages.Rows.Add (row);
			}

			// Unterverzeichnisse durchsuchen
			foreach(string subdir in Directory.GetDirectories(directory))
			{
				_LoadData (subdir);
			}
		}

		protected void _LoadNewData()
		{
			var newImages = this._Data.Tables ["new"];

			foreach (DataRow row in this._Data.Tables ["all"].Rows) {
				var filename = (string)row ["imagekey"];
				var newCacheItem = new Dictionary<string,string> ();
				newCacheItem.Add ("imagekey", filename);
				newCacheItem.Add ("filesize", row ["filesize"].ToString ());
				newCacheItem.Add ("lastchange", ((DateTime)row ["lastchange"]).ToString ("yyyy-MM-dd HH:mm:ss"));

				Dictionary<string,string> oldCacheItem = null;
				if (this._OldCache.ContainsKey (filename)) {
					// Backward compatibility
					oldCacheItem = this._OldCache[filename];
				}
				if (this._OldCache.ContainsKey (filename.ToLower())) {
					oldCacheItem = this._OldCache[filename.ToLower()];
				}

				if (oldCacheItem == null || oldCacheItem["filesize"] != newCacheItem["filesize"] || oldCacheItem["lastchange"] != newCacheItem["lastchange"])
				{
					// Copy row to newImages table
					var newRow = newImages.NewRow ();
					foreach (DataColumn col in newImages.Columns) {
						newRow [col.ColumnName] = row [col.ColumnName];
					}
					newImages.Rows.Add (newRow);

					// Add item to cache
					if (!this._OldCache.ContainsKey (filename)) {
						this._OldCache.Add (filename, newCacheItem);
					}
				}
			}
		}
	}
}
