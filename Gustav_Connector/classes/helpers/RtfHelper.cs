﻿using System;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Net.Sgoliver.NRtfTree.Core;
using Net.Sgoliver.NRtfTree.Util;

namespace Gustav_Connector
{
    public static class RtfHelper
    {
        private static List<string> _OpenTags = new List<string>();
        private static bool _HtmlEncode;
		private static Dictionary<int,string> ColorTable;
		private static NumberFormatInfo _NumberFormat = new NumberFormatInfo () { NumberDecimalSeparator = "." };
		private static float _CurrentFontSize = 1;
		private static int _CurrentFontColor = 0;
		private static bool _OpenedSpanTag = false;

		public static float BaseFontSize { get; set; }

        public static string ToHtml(string rtf)
        {
            return RtfHelper.ToHtml(rtf, true);
        }

        public static string ToHtml(string rtf, bool htmlencode)
        {
			_CurrentFontColor = 0;
			_CurrentFontSize = 1;
			_OpenedSpanTag = false;
			_OpenTags.Clear ();

            RtfHelper._HtmlEncode = htmlencode;

            RtfTree tree = new RtfTree();
            tree.MergeSpecialCharacters = true;
            tree.LoadRtfText(rtf);

            StringBuilder sb = new StringBuilder();
            FillHtml(tree.RootNode, sb);

			// Remove empty span tags
			return Regex.Replace (sb.ToString (), @"<span[^>]*></span>", String.Empty).Trim();
        }

        public static string ToText(string rtf)
        {
            RtfTree tree = new RtfTree();
            tree.MergeSpecialCharacters = true;
            tree.LoadRtfText(rtf);

            return tree.Text;
        }

		private static void ChangeFormat(StringBuilder sb, int? fontSize, int? fontColor) {
			if ((fontSize == null || fontSize == _CurrentFontSize) && (fontColor == null || fontColor == _CurrentFontColor)) {
				return;
			}

			WriteSpanEnd (sb);

			if (fontSize != null && BaseFontSize != 0) {
				_CurrentFontSize = ((float)(int)fontSize / 2) / BaseFontSize;
			}

			if (fontColor != null) {
				_CurrentFontColor = (int)fontColor;
			}

			WriteSpanStart (sb);
		}

		private static void WriteSpanStart(StringBuilder sb) {

			if (_OpenedSpanTag) {
				WriteSpanEnd (sb);
			}

			if (_CurrentFontSize != 1 || (_CurrentFontColor != 0 && ColorTable [_CurrentFontColor] != "#000000")) {
				sb.Append ("<span style=\"");
				if (_CurrentFontColor != 0 && ColorTable [_CurrentFontColor] != "#000000") {
					sb.Append ("color:");
					sb.Append (ColorTable [_CurrentFontColor]);
					sb.Append (";");
				}
				if (_CurrentFontSize != 1) {
					sb.Append ("font-size:");
					sb.Append (_CurrentFontSize.ToString(_NumberFormat));
					sb.Append ("em;");
				}
				sb.Append ("\">");
				_OpenedSpanTag = true;
			}
		}

		private static void WriteSpanEnd(StringBuilder sb) {
			if (_CurrentFontSize != 1 || _CurrentFontColor != 0 && _OpenedSpanTag) {
				sb.Append ("</span>");
				_OpenedSpanTag = false;
			}
		}

        private static void FillHtml(RtfTreeNode node, StringBuilder sb)
		{
			foreach (RtfTreeNode childNode in node.ChildNodes) {
				// Ignore some groups (fonttbl, colortbl, stylesheets, etc.)
				if (childNode.NodeType != RtfNodeType.Text &&
				                (childNode.NodeKey == "fonttbl" || childNode.NodeKey == "colortbl" || childNode.NodeKey == "stylesheet" || childNode.NodeKey == "info" || childNode.NodeKey == "xmlns" || childNode.NodeKey == "*"))
					break;

				FillHtml (childNode, sb);
			}

			switch (node.NodeType) {
				case RtfNodeType.Text:
					// Plain text (can contain rtf escaped special chars)
					if (node.ParentNode.FirstChild.NodeKey == "pntext") {
						return;
					}
					if (RtfHelper._HtmlEncode) {
						sb.Append (System.Web.HttpUtility.HtmlEncode (node.NodeKey));
					} else {
						sb.Append (node.NodeKey);
					}
					break;
				case RtfNodeType.Keyword:
					// Keywords has the format "\\{keyword}{parameter-number}"
					AddHtmlTag (node, sb);
					break;
				case RtfNodeType.Control:
					// Example "\\~" for non-breaking space
					AddControlElement (node, sb);
					break;
				case RtfNodeType.Group:
					// Groups are within {...}
					CloseOpenedTags (sb);

					if (node.FirstChild != null && node.FirstChild.NodeKey == "colortbl") {
						ReadColorTable (node);
					}
					break;
			}
		}

		private static void ReadColorTable(RtfTreeNode node)
		{
			var colors = new Dictionary<int,string> ();
			int r = 0, g = 0, b = 0;

			foreach (RtfTreeNode child in node.ChildNodes) {
				switch (child.NodeKey) {
				case "red":
					r = (byte)child.Parameter;
					break;
				case "green":
					g = (byte)child.Parameter;
					break;
				case "blue":
					b = (byte)child.Parameter;
					colors.Add (
						colors.Count + 1,
						"#" + r.ToString ("X2") + g.ToString ("X2") + b.ToString ("X2")
					);
					r = 0;
					b = 0;
					g = 0;
					break;
				}
			}

			ColorTable = colors;
		}

        private static void AddControlElement(RtfTreeNode node, StringBuilder sb)
        {
            switch (node.NodeKey)
            {
                case "~":
                    sb.Append("&nbsp;");
                    break;
            }
        }

        private static void AddHtmlTag(RtfTreeNode node, StringBuilder sb)
        {
            string tagName = null;
            bool endTagNeeded = true;

			switch (node.NodeKey) {
				case "b":
					tagName = "strong";
					break;
				case "i":
					tagName = "em";
					break;
				case "ul":
					tagName = "u";
					break;
				case "u":
					if (node.Parameter == 8232) {
						tagName = "br";
						endTagNeeded = false;
					} else
						return;
					break;
				case "par":
					// End of paragraph -> end of list item
					if (_OpenTags.Contains ("li")) {
						sb.Append ("</li>");
						_OpenTags.Remove ("li");
						return;
					} else {
						tagName = "br";
						endTagNeeded = false;
					}
					break;
				case "cf":
					// Font color
					ChangeFormat (sb, null, node.Parameter);
					return;
				case "fs":
					// Font size
					ChangeFormat (sb, node.Parameter, null);
					return;
				case "pn":
					// List
					tagName = "ul";
					break;
				case "pntext":
					// List item
					if (!_OpenTags.Contains ("ul")) {
						WriteSpanEnd (sb);
						sb.Append ("<ul>");
						_OpenTags.Add ("ul");
					}
					tagName = "li";
					break;
				case "pard":
					// Reset paragraphs -> end of list
					if (_OpenTags.Contains ("ul")) {
						sb.Append ("</ul>");
						_OpenTags.Remove ("ul");
						WriteSpanStart (sb);
					}
					return;
				case "line":
					tagName = "br";
					break;
				default:
					return;
			}

            bool endTag = node.HasParameter && node.Parameter == 0;

			// Close span tags
			WriteSpanEnd (sb);

            if (!endTag)
            {
                // Begin tag
				sb.AppendFormat("<{0}>", tagName);
                if (endTagNeeded && !_OpenTags.Contains(tagName))
                    _OpenTags.Add(tagName);
            }
            else
            {
                // End tag
                sb.AppendFormat("</{0}>", tagName);
                if (endTagNeeded && _OpenTags.Contains(tagName))
                    _OpenTags.Remove(tagName);
            }

			// Start new span tags
			WriteSpanStart (sb);
        }

        private static void CloseOpenedTags(StringBuilder sb)
        {
			if (_OpenTags.Contains ("ul")) {
				return;
			}

			_OpenTags.Reverse ();
			foreach (string tagName in _OpenTags)
            {
                sb.AppendFormat("</{0}>", tagName);
            }
            _OpenTags.Clear();
        }
    }
}
