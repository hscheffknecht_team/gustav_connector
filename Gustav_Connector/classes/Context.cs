using System;

namespace Gustav_Connector
{
	public class Context
	{
		
		private static Context _Current = new Context();
		public static Context Current {
			get { return _Current; }
		}

		public bool AutoMode { get; set; }

		public Context ()
		{
		}
	}
}

