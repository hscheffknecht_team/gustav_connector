using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Gustav_Connector
{
	public class EntityKeysCache
	{
		public EntityKeysCache (string ns)
		{
			this.Namespace = ns;
			this._CurrentItems = new Dictionary<string, Dictionary<string, string>> ();

			if (!Directory.Exists ("caches"))
				Directory.CreateDirectory ("caches");

			this.Load ();
		}

		public bool HasItem(string key) {
			return this._CurrentItems.ContainsKey (key);
		}

		public void SetItem(string key, Dictionary<string,string> item) {
			this._CurrentItems [key] = item;
		}

		public Dictionary<string,string> GetItem(string key) {
			return this._CurrentItems [key];
		}

		public void DeleteItem(string key) {
			this._CurrentItems.Remove (key);
		}

		public void ClearItems() {
			this._CurrentItems.Clear ();
		}

		public Dictionary<string,Dictionary<string,string>> GetAllItems() {
			return this._CurrentItems;
		}

		public void SetAllItems(Dictionary<string,Dictionary<string,string>> items) {
			this._CurrentItems = items;
		}

		public string Namespace { get; set; }
		private Dictionary<string,Dictionary<string,string>> _CurrentItems { get; set; }

		public void Load() {

			string filename = this._GetFilename ();

			if (File.Exists (filename)) {

				var cacheItems = new Dictionary<string,Dictionary<string,string>> ();

				var table = new DataTable ();
				table.ReadXml (filename);

				foreach (DataRow row in table.Rows) {

					var cacheItem = new Dictionary<string,string> ();

					foreach (DataColumn col in table.Columns) {
						if (row [col.ColumnName] != DBNull.Value)
							cacheItem [col.ColumnName] = (string)row [col.ColumnName];
					}

					cacheItems.Add ((string)row["primary_key"], cacheItem);
				}

				this._CurrentItems = cacheItems;
			}
		}

		public void Save() {

			var table = new DataTable (this.Namespace);
			table.Columns.Add ("primary_key");

			foreach (var key in this._CurrentItems.Keys) {

				var cacheItem = this._CurrentItems [key];

				if (table.Columns.Count == 1) {
					foreach (string colname in cacheItem.Keys) {
						if (colname != "primary_key")
							table.Columns.Add (colname);
					}
				}

				var row = table.NewRow ();

				// Add data
				row ["primary_key"] = key;

				foreach (var keyName in cacheItem.Keys) {
					row [keyName] = cacheItem [keyName];
				}

				table.Rows.Add (row);
			}

			table.WriteXml (this._GetFilename (), XmlWriteMode.WriteSchema);
		}

		private string _GetFilename() {

			return Path.Combine("caches", this.Namespace + "-cache.xml");
		}
	}
}

