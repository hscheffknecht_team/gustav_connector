using System;
using System.Data;

namespace Gustav_Connector
{
	public class ContactsDataCache
	{
		public static DataTable DatabaseTable { get; set; }
		public static DataTable GustavTable { get; set; }
	}
}

