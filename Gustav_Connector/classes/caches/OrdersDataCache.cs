using System;
using System.Data;

namespace Gustav_Connector
{
	public static class OrdersDataCache
	{
		public static DataTable Orders { get; set; }
		public static DataTable OrderItems { get; set; }
		public static DataTable Customers { get; set; }
	}
}

