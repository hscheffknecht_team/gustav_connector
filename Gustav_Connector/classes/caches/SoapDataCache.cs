using System;
using System.Collections.Generic;
using System.Xml;

namespace Gustav_Connector
{
	public static class SoapDataCache
	{
		private static XmlNode _Orders;

        public static XmlNode FinalOrders { get; set; }

		public static XmlNode Orders
		{
			get
			{
				if(_Orders == null) {

					SoapConnector soap = new SoapConnector();

					try {
						_Orders = soap.GetOrders();
					} catch(Exception ex) {
						Logger.Current.HandleException(ex, true);
					}

				}

				return _Orders;
			}
		}

		public static void Clear() 
		{
			_Orders = null;
		}

		public static string[] GetOrderFields()
		{
			return GetOrderFields(false);
		}

		public static string[] GetOrderFields(bool optional)
		{
			var fields = new List<string>();
			fields.Add("");

			if (FinalOrders != null && FinalOrders.ChildNodes.Count > 0) {
				_AddNodesToList(FinalOrders.FirstChild, ref fields, null);
			} else if(Orders != null && Orders.ChildNodes.Count > 0) {
				_AddNodesToList(Orders.FirstChild, ref fields, null);
			}

			return fields.ToArray();
		}

		private static void _AddNodesToList(XmlNode node, ref List<string> list, string pathprefix)
		{
			string newPathprefix = pathprefix;
			if(pathprefix != null && node.NodeType == XmlNodeType.Element) {
				list.Add(pathprefix+node.Name);
				newPathprefix = pathprefix+node.Name + "/";
			} else {
				newPathprefix = "";
			}

			foreach(XmlNode child in node.ChildNodes)
			{
				_AddNodesToList(child, ref list, newPathprefix);
			}
		}
	}
}

