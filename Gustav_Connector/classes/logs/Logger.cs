using System;
using System.IO;

namespace Gustav_Connector
{
	public class Logger
	{
		private static Logger _Current = new Logger();
		public static Logger Current {
			get { return _Current; }
		}

		private string _Filename = "log.txt";
		private Exception _LastException = null;

		public Logger ()
		{
		}

		public Exception LastException { 
			get {
				return this._LastException;
			}
		}

		public void HandleWarning(string warning, bool showDialog) 
		{
			File.AppendAllText(this._Filename, warning + "\r\n\r\n--------\r\n\r\n");

			if(showDialog && !Context.Current.AutoMode) {
				this.ShowDialog (warning, Gtk.MessageType.Warning);
			}
		}

		public void HandleException(Exception ex, bool showDialog) {

			HandleException (ex, showDialog, Context.Current.AutoMode && GlobalSettings.SendErrorMails);
		}

		public void HandleException(Exception ex, bool showDialog, bool sendErrorMail)
		{
			string error = String.Format ("{0}\r\n{1}\r\n\r\n--------\r\n\r\n", DateTime.Now.ToString (), ex.ToString ());
			File.AppendAllText(this._Filename, error);
			this._LastException = ex;

			if(showDialog) {
				this.ShowDialog ("Ein Fehler ist aufgetreten: " + ex.Message, Gtk.MessageType.Error);
			}

			if (sendErrorMail) {
				// Send error as mail
				var soap = new SoapConnector ();
				soap.SendErrorMail (ex.ToString ());
			}
		}

		private void ShowDialog(string msg, Gtk.MessageType messageType) {

			if(msg.Length > 200)
				msg = msg.Substring (0, 200) + "...";

			msg = msg
				.Replace ("{", "{{")
				.Replace ("}", "}}")
				.Replace ("\\", "\\\\");

			// Show message
			Gtk.Application.Invoke (delegate {
				var dialog = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, messageType, Gtk.ButtonsType.Ok, msg);
				dialog.Run ();
				dialog.Destroy ();
			});
		}
	}
}

