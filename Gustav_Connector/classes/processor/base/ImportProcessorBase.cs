using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Gtk;

namespace Gustav_Connector
{
	public abstract class ImportProcessorBase : ProcessorBase
	{
		public DataTable DatabaseTable { get; set; }
		public DataTable GustavTable { get; set; }
		public DataSet TemporaryTables { get; set; }
		public int CurrentPercentage { get; set; }

		public event EventHandler ProgressChanged;

		protected abstract string _GetSettingsKey();
		protected ImportSettings _Settings = null;
		protected bool _DeleteOldEntries = true;

		public ImportSettings Settings {
			get { return this._Settings; }
			set { this._Settings = value; }
		}

		public ImportProcessorBase ()
		{
			this._Settings = new ImportSettings (this._GetSettingsKey ());
		}

		protected virtual void OnProgressChanged(int currentPercentage) 
		{
			this.CurrentPercentage = currentPercentage;

			if (ProgressChanged != null)
				ProgressChanged(this, new EventArgs());
		}

		/// <summary>
		/// Returns table to write into (creates and returns the specified temp. table or return this.DatabaseTable)
		/// </summary>
		/// <param name="plugin">Plugin.</param>
		private DataTable _GetTargetTable(ReaderPluginBase plugin) {
			if (String.IsNullOrEmpty (plugin.TemporaryTableName)) {
				return this.DatabaseTable;
			}

			if (this.TemporaryTables.Tables [plugin.TemporaryTableName] == null) {
				this.TemporaryTables.Tables.Add (plugin.TemporaryTableName);
			}

			return this.TemporaryTables.Tables [plugin.TemporaryTableName];
		}

		private void _AddDataToTable(DataTable data, DataTable table) {

			// Check columns
			foreach (DataColumn col in data.Columns) {
				if (!table.Columns.Contains (col.ColumnName)) {
					table.Columns.Add (col.ColumnName, col.DataType);
				}
			}

			// Copy data
			foreach (DataRow srcRow in data.Rows) {

				var dstRow = table.NewRow ();

				foreach (DataColumn col in data.Columns) {

					dstRow [col.ColumnName] = srcRow [col.ColumnName];
				}

				table.Rows.Add (dstRow);
			}
		}

		protected override void _Start(bool preview)
		{
			this.DatabaseTable = new DataTable ();
			this.TemporaryTables = new DataSet ();

			this.OnProgressChanged (0);

			// Iterate all readers
			foreach (ReaderPluginBase plugin in _Settings.Readers) 
			{
				this._AddDataToTable (plugin.GetData (this), this._GetTargetTable(plugin));
			}

			this.GustavTable = this.DatabaseTable.Copy ();

			this.OnProgressChanged (25);

			// Iterate all plugins
			foreach (CommonPluginBase plugin in _Settings.Plugins) 
			{
				plugin.ProcessTable (this.GustavTable, this);
			}

			this.OnProgressChanged (50);

			if (!preview) 
			{
				var soap = new SoapConnector ();
				this._SendDataToServer (soap);

				this.OnProgressChanged (75);

				// Delete old entries
				if (this._DeleteOldEntries) {
					foreach (ReaderPluginBase reader in _Settings.Readers) {
						if (String.IsNullOrEmpty (reader.TemporaryTableName)) {
							var deletedKeys = reader.GetDeletedKeys (this);
							if (deletedKeys.Length > 0) {
								this._DeleteDataFromServer (soap, deletedKeys);
							}
						}
					}
				}

				this.OnProgressChanged (85);

				// Save state of reader(s)

				foreach (ReaderPluginBase plugin in _Settings.Readers) 
				{
					plugin.SaveState (this.DatabaseTable, this);
					plugin.SaveCache ();
				}

				_Settings.Save();
			}

			this.OnProgressChanged (100);
		}

		protected abstract void _SendDataToServer(SoapConnector soap);

		protected virtual void _DeleteDataFromServer(SoapConnector soap, Dictionary<string,string>[] keys) 
		{
			throw new NotImplementedException ("Löschen der Entität wird noch nicht unterstützt");
		}

		public abstract ReaderColumnInfo[] GetSelectColumns ();

		public virtual ReaderColumnInfo[] GetWhereColumns ()
		{
			var columns = new ReaderColumnInfo[] {
				new ReaderColumnInfo ("timestamp", "Timestamp")
			};

			return columns;
		}

		public virtual ReaderColumnInfo[] GetKeyColumns()
		{
			var keyColumn = new ReaderColumnInfo ("key", "Key");
			return new ReaderColumnInfo[] { keyColumn };
		}

		public DataCheckResult Check()
		{
			// Validate
			if (_KeysTable == null)
				throw new NotSupportedException();

			if (_Settings.Readers.Count == 0)
				return new DataCheckResult (new String[0], new String[0]);

			// Load keys from server
			var connector = new SoapConnector ();
			var serverKeys = connector.GetKeys (_KeysTable);

			// Load keys from local database
			var localKeys = GetAllKeys();

			// Get local only keys
			var localOnly = localKeys.Except(serverKeys, StringComparer.InvariantCultureIgnoreCase).ToArray();

			// Get server only keys
			var remoteOnly = serverKeys.Except(localKeys, StringComparer.InvariantCultureIgnoreCase).ToArray();

			return new DataCheckResult (localOnly, remoteOnly);
		}

		public void DeleteFromServer( Dictionary<string,string>[] keys) {
			this._DeleteDataFromServer (new SoapConnector (), keys);
		}

		public string[] GetAllKeys() {
			this.DatabaseTable = new DataTable ();
			this.TemporaryTables = new DataSet ();

			this.OnProgressChanged (0);

			// Iterate all readers
			foreach (ReaderPluginBase plugin in _Settings.Readers) 
			{
				if (plugin is FilesReaderPlugin) {
					this._AddDataToTable (((FilesReaderPlugin)plugin).GetAllData (this), this._GetTargetTable (plugin));
				} else {
					plugin.QueryData.SkipBinaryColumns = true;
					var lastTimestamp = plugin.LastTimestamp;
					plugin.LastTimestamp = 0;

					this._AddDataToTable (plugin.GetData (this), this._GetTargetTable (plugin));

					plugin.LastTimestamp = lastTimestamp;
					plugin.QueryData.SkipBinaryColumns = false;
				}
			}

			this.GustavTable = this.DatabaseTable.Copy ();

			// Iterate all plugins
			foreach (CommonPluginBase plugin in _Settings.Plugins) 
			{
				if (plugin.CreatesOrDeletesRows)
					plugin.ProcessTable (this.GustavTable, this);
			}

			var keys = new List<string> ();
			foreach (DataRow row in this.GustavTable.Rows) {
				var itemKeys = new List<string> ();
				foreach (ReaderColumnInfo column in GetKeyColumns()) {
					itemKeys.Add (row [column.ColumnName].ToString ());
				}
				keys.Add(String.Join("$$$", itemKeys));
			}

			return keys.ToArray ();
		}
	}

}
