using System;

namespace Gustav_Connector
{
	public abstract class ProcessorBase
	{
		protected string _KeysTable = null;

		public ProcessorBase ()
		{
		}

		public bool Start (bool preview) {
			
			try 
			{
				this._Start(preview);
				return true;
			}
			catch(Exception ex) {
				if (Context.Current.AutoMode) {
					string msg = String.Format("Fehler beim Import/Export ({0})", this.GetType());
					Logger.Current.HandleException (
						new Exception(msg, ex),
						false
					);
				} else {
					Gtk.Application.Invoke (delegate {
						Logger.Current.HandleException (ex, true);
					});
				}

				return false;
			}
		}

		protected abstract void _Start (bool preview);
	}
}

