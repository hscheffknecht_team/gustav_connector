using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class ProductimagesProcessor : ImportProcessorBase
	{
		public ProductimagesProcessor ()
		{
		}

		protected override string _GetSettingsKey ()
		{
			return "productimages";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			// Split table in smaller ones with max. 1000 rows
			int curRow = 0, i = 0;
			DataTable curTable = this.GustavTable.Clone ();

			foreach (DataRow row in this.GustavTable.Rows) {

				curRow++;
				i++;

				// Add row 
				curTable.Rows.Add (row.ItemArray);

				// Check if we have 1000 rows
				if (curRow % 1000 == 0) {

					// Send to Gustav server
					soap.AddProductimages (curTable);

					// Clear table
					curTable.Clear ();

					// Reset counter
					curRow = 0;

					// Update percentage
					var percent = (int)((i / (double)this.GustavTable.Rows.Count) * 25);
					this.OnProgressChanged (percent + 50);
				}

			}

			// Send remaining rows to Gustav server
			soap.AddProductimages (curTable);
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeleteProductimages (keys);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			return new ReaderColumnInfo[] {
				new ReaderColumnInfo("productKey", "Produkt-Key"),
				new ReaderColumnInfo("imageKey", "Bild-Key"),
				new ReaderColumnInfo("sort", "Sortierung", typeof(Int32)),
				new ReaderColumnInfo ("timestamp", "Timestamp", typeof(Int32))
			};
		}

		public override ReaderColumnInfo[] GetKeyColumns ()
		{
			
			return new ReaderColumnInfo[] {
				new ReaderColumnInfo("productKey", "Produkt-Key"),
				new ReaderColumnInfo("imageKey", "Bild-Key")
			};
		}
	}
}

