﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class OrderStatusProcessor : ImportProcessorBase
	{
		public OrderStatusProcessor ()
		{
		}

		protected override string _GetSettingsKey ()
		{
			return "orderstatus";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			// Split table in smaller ones with max. 1000 rows
			int curRow = 0, i = 0;
			DataTable curTable = this.GustavTable.Clone ();

			foreach (DataRow row in this.GustavTable.Rows) {

				curRow++;
				i++;

				// Add row 
				curTable.Rows.Add (row.ItemArray);

				// Check if we have 1000 rows
				if (curRow % 1000 == 0) {

					// Send to Gustav server
					soap.UpdateOrders (curTable);

					// Clear table
					curTable.Clear ();

					// Reset counter
					curRow = 0;

					// Update percentage
					var percent = (int)((i / (double)this.GustavTable.Rows.Count) * 25);
					this.OnProgressChanged (percent + 50);
				}

			}

			// Send remaining rows to Gustav server
			soap.UpdateOrders (curTable);
		}

		public override ReaderColumnInfo[] GetKeyColumns ()
		{
			return new ReaderColumnInfo[0];
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("orderNumber", "Auftragsnummer"));
			columns.Add (new ReaderColumnInfo ("webshopSettingId", "Webshop-ID (webshopsettings)"));
			columns.Add (new ReaderColumnInfo ("shippingDate", "Lieferdatum"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeletePrices (keys);
		}
	}
}

