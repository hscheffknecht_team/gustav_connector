using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class PricesProcessor : ImportProcessorBase
	{
		public PricesProcessor ()
		{
			_KeysTable = "prices";
		}

		protected override string _GetSettingsKey ()
		{
			return "prices";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			// Split table in smaller ones with max. 1000 rows
			int curRow = 0, i = 0;
			DataTable curTable = this.GustavTable.Clone ();

			foreach (DataRow row in this.GustavTable.Rows) {

				curRow++;
				i++;

				// Add row 
				curTable.Rows.Add (row.ItemArray);

				// Check if we have 1000 rows
				if (curRow % 1000 == 0) {

					// Send to Gustav server
					soap.AddPrices (curTable);

					// Clear table
					curTable.Clear ();

					// Reset counter
					curRow = 0;

					// Update percentage
					var percent = (int)((i / (double)this.GustavTable.Rows.Count) * 25);
					this.OnProgressChanged (percent + 50);
				}

			}

			// Send remaining rows to Gustav server
			soap.AddPrices (curTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("productKey", "Produkt-Key"));
			columns.Add (new ReaderColumnInfo ("pricelistKey", "Preislisten-Key"));
			columns.Add (new ReaderColumnInfo ("price", "Preis", typeof(Double)));
			columns.Add (new ReaderColumnInfo ("isPromotionPrice", "Aktionspreis (j/n)"));
			columns.Add (new ReaderColumnInfo ("promotionStartDate", "Gültig ab"));
			columns.Add (new ReaderColumnInfo ("promotionEndDate", "Gültig bis"));
			columns.Add (new ReaderColumnInfo ("customerKey", "Kunden-Key"));
			columns.Add (new ReaderColumnInfo ("customergroupKey", "Kundengruppen-Key"));
			columns.Add (new ReaderColumnInfo ("minQty", "Mindestmenge"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeletePrices (keys);
		}
	}
}

