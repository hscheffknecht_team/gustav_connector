using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class CustomersProcessor : ImportProcessorBase
	{
		public CustomersProcessor ()
		{
			_KeysTable = "erpcustomers";
		}

		protected override string _GetSettingsKey ()
		{
			return "customers";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			// Split table in smaller ones with max. 100 rows
			int curRow = 0, i = 0;
			DataTable curTable = this.GustavTable.Clone ();

			foreach (DataRow row in this.GustavTable.Rows) {

				curRow++;
				i++;

				// Add row 
				curTable.Rows.Add (row.ItemArray);

				// Check if we have 1000 rows
				if (curRow % 100 == 0) {

					// Send to Gustav server
					soap.AddCustomers (curTable);

					// Clear table
					curTable.Clear ();

					// Reset counter
					curRow = 0;

					// Update percentage
					var percent = (int)((i / (double)this.GustavTable.Rows.Count) * 25);
					this.OnProgressChanged (percent + 50);
				}

			}

			// Send remaining rows to Gustav server
			soap.AddCustomers (curTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("company", "Firma"));
			columns.Add (new ReaderColumnInfo ("firstName", "Vorname"));
			columns.Add (new ReaderColumnInfo ("lastName", "Nachname"));
			columns.Add (new ReaderColumnInfo ("street", "Straße"));
			columns.Add (new ReaderColumnInfo ("postalCode", "PLZ"));
			columns.Add (new ReaderColumnInfo ("city", "Ort"));
			columns.Add (new ReaderColumnInfo ("countryCode", "Ländercode"));
			columns.Add (new ReaderColumnInfo ("phone", "Telefon"));
			columns.Add (new ReaderColumnInfo ("fax", "Fax"));
			columns.Add (new ReaderColumnInfo ("email", "E-Mail"));
			columns.Add (new ReaderColumnInfo ("vatId", "UID"));
			columns.Add (new ReaderColumnInfo ("discountRowNr1", "Rabattleiste 1"));
			columns.Add (new ReaderColumnInfo ("discountRowNr2", "Rabattleiste 2"));
			columns.Add (new ReaderColumnInfo ("customergroupKey", "Kundengruppen-Key"));
			columns.Add (new ReaderColumnInfo ("timestamp", "Timestamp"));
			columns.Add (new ReaderColumnInfo ("defaultPricelistKey", "Preislisten-Key"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeleteErpCustomers (keys);
		}
	}
}

