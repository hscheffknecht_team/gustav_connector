using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public partial class ProdgrprelProcessor : ImportProcessorBase
	{
		protected override string _GetSettingsKey ()
		{
			return "prodgrprel";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.AddProductgroupsRelations (this.GustavTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo("productKey", "Produkt-Key"));
			columns.Add (new ReaderColumnInfo("groupKey", "Gruppen-Key"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string,string>[] keys)
		{
			soap.DeleteProdgrprels (keys);
		}

		public override ReaderColumnInfo[] GetKeyColumns ()
		{
			return new ReaderColumnInfo[] {
				new ReaderColumnInfo("productKey", "Produkt-Key"),
				new ReaderColumnInfo("groupKey", "Gruppen-Key")
			};
		}
	}
}

