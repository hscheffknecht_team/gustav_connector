using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public class CustomergroupsProcessor : ImportProcessorBase
	{
		public CustomergroupsProcessor ()
		{
			_KeysTable = "erpcustomergroups";
		}

		protected override string _GetSettingsKey ()
		{
			return "customergroups";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.AddCustomergroups (this.DatabaseTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("name", "Name"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeleteErpCustomerGroups (keys);
		}
	}
}

