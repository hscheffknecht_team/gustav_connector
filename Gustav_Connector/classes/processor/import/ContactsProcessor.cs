using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public class ContactsProcessor : ImportProcessorBase
	{
		public ContactsProcessor ()
		{
			_KeysTable = "erpcontacts";
		}

		protected override string _GetSettingsKey ()
		{
			return "contacts";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.AddContacts (this.DatabaseTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("firstName", "Vorname"));
			columns.Add (new ReaderColumnInfo ("lastName", "Nachname"));
			columns.Add (new ReaderColumnInfo ("emailAddress", "E-Mail"));
			columns.Add (new ReaderColumnInfo ("erpCustomerKey", "Kunden-Key"));
			columns.Add (new ReaderColumnInfo ("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeleteErpContacts (keys);
		}
	}
}

