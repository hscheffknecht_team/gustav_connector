using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class ImagesProcessor : ImportProcessorBase
	{
		public ImagesProcessor ()
		{
			_KeysTable = "images";
		}

		protected override string _GetSettingsKey ()
		{
			return "images";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			if (GlobalSettings.SkipUpload)
				return;
			
			/*
			 * Split datatable to send an own request for each image
			 */

			// Create table
			var tbl = new DataTable ();
			foreach (DataColumn col in this.DatabaseTable.Columns) {

				if (col.ColumnName == "data") {
					tbl.Columns.Add ("data", typeof(byte[]));
				} else {
					tbl.Columns.Add (col.ColumnName, col.DataType);
				}
			}

			int i = 0;

			foreach (DataRow srcRow in this.DatabaseTable.Rows) {

				// Add row
				var dstRow = tbl.NewRow ();

				// Copy data
				var cont = false;
				foreach (DataColumn col in this.DatabaseTable.Columns) {

						// Load image data
					if (col.ColumnName == "data") {

						var data = ImageHelper.GetBinaryData ((byte[])srcRow ["data"]);

						try {	
							if (ImageHelper.IsImage((string) srcRow ["name"])) {
								dstRow ["data"] = ImageHelper.ReduceAndConvertToJPG (data);
							} else {
								dstRow["data"] = data;
							}
							GC.Collect();
							GC.WaitForPendingFinalizers();
						} catch (Exception ex) {
							Logger.Current.HandleWarning (
								String.Format ("Bild {0} konnte nicht verarbeitet werden: {1}", (string) srcRow ["key"], ex.Message),
								true
							);
							Logger.Current.HandleException (ex, false);
							cont = true;
							break;
						}
					} else {
						dstRow [col.ColumnName] = srcRow [col.ColumnName];
					}
				}

				if (cont) {
					continue;
				}
				tbl.Rows.Add (dstRow);

				// Send to server
				soap.AddImages (tbl);

				// Delete row
				dstRow.Delete ();

				// Update percentage
				i++;
				var percent = (int)(i / (double)this.DatabaseTable.Rows.Count * 25);
				this.OnProgressChanged (percent+50);
			}
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string, string>[] keys)
		{
			soap.DeleteImages (keys);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("name", "Name"));
			columns.Add (new ReaderColumnInfo ("description", "Beschreibung"));
			columns.Add (new ReaderColumnInfo ("data", "Inhalt", typeof(byte[])));
			columns.Add (new ReaderColumnInfo ("pluginData", "Plugin-Daten"));
			columns.Add (new ReaderColumnInfo ("timestamp", "Timestamp", typeof(Int32)));

			return columns.ToArray ();
		}
	}
}

