using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public partial class ProductsProcessor : ImportProcessorBase
	{
		public ProductsProcessor()
		{
			_KeysTable = "products";
		}

		protected override string _GetSettingsKey ()
		{
			return "products";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			// Split table in smaller ones with max. 100 rows
			int curRow = 0, i = 0;
			DataTable curTable = this.GustavTable.Clone ();

			foreach (DataRow row in this.GustavTable.Rows) {

				curRow++;
				i++;

				// Add row 
				curTable.Rows.Add (row.ItemArray);

				// Check if we have 1000 rows
				if (curRow % 100 == 0) {

					// Send to Gustav server
					soap.AddProducts (curTable);

					// Clear table
					curTable.Clear ();

					// Reset counter
					curRow = 0;

					// Update percentage
					var percent = (int)((i / (double)this.GustavTable.Rows.Count) * 25);
					this.OnProgressChanged (percent + 50);
				}

			}

			// Send remaining rows to Gustav server
			soap.AddProducts (curTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();
			
			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("ean", "EAN"));
			columns.Add (new ReaderColumnInfo ("mainProductKey", "Hauptprodukt-Key"));
			columns.Add (new ReaderColumnInfo ("name", "Name"));
			columns.Add (new ReaderColumnInfo ("shortDescription", "Kurzbeschreibung"));
			columns.Add (new ReaderColumnInfo ("description", "Beschreibung"));
			columns.Add (new ReaderColumnInfo ("unit", "Einheit"));
			columns.Add (new ReaderColumnInfo ("discountColumnNr", "Rabattspalte"));
			columns.Add (new ReaderColumnInfo ("createdOnErp", "ERP-Erstellungsdatum"));
			columns.Add (new ReaderColumnInfo ("sort", "Sortierung"));
			columns.Add (new ReaderColumnInfo ("qtyIncrement", "Losgröße"));
			columns.Add (new ReaderColumnInfo ("stock", "Lagerstand"));
			columns.Add (new ReaderColumnInfo ("packageSize", "Verpackungsgröße"));
			columns.Add (new ReaderColumnInfo ("manufacturerKey", "Hersteller-Key"));
			columns.Add (new ReaderColumnInfo ("pluginData", "Plugin-Daten"));
			columns.Add (new ReaderColumnInfo ("relatedProducts", "Zubehör"));
			columns.Add (new ReaderColumnInfo ("crossProducts", "Ähnliche Produkte"));
			columns.Add (new ReaderColumnInfo ("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string,string>[] keys)
		{
			soap.DeleteProducts (keys);
		}
	}
}

