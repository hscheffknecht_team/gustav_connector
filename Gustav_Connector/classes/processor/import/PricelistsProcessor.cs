using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public class PricelistsProcessor : ImportProcessorBase
	{
		public PricelistsProcessor ()
		{
			_KeysTable = "pricelists";
		}

		protected override string _GetSettingsKey ()
		{
			return "pricelists";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.AddPricelists (this.DatabaseTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo("key", "Key"));
			columns.Add (new ReaderColumnInfo("name", "Name"));
			columns.Add (new ReaderColumnInfo("sort", "Sortierung"));
			columns.Add (new ReaderColumnInfo ("gross", "Brutto (ja/nein)"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string,string>[] keys)
		{
			soap.DeletePricelists (keys);
		}
	}
}
