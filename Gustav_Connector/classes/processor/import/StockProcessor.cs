﻿using System;
using System.Collections.Generic;

namespace Gustav_Connector
{
	public class StockProcessor : ImportProcessorBase
	{
		public StockProcessor ()
		{
			this._DeleteOldEntries = false;
		}

		protected override string _GetSettingsKey ()
		{
			return "stock";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.UpdateStockQtys (this.DatabaseTable);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo("productKey", "Produkt-Key"));
			columns.Add (new ReaderColumnInfo("qty", "Lagerstand"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}
	}
}

