using System;
using System.Collections.Generic;
using System.Data;

namespace Gustav_Connector
{
	public class ProductgroupsProcessor : ImportProcessorBase
	{
		public ProductgroupsProcessor()
		{
			_KeysTable = "productgroups";
		}

		protected override string _GetSettingsKey ()
		{
			return "productgroups";
		}

		protected override void _SendDataToServer (SoapConnector soap)
		{
			soap.AddProductgroups (this.GustavTable);
		}

		protected override void _DeleteDataFromServer (SoapConnector soap, Dictionary<string,string>[] keys)
		{
			soap.DeleteProductgroups (keys);
		}

		public override ReaderColumnInfo[] GetSelectColumns ()
		{
			var columns = new List<ReaderColumnInfo> ();

			columns.Add (new ReaderColumnInfo ("key", "Key"));
			columns.Add (new ReaderColumnInfo ("name", "Name"));
			columns.Add (new ReaderColumnInfo ("parentKey", "ParentKey"));
			columns.Add (new ReaderColumnInfo ("imageKey", "ImageKey"));
			columns.Add (new ReaderColumnInfo ("sort", "Sortierung"));
			columns.Add (new ReaderColumnInfo("timestamp", "Timestamp"));

			return columns.ToArray ();
		}
	}
}

