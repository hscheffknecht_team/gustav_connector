using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;

namespace Gustav_Connector
{
	public class OrderProcessor : ProcessorBase
	{
		public DataTable AllOrders { get; set; }
		public DataTable AllOrderItems { get; set; }
		public DataTable AllCustomers { get; set; }

		public OrderProcessor ()
		{
		}

		protected override void _Start(bool preview)
		{
			if (OrderSettings.Current.OrdersPlugins.Count == 0)
				return;

			// Create datatables
			this.AllOrders = this._CreateDataTable(OrderSettings.Current.OrdersColumns);
			this.AllOrderItems = this._CreateDataTable(OrderSettings.Current.OrderItemsColumns);
			this.AllCustomers = this._CreateDataTable(OrderSettings.Current.CustomersColumns);

			// Clone Orders to FinalOrders
            SoapDataCache.FinalOrders = SoapDataCache.Orders.Clone();

            // Iterate soap source
			foreach(XmlNode node in SoapDataCache.FinalOrders.ChildNodes)
			{
				// Iterate plugins (OrderNode)
				foreach(var plugin in OrderSettings.Current.OrdersPlugins)
				{
					var orderPlugin = (OrderPluginBase)plugin;
					// Check filter
					if(orderPlugin.ProcessFilter(node)) {
						// Process event
						orderPlugin.ProcessOrderNode(node, this);
					}
				}
			}

			// Iterate plugins (End)
			foreach(var plugin in OrderSettings.Current.OrdersPlugins)
			{
				((OrderPluginBase)plugin).ProcessEnd(this);
			}

			// Write data
			OrdersDataCache.Orders = this.AllOrders;
			OrdersDataCache.OrderItems = this.AllOrderItems;
			OrdersDataCache.Customers = this.AllCustomers;

			// Write orders to DB
			if(!preview)
			{
				var writerNr = 0;
				foreach(WriterPluginBase writer in OrderSettings.Current.WriterPlugins)
				{
					writerNr++;
					writer.Process(this);
				}

				// Write approve order number
				if(SoapDataCache.Orders.ChildNodes.Count > 0)
				{
					_WriteLogFile ();
					_Check ();
					GlobalSettings.LastOrderApproveNumber = SoapDataCache.Orders.LastChild.SelectSingleNode("approve_number").InnerText;
					GlobalSettings.Save();
					SoapDataCache.Clear();
				}
			}
		}

		/// <summary>
		/// Writes all data to xml file in "logfiles" subdirectory
		/// </summary>
		private void _WriteLogFile()
		{
			if (!Context.Current.AutoMode)
					return;

			// Create Dataset
			var dataset = new DataSet ("orderdata");

			DataTable customersTable = this.AllCustomers.Copy ();
			customersTable.TableName = "customers";
			dataset.Tables.Add (customersTable);

			DataTable ordersTable = this.AllOrders.Copy ();
			ordersTable.TableName = "orders";
			dataset.Tables.Add (ordersTable);

			DataTable orderItemsTable = this.AllOrderItems.Copy ();
			orderItemsTable.TableName = "orderitems";
			dataset.Tables.Add (orderItemsTable);

			// Write XML
			string dir = Common.GetSubdirectory("logfiles");
			string filename = String.Format ("orders_{0}.xml", DateTime.Now.ToString ("yyyy-MM-dd_HH-mm-ss"));
			string fullFilename = Path.Combine (dir, filename);
			dataset.WriteXml (fullFilename, XmlWriteMode.IgnoreSchema);
		}

		/// <summary>
		/// Calls the "CheckAfterExport" method in all order plugins
		/// </summary>
		private void _Check() {
			foreach (OrderPluginBase plugin in OrderSettings.Current.OrdersPlugins) {
				plugin.CheckAfterExport ();
			}
		}

		private DataTable _CreateDataTable(List<ColumnInfo> columns)
		{
			// Create datatable
			var table = new DataTable();
			var primaryKeys = new List<DataColumn>();

			foreach(var column in columns) {
				
				// Add column
				table.Columns.Add(column.Name);
				
				// Add primary key to list
				if(column.IsPrimaryKey)
					primaryKeys.Add(table.Columns[column.Name]);
			}

			// Add primary keys to table
			table.PrimaryKey = primaryKeys.ToArray();

			// Add column for Gustav ID
			table.Columns.Add ("gustav_id", typeof(Int32));

			return table;
		}
	}
}

