Gustav Connector
===

This is a C#/Gtk# application for connecting ERP software to [Gustav](http://sandholzer-werbung.cc/gustav/).

Documentation is only available in german within the /doc directory:
 
[Move forward to the user documentation](doc/index.md)
